package redis;

import com.jc.soft.JCManagerApplication;
import com.jc.soft.contants.Constants;
import com.jc.soft.domain.MessageEntity;
import com.jc.soft.service.WebSocketServer;
import com.jc.soft.util.DateUtil;
import com.jc.soft.util.RedisUtil;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.StringUtils;

import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = JCManagerApplication.class)
public class RedisTest {

    private static final Logger logger = LoggerFactory.getLogger(RedisTest.class);
    @Autowired
    private RedisUtil redisUtil;

    @Test
    public void test() {
//        redisUtil.set("super", "super");
//        redisUtil.setExpire("super", 3);
//        logger.info("super:{}", redisUtil.get("super"));

        /*logger.info("time:{}", redisUtil.getExpire("super"));

        redisUtil.hmSet("super1", new HashMap<String, Object>() {{
            put("01", "01");
            put("02", 1);
        }});

        logger.info("super1:" + redisUtil.hmGet("super1"));

        logger.info("super2:" + redisUtil.incr("super2", 2));*/


        String formatDate = DateUtil.formatDate(new Date(), Constants.YYYYMMDD);

       /* MessageEntity messageEntity = new MessageEntity();
        messageEntity.setMessage("test");

        String key = redisUtil.get("key")+"";
        String key1="1-2"+formatDate;
        if("null".equals(key)){
            redisUtil.set("key",key1);
        }else {
            redisUtil.set("key",key+","+key1);
        }
        redisUtil.setList(key1,messageEntity );
        redisUtil.setList(key1,messageEntity );
        List<Object> list = redisUtil.getList(key1);


        key = redisUtil.get("key")+"";
        String key2="1-3"+formatDate;
        if(StringUtils.isEmpty(key)){
            redisUtil.set("key",key2);
        }else {
            redisUtil.set("key",key+","+key2);
        }
        redisUtil.setList(key2,messageEntity);
        redisUtil.setList(key2,messageEntity);

        List<Object> list1 = redisUtil.getList(key2);*/

       /* String key1="1-2"+formatDate;
        List<Object> msgList = new ArrayList<>();
        String key = redisUtil.get("key") + "";
        if(!"null".equals(key)){
            List<String> list = Arrays.asList(key.split(","));

            Iterator<String> it = list.iterator();
            while (it.hasNext()){
                String s = it.next();
                List<Object> msg = redisUtil.getList(s);
                msgList.addAll(msg);
                if(s.equals(key1)){
//                    it.remove();
                }
            }

            String s = list.toString();
            System.out.println(s);
        }*/

        List<Object> list = redisUtil.getList("wxPrivate-1-2-20211124");
        System.out.println(list);
    }

    public static void main(String[] args) {

    }
}
