package com.jc.soft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author Administrator
 */
@SpringBootApplication
@MapperScan("com.jc.soft.mapper")
public class JCManagerApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(JCManagerApplication.class,args);
        System.out.println("=======");
    }

    /**
     * 工程打成war需要继承SpringBootServletInitializer，重写configure方法
     */
    /*@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(JCManagerApplication.class);
    }*/

}
