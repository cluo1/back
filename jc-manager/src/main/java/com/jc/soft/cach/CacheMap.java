package com.jc.soft.cach;

import javax.websocket.Session;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CacheMap {
    private static volatile Map<Integer, Session> WEBSOCKET_MAP = null;

    public static Map<Integer,Session> getInstance(){
        if(WEBSOCKET_MAP==null){
            synchronized (CacheMap.class){
                if(WEBSOCKET_MAP==null){
                    WEBSOCKET_MAP = new ConcurrentHashMap<>();
                }
            }
        }
        return WEBSOCKET_MAP;
    }

}
