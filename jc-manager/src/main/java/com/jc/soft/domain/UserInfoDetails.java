package com.jc.soft.domain;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class UserInfoDetails implements UserDetails {
    private Integer id;
    private String username;
    private String password;
    private boolean active;
    private List<GrantedAuthority> authorities;

    public UserInfoDetails(UserInfo user) {
        this.id = user.getId();
        this.username = user.getUserName();
        this.password = user.getPassWord();
        this.active = true;//user.isActive();
        this.authorities = Arrays.stream(new String[]{"ADMIN"}).map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }

    public UserInfoDetails(String username) {
        this.username = username;
    }

    public UserInfoDetails() {

    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return active;
    }
}
