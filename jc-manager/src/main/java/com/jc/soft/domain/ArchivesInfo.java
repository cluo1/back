package com.jc.soft.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import javax.persistence.*;


@Data
@Table(name = "jc_archives")
public class ArchivesInfo extends Page {
    /**
     * id
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 姓名
     */
    @Column(name = "name")
    private String name;

    /**
     * 性别（男，女）
     */
    @Column(name = "sex")
    private String sex;

    /**
     * 照片（存照片位置路径）
     */
    @Column(name = "picture")
    private String picture;

    /**
     * 出生日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "birth_date")
    private Date birthDate;

    /**
     * 所在地
     */
    @Column(name = "location")
    private String location;

    /**
     * 工作
     */
    @Column(name = "work")
    private String work;

    /**
     * 个人简介
     */
    @Column(name = "introduction")
    private String introduction;

    /**
     * 择偶要求
     */
    @Column(name = "requirement")
    private String requirement;

    /**
     * 微信用户表id
     */
    @Column(name = "wx_id")
    private Integer wxId;

    /**
     * 联系方式
     */
    @Column(name = "contact")
    private String contact;

    /**
     * 隐藏标识（0显示 1隐藏）
     */
    @Column(name = "status")
    private String status;

    /**
     * 注册时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 查看档案者微信id
     */
    @Transient
    private Integer view_wxId;

    @Transient
    private Integer age1;
    @Transient
    private Integer age2;

    /**
     * 学历
     */
    @Column(name = "education")
    private String education;
    /**
     * 年收入
     */
    @Column(name = "salary")
    private String salary;

    /**
     * 是否收藏标识
     */
    @Transient
    private boolean isFavorites;

}