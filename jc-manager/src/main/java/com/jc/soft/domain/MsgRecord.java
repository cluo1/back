package com.jc.soft.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@Table(name = "jc_msg_record")
@Data
public class MsgRecord extends Page{


    /**
     * 发送人id
     */
    @Id
    @Column(name = "from_id")
    private Integer fromId;

    /**
     * 接收人id
     */
    @Id
    @Column(name = "to_id")
    private Integer toId;

    /**
     * 群组id
     */
    @Id
    @Column(name = "group_id")
    private Integer groupId;

    /**
     * 未读消息条数
     */
    private Integer count;

    /**
     * 总消息条数
     */
    private Integer total;

    /**
     * 是否已读 （1：是，0：否）
     */
    private Integer status;

    /**
     * 系统标识（0：微信私聊 1：后台私聊 2：微信客服）
     */

    private Integer type;

    @Column(name = "update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime;

    /**
     * 发送者姓名
     */
    @Transient
    private String fromName;

    /**
     * 发送者头像
     */
    @Transient
    private String fromAvatar;

    /**
     * 系统标识
     * 小程序私聊：wxPrivate 群聊：wxGroup
     * 后台私聊sysPrivate 群聊：sysGroup
     */
    @Transient
    private String flag;

    public MsgRecord() {
    }
    public MsgRecord(Integer fromId, Integer toId, Integer groupId, Integer count, Integer total, Integer status, Integer type,Date updateTime) {
        this.fromId = fromId;
        this.toId = toId;
        this.groupId = groupId;
        this.count = count;
        this.total = total;
        this.status = status;
        this.type = type;
        this.updateTime = updateTime;
    }
}