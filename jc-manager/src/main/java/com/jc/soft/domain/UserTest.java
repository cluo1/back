package com.jc.soft.domain;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "user")
@Data
public class UserTest {
    @Id
    private Integer id;

    private String username;

    private String password;
}