package com.jc.soft.domain;

import lombok.Data;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;

/**
 * @author Administrator
 */
@Data
public class Page<T> implements Serializable {
    private static final long serialVersionUID = -1861678872737157697L;

    // 当前页
    @Transient
    private Integer currentPage = 1;
    // 每页显示的总条数
    @Transient
    private Integer pageSize = 10;
    // 起始位置
    @Transient
    private int startIndex = 0;
    // 总条数
    @Transient
    private Long totalAll;
    // 分页结果
    @Transient
    private List<T> list;

    public Page() {
    }

    public Page(Long totalAll, List<T> list) {
        this.totalAll = totalAll;
        this.list = list;
    }

    /**
     * 获取起始位置
     * @return
     */
    public int getStartIndex() {
        return (currentPage-1)*pageSize;
    }
}
