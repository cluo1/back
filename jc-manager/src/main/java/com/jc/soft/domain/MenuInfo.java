package com.jc.soft.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Table(name = "jc_menu")
public class MenuInfo extends Page {
    /**
     * 主键id
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 菜单名称
     */
    @Column(name = "name")
    private String name;

    /**
     * 路径
     */
    @Column(name = "path")
    private String path;

    /**
     * 组件
     */
    @Column(name = "component")
    private String component;

    /**
     * 重定向
     */
    @Column(name = "redirect")
    private String redirect;

    /**
     * 仅有一个子节点时是否显示根目录（1:是0：否）
     */
    @Column(name = "always_show")
    private Boolean alwaysShow;

    /**
     * 父ID
     */
    @Column(name = "pid")
    private Integer pid;

    /**
     * meta标题
     */
    @Column(name = "meta_title")
    private String metaTitle;

    /**
     * meta icon
     */
    @Column(name = "meta_icon")
    private String metaIcon;

    /**
     * 是否缓存（1:是 0:否）
     */
    @Column(name = "meta_nocache")
    private Boolean metaNocache;

    /**
     * 是否加固（1:是0：否）
     */
    @Column(name = "meta_affix")
    private Boolean metaAffix;

    /**
     * 子菜单
     */
    @Transient
    private List<MenuInfo> children;

}