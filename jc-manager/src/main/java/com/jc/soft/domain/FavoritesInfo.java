package com.jc.soft.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "jc_favorites")
public class FavoritesInfo {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "wxId")
    private Integer wxid;

    /**
     * 档案表id
     */
    @Column(name = "archId")
    private Integer archid;

    /**
     * 查询收藏类型 1我收藏的人 2收藏我的人
     */
    @Transient
    private Integer type;

}