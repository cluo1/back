package com.jc.soft.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Data
@Table(name = "jc_advertising")
public class Advertising extends Page{
    /**
     * 主键
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "JDBC")
    private Integer id;


    /**
     * 图片
     */
    @Column(name = "picture")
    private String picture;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

    /**
     * 状态，0显示， 1隐藏
     */
    @Column(name = "`status`")
    private String status;

    /**
     * 序号
     */
    @Column(name = "order_no")
    private Integer orderNo;

    public Advertising() {
    }

    public Advertising(String picture, Date createTime, String status) {
        this.picture = picture;
        this.createTime = createTime;
        this.status = status;
    }
}