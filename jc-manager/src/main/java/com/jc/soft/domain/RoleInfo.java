package com.jc.soft.domain;

import lombok.Data;

import java.util.Date;
import javax.persistence.*;

@Data
@Table(name = "jc_role")
public class RoleInfo extends Page {
    /**
     * 主键id
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 角色类型，1普通角色，2后台系统角色
     */
    @Column(name = "type")
    private Integer type;

    /**
     * 角色名称
     */
    @Column(name = "role_name")
    private String roleName;

    /**
     * 角色说明
     */
    @Column(name = "remark")
    private String remark;

    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;
    /**
     * 更新时间
     */
    @Transient
    private String routeId;

}