package com.jc.soft.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "jc_menu_role")
public class MenuRoleInfo {
    /**
     * 主键
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 菜单id
     */
    @Column(name = "menu_id")
    private Integer menuId;

    /**
     * 角色id
     */
    @Column(name = "role_id")
    private Integer roleId;

}