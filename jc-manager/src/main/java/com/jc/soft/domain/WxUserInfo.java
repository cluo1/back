package com.jc.soft.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Table(name = "jc_wx_user")
public class WxUserInfo extends Page {

    /**
     * ID
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 小程序用户的openid
     */
    @Column(name = "openid")
    @NotNull
    private String openid;

    /**
     * 用户昵称
     */
    @Column(name = "nickName")
    private String nickName;

    /**
     * 用户头像
     */
    @Column(name = "avatarUrl")
    private String avatarUrl;

    /**
     * 性别  0-男、1-女
     */
    @Column(name = "sex")
    private String sex;

    /**
     * 所在国家
     */
    @Column(name = "country")
    private String country;

    /**
     * 省份
     */
    @Column(name = "province")
    private String province;

    /**
     * 城市
     */
    @Column(name = "city")
    private String city;

    /**
     * 创建/注册时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "ctime")
    private Date ctime;

    /**
     * 是否是客服 0不是 1是的
     */
    @Column(name = "customer_service")
    private Integer customer_service;

    /**
     * 档案表id
     */
    @Column(name = "archId")
    private Integer archId;

    /**
     * 用户唯一识别码
     */
    @Transient
    private String code;
    /**
     * 收藏量-被收藏
     */
    @Transient
    private int collect;

}