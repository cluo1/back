package com.jc.soft.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * MessageEntity
 *
 * @author zjw
 * @createTime 2021/2/3 20:08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageEntity {

    /**
     * 发送人id
     */
    private int fromId;

    /**
     * 发送人名称
     */
    private String fromName;

    /**
     * 发送人的头像
     */
    private String fromAvatar;

    /**
     * 接收人id
     */
    private int toId;
    /**
     * 接收人姓名
     */
    private String toName;
    /**
     * 接收人的头像
     */
    private String toAvatar;
    private String message;

    private Date date;

    private String time;

    /**
     * 系统标识
     * 小程序私聊：wxPrivate 群聊：wxGroup 客服：cuService
     * 后台私聊sysPrivate 群聊：sysGroup
     */
    private String flag;

}
