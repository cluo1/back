package com.jc.soft.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Table(name = "jc_user")
public class UserInfo extends Page{
    /**
     * 主键id
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 真实姓名
     */
    @Column(name = "real_name")
    private String realName;

    /**
     * 登录名称
     */

    @Column(name = "user_name")
    private String userName;

    /**
     * 登录密码
     */
    @Column(name = "pass_word")
    private String passWord;

    /**
     * 性别：0未知，1男，2女
     */
    @Column(name = "gender")
    private Integer gender;

    /**
     * 联系方式
     */
    @Column(name = "phone")
    private String phone;

    /**
     * 邮箱地址
     */
    @Column(name = "email")
    private String email;

    // 角色名称
    @Transient
    private List<String> roleNames;
    //角色id
    @Transient
    private List<Integer> roleIds;

    /**
     * 头像照片
     */
    @Column(name = "avatar")
    private String avatar;

    @Transient
    private String oldPassWord;

}