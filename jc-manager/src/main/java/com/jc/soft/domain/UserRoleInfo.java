package com.jc.soft.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "jc_user_role")
public class UserRoleInfo {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 角色id
     */
    @Column(name = "role_id")
    private Integer roleId;

    /**
     * 角色名称
     */
    @Transient
    private String roleName;

}