package com.jc.soft.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class User implements Serializable {

    private static final long serialVersionUID = -1306976738418719264L;

    private UserInfo userInfo;

    private List<RoleInfo> roles;

}
