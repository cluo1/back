package com.jc.soft.exception;

import com.jc.soft.dto.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局统一异常处理
 * @Author Admin
 */
@RestControllerAdvice
@Slf4j
public class DefaultExceptionHandler {
    /**
     * 处理所有自定义异常
     * @param e 自定义异常
     */
    @ExceptionHandler(CustomException.class)
    public ResponseResult handleCustomException(CustomException e){
        log.error(e.getResponse().getMessage());
        return e.getResponse();
    }

    @ExceptionHandler(Exception.class)
    public ResponseResult<Void> handleException(Exception e){
        e.printStackTrace();
        log.error(e.getMessage());
        return new ResponseResult<>(ResponseResult.CodeStatus.FAIL,"出错了, 请稍后再试");
    }

    /**
     * 方法参数校验
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseResult<String> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        log.error(e.getMessage(), e);
        return new ResponseResult<>(ResponseResult.CodeStatus.FAIL,e.getBindingResult().getFieldError().getDefaultMessage());
    }
}
