package com.jc.soft.exception;

import com.jc.soft.dto.ResponseResult;
import lombok.Getter;

@Getter
public class CustomException extends RuntimeException {
    private ResponseResult response;

    public CustomException(ResponseResult response) {
        this.response = response;
    }
}
