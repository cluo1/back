package com.jc.soft.controller.sys;

import com.jc.soft.domain.Page;
import com.jc.soft.domain.User;
import com.jc.soft.domain.UserInfo;
import com.jc.soft.dto.ResponseResult;
import com.jc.soft.service.UserService;
import com.jc.soft.util.BaseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/user")
public class UserController {

    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;


    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    /**
     * 获取用户及角色信息
     * @param request
     * @return
     */
    @RequestMapping("/info")
    public ResponseResult<User> getUserInfo(HttpServletRequest request){

        try {
            User user = BaseUtil.getUserByReq(BaseUtil.getJwt(request));
            return new ResponseResult<>(ResponseResult.CodeStatus.OK,"获取用户信息成功！",user);
        } catch (Exception e) {
            logger.error("getUserInfo error ",e);
            return new ResponseResult<>(ResponseResult.CodeStatus.FAIL,"未查询到用户信息！");
        }
    }

    /**
     * 获取用户列表
     * @param userInfo 分页条件
     * @return {@link ResponseResult}
     */
    @RequestMapping("/findUsers")
    public ResponseResult<Page<UserInfo>> findUserList(@RequestBody UserInfo userInfo){

        Page<UserInfo> userList = null;
        try {
            userList = userService.getUserList(userInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseResult<>(ResponseResult.CodeStatus.FAIL,ResponseResult.CodeStatus.SELECT_FAIL);

        }
        return new ResponseResult<>(ResponseResult.CodeStatus.OK,ResponseResult.CodeStatus.SELECT_SUCCESS,userList);

    }

    /**
     * 查询用户信息
     * @param userName 用户名
     */
    @RequestMapping("/check")
    public ResponseResult<UserDetails> findUserByUsername(String userName) {
        try {
            UserDetails userDetails = userService.loadUserByUsername(userName);

            if (userDetails != null ) {
                return new ResponseResult<>(ResponseResult.CodeStatus.OK,"获取用户信息", userDetails);
            } else {
                return new ResponseResult<>(ResponseResult.CodeStatus.OK,"未获取到用户信息");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseResult<>(ResponseResult.CodeStatus.FAIL, "获取用户信息失败");
    }

    /**
     * 添加或编辑用户及角色
     * @param userInfo 用户角色实体
     * @return {@link ResponseResult}
     */
    @PostMapping("updateUser")
    public ResponseResult<String> addOrUpdateUser(@RequestBody UserInfo userInfo){

        try {
            int i = 0;
            if(userInfo.getId()!=null && userInfo.getId()>0){
               i = userService.updateUserAndRole(userInfo);
            }else {
                i =  userService.insertUser(userInfo);
            }
            if (i > 0) {
                return new ResponseResult<>(ResponseResult.CodeStatus.OK,"操作成功！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseResult<>(ResponseResult.CodeStatus.FAIL,"操作失败！");
    }

    /**
     * 添加或编辑用户及角色
     * @param userInfo 用户角色实体
     * @return {@link ResponseResult}
     */
    @PutMapping("/update")
    public ResponseResult<String> update(@RequestBody UserInfo userInfo){

        try {
            int i = userService.updateUser(userInfo);
            if (i > 0) {
                return new ResponseResult<>(ResponseResult.CodeStatus.OK,"操作成功！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseResult<>(ResponseResult.CodeStatus.FAIL,"操作失败！");
    }



    /**
     * 重置密码
     * @param user 用户
     * @return {@link ResponseResult}
     */
    @PostMapping("/resetPwd")
    public ResponseResult<String> resetPassword(@RequestBody UserInfo user){
        try {
            int update = 0;
            if (user.getId() != null && user.getId() > 0) {
                update = userService.resetPassword(user);
            }
            if (update > 0) {
                return new ResponseResult<>(ResponseResult.CodeStatus.OK,"操作成功！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseResult<>(ResponseResult.CodeStatus.FAIL,"操作失败！");
    }

    @RequestMapping("delUsers")
    public ResponseResult<String> delUsersByIds(@RequestParam("ids") String ids) {
        try {
            int del = userService.delUsersByIds(ids);
            if (del > 0) {
                return new ResponseResult<>(ResponseResult.CodeStatus.OK,"删除用户成功！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseResult<>(ResponseResult.CodeStatus.FAIL,"删除用户失败！");
    }

    /**
     * 修改密码
     * @param userInfo 用户
     * @return {@link ResponseResult}
     */
    @PostMapping("updatePwd")
    public ResponseResult<String> updatePwd(@RequestBody UserInfo userInfo){
        try {
            int update = 0;
            // 更新角色
            if (userInfo != null && userInfo.getId() > 0) {
                UserInfo userById = userService.getUserById(userInfo.getId());
                boolean matches = passwordEncoder.matches(userInfo.getOldPassWord(), userById.getPassWord());
                if (!matches) {
                    return new ResponseResult<>(ResponseResult.CodeStatus.FAIL,"修改失败，原密码输入错误！");
                }
                boolean matches2 = passwordEncoder.matches(userInfo.getOldPassWord(), userInfo.getPassWord());
                if (matches2) {
                    return new ResponseResult<>(ResponseResult.CodeStatus.FAIL,"修改失败，新密码不能与旧密码相同！");
                }
                String pwd = passwordEncoder.encode(userInfo.getPassWord());
                userInfo.setPassWord(pwd);
                update = userService.updateUser(userInfo);
            }
            if (update > 0) {
                return new ResponseResult<>(ResponseResult.CodeStatus.OK,"密码修改成功！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseResult<>(ResponseResult.CodeStatus.FAIL,"操作失败！");
    }

}
