package com.jc.soft.controller.wx;


import com.jc.soft.domain.LoginEntity;
import com.jc.soft.domain.ResultEntity;
import com.jc.soft.domain.UserTest;
import com.jc.soft.service.UserServiceTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * LoginController
 *
 * @author zjw
 * @createTime 2021/1/29 14:51
 */
@RestController
public class LoginInController {

    @Autowired
    private  UserServiceTest userServiceTest;


    @PostMapping("/login")
    public  ResultEntity login(@RequestBody LoginEntity loginEntity) {
        UserTest user = userServiceTest.findByUsernameAndPassword(loginEntity.getUsername(), loginEntity.getPassword());
        return new ResultEntity(user);
    }

}
