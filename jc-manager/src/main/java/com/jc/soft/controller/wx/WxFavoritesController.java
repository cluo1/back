package com.jc.soft.controller.wx;

import com.jc.soft.domain.ArchivesInfo;
import com.jc.soft.domain.FavoritesInfo;
import com.jc.soft.dto.ResponseResult;
import com.jc.soft.service.FavoritesService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/wx")
public class WxFavoritesController {

    @Resource
    private FavoritesService favoritesService;

    /**
     * 添加收藏
     * @param
     * @return
     */
    @RequestMapping("addFavorites")
    public ResponseResult addFavorites(@RequestBody FavoritesInfo favoritesInfo) {
        try {
            if(favoritesInfo!=null){
                favoritesService.addFavorites(favoritesInfo);
            }
            return new ResponseResult<>(ResponseResult.CodeStatus.OK, "收藏成功！");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseResult<>(ResponseResult.CodeStatus.FAIL, "收藏失败！");
    }

    /**
     * 删除收藏
     * @param favoritesInfo
     * @return {@link ResponseResult}
     */
    @RequestMapping("/delFavorites")
    public ResponseResult delFavorites(@RequestBody FavoritesInfo favoritesInfo) {
        try {
            if (favoritesInfo != null) {
                favoritesService.delFavorites(favoritesInfo);
                return new ResponseResult<>(ResponseResult.CodeStatus.OK, "取消收藏成功！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseResult<>(ResponseResult.CodeStatus.FAIL, "取消收藏失败");
    }

    /**
     * 获取收藏信息
     * @param favoritesInfo
     * @return
     */
    @RequestMapping("/getFavoritList")
    public ResponseResult<List<ArchivesInfo>> getFavoritList(@RequestBody FavoritesInfo favoritesInfo){
        try {
            List<ArchivesInfo> list = favoritesService.getFavoritList(favoritesInfo);
            return new ResponseResult(ResponseResult.CodeStatus.OK, ResponseResult.CodeStatus.SELECT_SUCCESS, list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseResult<>(ResponseResult.CodeStatus.FAIL, ResponseResult.CodeStatus.SELECT_FAIL);
    }
}
