package com.jc.soft.controller;

import com.jc.soft.domain.MsgRecord;
import com.jc.soft.dto.ResponseResult;
import com.jc.soft.service.MsgRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/wx")
public class MsgRecordController {

    @Autowired
    private MsgRecordService msgRecordService;

    /**
     * 未读消息列表
     * @param msgRecord
     * @return
     */
    @RequestMapping("/getMsgList")
    public ResponseResult<List<MsgRecord>> getMsgList(@RequestBody MsgRecord msgRecord){
        List<MsgRecord> msgList = null;
        try {
            msgList = msgRecordService.getMsgList(msgRecord);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseResult<>(ResponseResult.CodeStatus.OK, ResponseResult.CodeStatus.OPERATE_SUCCESS,msgList);
        }

        return new ResponseResult<>(ResponseResult.CodeStatus.OK,  ResponseResult.CodeStatus.OPERATE_SUCCESS,msgList);

    }
}
