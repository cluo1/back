package com.jc.soft.controller.wx;

import com.jc.soft.config.Config;
import com.jc.soft.dto.*;
import com.jc.soft.domain.Page;
import com.jc.soft.domain.WxUserInfo;
import com.jc.soft.service.WxUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/wx")
@Slf4j
public class WxUserController {
    @Autowired
    private WxUserService wxUserService;

    /**************************** 微信用户表 -开始 ****************************/
    /**
     * 获取openid
     */
    @PostMapping("/getOpenId")
    public ResponseResult<String> getOpenId(@RequestBody Config config)  {
        try {
            String openid= wxUserService.getOpenid(config.getCode(),config.getSource());
            return new ResponseResult<>(ResponseResult.CodeStatus.OK,"获取openid成功！", openid);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseResult<>(ResponseResult.CodeStatus.FAIL, "获取openid失败！");
        }
    }
    /**
     * 用户登录小程序保存登录信息
     */
    @RequestMapping("/addWxUser")
    public ResponseResult<WxUserInfo> addWxUser(@RequestBody WxUserInfo wxUserInfo){
        try {
            wxUserService.addWxUser(wxUserInfo);
            return new ResponseResult<>(ResponseResult.CodeStatus.OK, "增加微信新用户成功", wxUserInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseResult<>(ResponseResult.CodeStatus.FAIL, "增加微信新用户失败");
    }

    @RequestMapping("/findWxUser")
    public ResponseResult findWxUser(@RequestBody WxUserInfo wxUserInfo)  {
        try {
            wxUserInfo = wxUserService.findWxUser(wxUserInfo);
            // 记录总数
            return new ResponseResult<>(ResponseResult.CodeStatus.OK,"获取微信用户成功！", wxUserInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseResult<>(ResponseResult.CodeStatus.FAIL, "获取微信用户失败！");
        }

    }

    @RequestMapping("/findWxUserList")
    public ResponseResult findWxUserList(@RequestBody WxUserInfo wxUserInfo)  {
        try {
            Page<WxUserInfo> wxUserList = wxUserService.findWxUserList(wxUserInfo);
            return new ResponseResult<>(ResponseResult.CodeStatus.OK,"获取微信用户成功！",wxUserList);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseResult<>(ResponseResult.CodeStatus.FAIL, "获取微信用户失败！");
        }

    }

}
