package com.jc.soft.controller.sys;

import com.jc.soft.domain.MenuInfo;
import com.jc.soft.domain.RoleInfo;
import com.jc.soft.domain.User;
import com.jc.soft.dto.ResponseResult;
import com.jc.soft.service.MenuService;
import com.jc.soft.util.BaseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/route")
public class MenuController {

    @Autowired
    private MenuService menuService;
    /**
     * 获取所有动态菜单信息
     * @return {@link MenuInfo}
     */
    @GetMapping("/tree")
    public ResponseResult<List<MenuInfo>> findAllMenus(HttpServletRequest request) {
        User user = BaseUtil.getUserByReq(BaseUtil.getJwt(request));

        List<RoleInfo> roles = user.getRoles();
        List<Integer> collect = roles.stream().map(m -> m.getId()).collect(Collectors.toList());
        List<MenuInfo> tree = menuService.getAllMenuByRole(collect);
        return new ResponseResult<>(ResponseResult.CodeStatus.OK, "获取所有菜单信息", tree);

    }
}
