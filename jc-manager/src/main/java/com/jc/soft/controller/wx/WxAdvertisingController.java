package com.jc.soft.controller.wx;

import com.jc.soft.domain.Advertising;
import com.jc.soft.dto.ResponseResult;
import com.jc.soft.service.AdvertisingService;;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/wx")
public class WxAdvertisingController {

    @Resource
    private AdvertisingService advertisingService;

    @PostMapping("/findAllAdvertising")
    public ResponseResult<List<Advertising>> findAllAdvertising(@RequestBody Advertising advertising){
        List<Advertising> list = advertisingService.selectAdvertising(advertising);
        if (list != null){
            return new ResponseResult<>(ResponseResult.CodeStatus.OK, ResponseResult.CodeStatus.OPERATE_SUCCESS,list);
        }else{
            return new ResponseResult<>(ResponseResult.CodeStatus.OK,  ResponseResult.CodeStatus.OPERATE_SUCCESS,null);

        }
    }

}
