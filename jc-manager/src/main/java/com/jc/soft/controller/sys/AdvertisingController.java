package com.jc.soft.controller.sys;

import com.jc.soft.domain.Advertising;
import com.jc.soft.domain.Page;
import com.jc.soft.dto.ResponseResult;
import com.jc.soft.service.AdvertisingService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

;

@RestController
@RequestMapping("/advertising")
public class AdvertisingController {

    @Resource
    private AdvertisingService advertisingService;

    @PostMapping("/query")
    public ResponseResult<Page<Advertising>> findAllAdvertising(@RequestBody Advertising advertising){

        try {
            Page<Advertising> page = advertisingService.getAdvertList(advertising);
            return new ResponseResult<>(ResponseResult.CodeStatus.OK, "查询广告成功",page);
        } catch (Exception e) {
            return new ResponseResult<>(ResponseResult.CodeStatus.FAIL, "查询广告失败");
        }

    }

    /**
     * 添加或修改广告位信息
     * @param
     * @return
     */
    @RequestMapping("addOrUpdateAdver")
    public ResponseResult addOrUpdateAdver(@RequestBody Advertising advertising) {
        try {
            advertisingService.addOrUpdateAdvertising(advertising);
            return new ResponseResult<>(ResponseResult.CodeStatus.OK,  ResponseResult.CodeStatus.OPERATE_SUCCESS,advertising);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseResult<>(ResponseResult.CodeStatus.FAIL, ResponseResult.CodeStatus.OPERATE_FIAL,null);

        }
    }

    /**
     * 删除广告位
     * @param ids 广告位id
     * @return {@link ResponseResult}
     */
    @RequestMapping("/delete")
    public ResponseResult delAdvertisingByIds(String ids) {
        try {
            if (ids != null) {
                advertisingService.delAdvertising(ids);
                return new ResponseResult<>(ResponseResult.CodeStatus.OK, "删除成功！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseResult<>(ResponseResult.CodeStatus.FAIL, "操作失败");
    }

}
