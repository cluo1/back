package com.jc.soft.controller;

import com.jc.soft.service.FileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

@RequestMapping("/wx")
@RestController
public class FileUploadController {

    @Autowired
    private FileService fileService;

    @PostMapping("/uploadFile")
    public synchronized String uploadFile(HttpServletRequest request, @RequestParam(value = "file", required = false) MultipartFile file) {
        return fileService.uploadFile(file);
    }

    @PostMapping("/delFile")
    public  void delFile(HttpServletRequest request, String path) {
       fileService.delFile(path);
    }
}

