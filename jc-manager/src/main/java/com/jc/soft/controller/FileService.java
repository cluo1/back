package com.jc.soft.controller;

import com.alibaba.excel.util.FileUtils;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class FileService {

    private static final Logger logger = LoggerFactory.getLogger(FileService.class);

    @RequestMapping("/voiceAudiometry")
    public Map voiceAudiometry(@RequestBody String path){

        Map<String, Object> map = new HashMap<>();
        JSONObject jsonObject = JSONObject.parseObject(path);
        String path1 = jsonObject.getString("path");
        File file =new File(path1);
        byte[] bytes = new byte[0];
        try {
            bytes = FileUtils.readFileToByteArray(file);
        } catch (IOException e) {
            map.put("code","-1");
            map.put("message","获取录音失败");
        }
        String s = Base64.encodeBase64String(bytes);
        map.put("code","0");
        map.put("data",s);
        map.put("message","获取录音成功");

        return map;
    }

}
