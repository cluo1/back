package com.jc.soft.controller.sys;

import com.jc.soft.domain.Page;
import com.jc.soft.domain.RoleInfo;
import com.jc.soft.dto.ResponseResult;
import com.jc.soft.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    /**
     * 添加或编辑角色
     * @param role 角色
     * @return {@link ResponseResult}
     */
    @PostMapping("update")
    public ResponseResult<String> updateOrAddRoles(@RequestBody RoleInfo role){
        try {
            int update = 0;
            if (role.getId() != null && role.getId() > 0) {// 更新角色
                update = roleService.updateRole(role);
            } else { // 添加角色
                RoleInfo r = roleService.getRole(role);
                if(!ObjectUtils.isEmpty(r)){
                    return new ResponseResult<>(ResponseResult.CodeStatus.FAIL,"角色名称已存在");
                }
                role.setType(1);
                update = roleService.addRole(role);

            }
            if (update > 0) {
                return new ResponseResult<>(ResponseResult.CodeStatus.OK,"操作成功！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseResult<>(ResponseResult.CodeStatus.FAIL,"操作失败！");
    }

    @RequestMapping("/list")
    public ResponseResult<Page<RoleInfo>> getRoleList(@RequestBody RoleInfo roleInfo){
        Page<RoleInfo> roleList = null;
        try {
            roleList = roleService.getRoleList(roleInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseResult<>(ResponseResult.CodeStatus.FAIL,"false", roleList);

        }
        return new ResponseResult<>(ResponseResult.CodeStatus.OK,"true", roleList);
    }

    @RequestMapping("/del")
    public ResponseResult<String> delRolesByIds(String ids){
        try {
            roleService.delRole(ids);
        } catch (Exception e) {
            return new ResponseResult<>(ResponseResult.CodeStatus.FAIL,ResponseResult.CodeStatus.OPERATE_FIAL);
        }
        return new ResponseResult<>(ResponseResult.CodeStatus.OK,ResponseResult.CodeStatus.OPERATE_SUCCESS);
    }
}
