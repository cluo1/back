package com.jc.soft.controller.wx;

import com.alibaba.fastjson.JSONObject;
import com.jc.soft.cach.CacheMap;
import com.jc.soft.domain.MsgRecord;
import com.jc.soft.dto.ResponseResult;
import com.jc.soft.service.MsgRecordService;
import com.jc.soft.util.PageUtil;
import com.jc.soft.util.RedisUtil;
import org.apache.tomcat.util.buf.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * PullMessageController
 *
 * @author zjw
 * @createTime 2021/2/3 22:44
 */
@RestController
@RequestMapping("/wx")
public class PullMessageController {

    private final RedisUtil redis;
    private final static Logger logger = LoggerFactory.getLogger(PullMessageController.class);

    @Autowired
    public PullMessageController(RedisUtil redis) {
        this.redis = redis;
    }

    @Autowired
    private MsgRecordService msgRecordService;
    /**
     * 获取消息列表
     * @param msgRecord
     * @return
     */
    @PostMapping("/pullMsg")
    public ResponseResult pullMsg(@RequestBody MsgRecord msgRecord) {

        //更新已读
        if( "wxPrivate".equals(msgRecord.getFlag()) || "sysPrivate".equals(msgRecord.getFlag())||"cuService".equals(msgRecord.getFlag())){
            if("wxPrivate".equals(msgRecord.getFlag())){
                msgRecord.setType(0);
            }else if("sysPrivate".equals(msgRecord.getFlag())){
                msgRecord.setType(1);
            }else{
                msgRecord.setType(2);
            }
            msgRecordService.updateMsg(msgRecord);
        }

        Map<String, Object> map = new HashMap<>();
        List<Object> msgList = new ArrayList<>();
        map.put("msgList",msgList);

        Integer fromId = msgRecord.getFromId();
        Integer toId = msgRecord.getToId();
        if(CacheMap.getInstance().get(fromId)!= null){
            map.put("myLine","1");
        }else {
            map.put("myLine","0");
        }
        if(CacheMap.getInstance().get(toId)!= null){
            map.put("toLine","1");
        }else {
            map.put("toLine","0");
        }
        String key="";
        if("wxGroup".equals(msgRecord.getFlag()) || "sysGroup".equals(msgRecord.getFlag())){
            key = msgRecord.getFlag()+"1";
        }else {
            key  = LongStream.of(fromId,toId)
                    .sorted()
                    .mapToObj(String::valueOf)
                    .collect(Collectors.joining("-"));
            key = msgRecord.getFlag()+"-"+key;
        }

        //按天获取消息
        String val = redis.get(key) + "";
        if("null".equals(val)){
            return new ResponseResult<>(ResponseResult.CodeStatus.OK, ResponseResult.CodeStatus.OPERATE_SUCCESS,map);
        }
        List<String> list = Arrays.asList(val.split(",")).stream().collect(Collectors.toList());
        logger.info("day==msg==key:{}", JSONObject.toJSON(list));
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()){
            //每天的消息key
            String next = iterator.next();
            List<Object> msg = redis.getList(next);
            if(!CollectionUtils.isEmpty(msg)){
                msgList.addAll(msg);
            }else {
                iterator.remove();
            }
        }
        if(CollectionUtils.isEmpty(msgList)){
            redis.del(key);
        }else {
            redis.set(key, StringUtils.join(list,','));
        }

        //分页
        List pageList = new ArrayList();
        if(!"sysPrivate".equals(msgRecord.getFlag())){
            pageList = PageUtil.endPage(msgList, msgRecord.getCurrentPage(), msgRecord.getPageSize());
        }else {
            pageList = msgList;
        }
//        map.put("msgList",pageList);
        map.put("msgList",msgList);
        return new ResponseResult<>(ResponseResult.CodeStatus.OK, ResponseResult.CodeStatus.OPERATE_SUCCESS,map);

    }

}
