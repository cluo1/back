package com.jc.soft.controller.wx;

import com.alibaba.fastjson.JSON;
import com.jc.soft.dto.*;
import com.jc.soft.domain.Page;
import com.jc.soft.domain.ArchivesInfo;
import com.jc.soft.domain.FavoritesInfo;
import com.jc.soft.service.ArchivesService;
import com.jc.soft.service.FavoritesService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 个人档案Controller
 */
@RestController
@RequestMapping("/wx")
@Slf4j
public class WxArchivesController {

    private final static Logger logger = LoggerFactory.getLogger(WxArchivesController.class);
    @Autowired
    private ArchivesService archivesService;
    @Autowired
    private FavoritesService favoritesService;


    /**
     * 新增或更新档案
     */
    @PostMapping("/addOrUpdateArchives")
    public ResponseResult<ArchivesInfo> addOrUpdateArchives(@RequestBody ArchivesInfo archivesInfo)  {
        logger.info("addOrUpdateArchives======{}", JSON.toJSONString(archivesInfo));
        try {
            archivesService.addOrUpdateArchives(archivesInfo);
            return new ResponseResult<>(ResponseResult.CodeStatus.OK,ResponseResult.CodeStatus.OPERATE_SUCCESS, archivesInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseResult<>(ResponseResult.CodeStatus.FAIL, ResponseResult.CodeStatus.OPERATE_FIAL);
        }
    }
    /**
     * 获取档案信息
     */
    @RequestMapping("/getArchives")
    public ResponseResult<ArchivesInfo> getArchives(@RequestBody ArchivesInfo archivesInfo){
        logger.info("getArchives======{}", JSON.toJSONString(archivesInfo));

        try {
            ArchivesInfo res = archivesService.getArchives(archivesInfo);
            //别人查看档案
            if(archivesInfo.getView_wxId()!=null){
                FavoritesInfo f=favoritesService.getFavorites(archivesInfo.getView_wxId(),archivesInfo.getId());
                if(f!=null){
                    res.setFavorites(true);
                }else{
                    res.setFavorites(false);
                }
            }
            return new ResponseResult<ArchivesInfo>(ResponseResult.CodeStatus.OK, ResponseResult.CodeStatus.SELECT_SUCCESS,res );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseResult<>(ResponseResult.CodeStatus.FAIL, ResponseResult.CodeStatus.SELECT_FAIL);
    }

    /**
     * 分页获取档案信息
     * @param archivesInfo
     * @return
     */
    @RequestMapping("/getArchivesList")
    public ResponseResult getArchivesList(@RequestBody ArchivesInfo archivesInfo){
        logger.info("getArchivesList======{}", JSON.toJSONString(archivesInfo));

        try {
            Page<ArchivesInfo> page = archivesService.getArchivesList(archivesInfo);
            return new ResponseResult(ResponseResult.CodeStatus.OK, ResponseResult.CodeStatus.SELECT_SUCCESS, page);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseResult<>(ResponseResult.CodeStatus.FAIL, ResponseResult.CodeStatus.SELECT_FAIL);
    }
    /**
     * 获取所有档案信息
     * @param archivesInfo
     * @return
     */
    @RequestMapping("/getAllArchivesList")
    public ResponseResult getAllArchivesList(@RequestBody ArchivesInfo archivesInfo){
        logger.info("getArchivesList======{}", JSON.toJSONString(archivesInfo));

        try {
            List<ArchivesInfo> list= archivesService.getAllArchivesList(archivesInfo);
            return new ResponseResult(ResponseResult.CodeStatus.OK, ResponseResult.CodeStatus.SELECT_SUCCESS, list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseResult<>(ResponseResult.CodeStatus.FAIL, ResponseResult.CodeStatus.SELECT_FAIL);
    }
    /**
     * 更新档案状态
     * @param archivesInfo
     * @return
     */
    @RequestMapping("/updateArchivesStatus")
    public ResponseResult updateArchivesStatus(@RequestBody ArchivesInfo archivesInfo)  {
        logger.info("updateArchivesStatus======{}", JSON.toJSONString(archivesInfo));

        try {
            archivesService.updateArchivesStatus(archivesInfo);
            // 记录总数
            return new ResponseResult<>(ResponseResult.CodeStatus.OK,ResponseResult.CodeStatus.OPERATE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseResult<>(ResponseResult.CodeStatus.FAIL, ResponseResult.CodeStatus.OPERATE_FIAL);
        }

    }

}
