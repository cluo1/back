package com.jc.soft.controller.wx;

import com.jc.soft.domain.ResultEntity;
import com.jc.soft.domain.UserTest;
import com.jc.soft.service.UserService;
import com.jc.soft.service.UserServiceTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * GetFriendsController
 *
 * @author zjw
 * @createTime 2021/2/4 08:54
 */
@RestController
public class GetFriendsController {

    @Autowired
    private UserServiceTest userServiceTest;


    @PostMapping("/getFriends")
    public ResultEntity getFriends(@RequestParam("id") Long uid) {
        List<UserTest> friends = userServiceTest.getFriends(uid);
        return new ResultEntity(friends);
    }

}
