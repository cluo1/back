package com.jc.soft.controller.sys;

import com.jc.soft.exception.CustomException;
import com.jc.soft.service.UserService;
import com.jc.soft.dto.ResponseResult;
import com.jc.soft.jwt.JWTTokenUtil;
import com.jc.soft.util.BaseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/auth")
public class LoginController {
    private final static Logger logger = LoggerFactory.getLogger(LoginController.class);
    @Resource
    private AuthenticationManager authenticationManager;

    @Resource
    private UserService userService;

    @Resource
    private JWTTokenUtil myJwtToken;

    /**
     * 登录
     * @return {@link ResponseResult}
     */
    @RequestMapping("/login")
    public ResponseResult<Map<String, String>> login(@RequestParam("username") String username, @RequestParam("password") String password) throws Exception {
        try {
            //该方法会去调用userDetailsService.loadUserByUsername()去验证用户名和密码，如果正确，则存储该用户名密码到“security 的 context中”
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    username, password));
        } catch (Exception e) {
            throw new CustomException(new ResponseResult<>(ResponseResult.CodeStatus.FAIL,"用户名或密码错误"));
        }

        final UserDetails userDetails = userService.loadUserByUsername(username);
        String jwt = myJwtToken.generateToken(userDetails);

        BaseUtil.getUserByReq(jwt);
        Map<String, String> map = new HashMap<>();
        map.put("username",username);
        map.put("token",jwt);
        return new ResponseResult<>(ResponseResult.CodeStatus.OK,map);
    }

    /**
     * 注销登录
     * @return {@link ResponseResult}
     */
    @RequestMapping("logout")
    public ResponseResult logout(){
        return new ResponseResult<>(ResponseResult.CodeStatus.OK);
    }


}
