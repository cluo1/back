package com.jc.soft.config;

import lombok.Data;

@Data
public class Config {

    /**
     * 来源
     */
    private String source;

    /**
     * 小程序code
     */
    private String code;

}
