package com.jc.soft.config;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 自定义密码不加密实现
 */
public class NoEnPwdEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence charSequence) {
        return charSequence.toString();
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        if (charSequence.toString().equals(s)) {
            return true;
        }
        return false;
    }
}
