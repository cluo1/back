package com.jc.soft.service;

import com.jc.soft.domain.WxUserInfo;
import com.jc.soft.domain.Page;

public interface WxUserService {

    /**
     * 查询openid
     *
     * @param code
     * @param source
     * @return
     */
    public String getOpenid(String code, String source);

    /**
     * 新增微信用户
     * @param wxUserInfo
     * @return
     */
    WxUserInfo addWxUser(WxUserInfo wxUserInfo);

    /**
     * 获取微信用户信息
     * @param wxUserInfo
     * @return
     */
    WxUserInfo findWxUser(WxUserInfo wxUserInfo);


    Page<WxUserInfo> findWxUserList(WxUserInfo wxUserInfo);

}
