package com.jc.soft.service;

import com.jc.soft.domain.MsgRecord;

import java.util.List;

public interface MsgRecordService {

    int saveOrUpdateMsg(MsgRecord msgRecord);

    List<MsgRecord> getMsgList(MsgRecord msgRecord);

    int updateMsg(MsgRecord msgRecord);
}
