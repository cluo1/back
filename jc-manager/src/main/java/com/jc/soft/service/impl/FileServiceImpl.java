package com.jc.soft.service.impl;

import com.jc.soft.service.FileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

@Service
public class FileServiceImpl implements FileService {

    private static final Logger logger = LoggerFactory.getLogger(FileServiceImpl.class);

    @Value("${file.path:/home}")
    private String filePath;

    @Value("${file.prefix:/img}")
    private String filePrefix;


    @Override
    public String uploadFile(MultipartFile file) {
        String fileName = file.getOriginalFilename();

        try {
            //此处解决获取到的fileName包含路径信息问题
            int unixSep = fileName.lastIndexOf('/');

            int winSep = fileName.lastIndexOf('\\');

            int pos = (winSep > unixSep ? winSep : unixSep);
            if (pos != -1) {
                fileName = fileName.substring(pos + 1);
            }
            String fold = filePath +"/" + fileName;

            File folder = new File(fold);
            if (!folder.getParentFile().exists()) {
                folder.getParentFile().mkdirs();
            }
            FileInputStream in = null;
            FileOutputStream out = null;
            FileChannel fileChannel = null;
            FileChannel fileChannelout = null;
            try {
                out = new FileOutputStream(folder);
                in = (FileInputStream) file.getInputStream();
                fileChannel = in.getChannel();
                fileChannelout = out.getChannel();
                ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
                while (true) {
                    byteBuffer.clear();
                    int r = fileChannel.read(byteBuffer);
                    if (r == -1) {
                        break;
                    }
                    byteBuffer.flip();
                    while (byteBuffer.remaining() > 0) {
                        fileChannelout.write(byteBuffer);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("文件传输错误:" + e.getMessage());
            } finally {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
                if (fileChannel != null) {
                    fileChannel.close();
                }
                if (fileChannelout != null) {
                    fileChannelout.close();
                }
            }

            if (file.isEmpty()) {
                logger.error(":上传空文件 -->>");
                return "EMPTY";
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("上传文件错误:" + e.getMessage());
            return "EXCEPTION";
        }
        return filePrefix+"/"+fileName;
    }

    @Override
    public void delFile(String fileName) {
        String path = filePath + "/" + fileName;
        File file = new File(path);
        if(file.exists()){
            file.delete();
        }
    }
}
