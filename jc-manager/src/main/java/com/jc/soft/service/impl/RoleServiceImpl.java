package com.jc.soft.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jc.soft.domain.*;
import com.jc.soft.mapper.MenuRoleInfoMapper;
import com.jc.soft.mapper.RoleInfoMapper;
import com.jc.soft.mapper.UserRoleInfoMapper;
import com.jc.soft.service.RoleService;
import com.jc.soft.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 角色接口实现
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleInfoMapper roleInfoMapper;

    @Autowired
    private MenuRoleInfoMapper menuRoleInfoMapper;

    @Autowired
    private UserRoleInfoMapper userRoleInfoMapper;

    @Autowired
    private UserService userService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int addRole(RoleInfo roleInfo) {

        roleInfo.setCreateTime(new Date());
        roleInfo.setUpdateTime(new Date());
        int insert = roleInfoMapper.insert(roleInfo);

        if(insert==1){
            Map<String, Object> map = new HashMap<>();
            map.put("roleId",roleInfo.getId());
            map.put("routeId",roleInfo.getRouteId());
            menuRoleInfoMapper.insertMR(map);
        }
        return insert;
    }
    @Override
    public Page<RoleInfo> getRoleList(RoleInfo roleInfo) {
        PageHelper.startPage(roleInfo.getCurrentPage(), roleInfo.getPageSize());
        Example example = new Example(RoleInfo.class);
        PageInfo<RoleInfo> pageInfo = new PageInfo<>(roleInfoMapper.selectByExample(example));
        pageInfo.getList().forEach(role->{
        Example menuRole = new Example(MenuRoleInfo.class);
            menuRole.createCriteria().andEqualTo("roleId",role.getId());
            List<MenuRoleInfo> menuRoleInfos = menuRoleInfoMapper.selectByExample(menuRole);
            List<String> collect = menuRoleInfos.stream().map(m -> m.getMenuId()+"").collect(Collectors.toList());
            role.setRouteId(String.join(",",collect));
        });
        return new Page<>(pageInfo.getTotal(),pageInfo.getList());
    }

    @Override
    public RoleInfo getRole(RoleInfo roleInfo) {
        Example example = new Example(RoleInfo.class);
        if(!StringUtils.isEmpty(roleInfo.getRoleName())){
            example.createCriteria().andEqualTo("roleName",roleInfo.getRoleName());
        }
        if(!StringUtils.isEmpty(roleInfo.getId())){
            example.createCriteria().andEqualTo("id",roleInfo.getId());
        }

        return roleInfoMapper.selectOneByExample(example);
    }

    @Override
    @Transactional
    public int updateRole(RoleInfo roleInfo) {

        roleInfo.setUpdateTime(new Date());

        Example roleExample = new Example(MenuRoleInfo.class);
        roleExample.createCriteria().andEqualTo("roleId",roleInfo.getId());
        menuRoleInfoMapper.deleteByExample(roleExample);

        Map<String, Object> map = new HashMap<>();
        map.put("roleId",roleInfo.getId());
        map.put("routeId",roleInfo.getRouteId());
        menuRoleInfoMapper.insertMR(map);

        return roleInfoMapper.updateByPrimaryKey(roleInfo);
    }

    @Transactional
    @Override
    public int delRole(String ids){

        /*UserRoleInfo userRoleInfo = new UserRoleInfo();
        userRoleInfo.setRoleId(id);
        int i = userRoleInfoMapper.selectCount(userRoleInfo);
        if(i>0){
            throw new Exception("该角色下有绑定用户，请删除用户或更换用户角色！");
        }
        roleInfoMapper.deleteByPrimaryKey(id);
        */
        List<String> list = Arrays.asList(ids.split(","));

        Example roleExample = new Example(RoleInfo.class);
        roleExample.createCriteria().andIn("id", list);
        roleInfoMapper.deleteByExample(roleExample);

        Example menuExample = new Example(MenuRoleInfo.class);
        menuExample.createCriteria().andIn("roleId",list);
        menuRoleInfoMapper.deleteByExample(menuExample);

        userService.delUserRoleByIds(1,list);
        return 1;
    }

}
