package com.jc.soft.service;

import com.jc.soft.domain.ArchivesInfo;
import com.jc.soft.domain.Page;

import java.util.List;

/**
 * 个人档案接口实现
 */
public interface ArchivesService {
    /**
     * 新增或更新档案
     * @param archivesInfo
     * @return
     */
   int addOrUpdateArchives(ArchivesInfo archivesInfo);

    /**
     * 获取档案信息
     * @param archivesInfo
     * @return
     */
    ArchivesInfo getArchives(ArchivesInfo archivesInfo);

    /**
     * 分页获取所有档案
     * @param archivesInfo
     * @return
     */
    Page<ArchivesInfo> getArchivesList(ArchivesInfo archivesInfo);

    /**
     * 更新status值（可只取id和status更新）
     * @param archivesInfo
     * @return
     */
    int updateArchivesStatus(ArchivesInfo archivesInfo);

    /**
     * 获取所有档案
     * @param archivesInfo
     * @return
     */
    List<ArchivesInfo> getAllArchivesList(ArchivesInfo archivesInfo);

}
