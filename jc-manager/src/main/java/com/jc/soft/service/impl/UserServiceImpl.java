package com.jc.soft.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jc.soft.domain.*;
import com.jc.soft.mapper.UserCachMapper;
import com.jc.soft.mapper.UserInfoMapper;
import com.jc.soft.mapper.UserRoleInfoMapper;
import com.jc.soft.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * 用户接口实现
 */
@Service
public class UserServiceImpl implements UserService {

    private final static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Autowired
    private UserCachMapper userCachMapper;

    @Autowired
    private UserRoleInfoMapper userRoleInfoMapper;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Example example = new Example(UserInfo.class);
        example.createCriteria().andEqualTo("userName",userName);

        UserInfo userInfo = null;
        try {
            userInfo =  userInfoMapper.selectOneByExample(example);
        } catch (Exception e) {
            logger.error("e======{}",e);
        }
        if(userInfo == null){
            return null;
        }
        return new UserInfoDetails(userInfo);
    }

    @Override
    public User getUserByName(String userName) {

        User user = new User();
        UserInfo userInfo = userCachMapper.getUserByName(userName);

        if(userInfo!=null){
            List<RoleInfo> roles = userCachMapper.getRolesByUserId(userInfo.getId());
            user.setUserInfo(userInfo);
            user.setRoles(roles);
        }
        return user;
    }

    @Override
    public UserInfo getUserById(Integer id) {
        return userInfoMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<UserInfo> getUserList(UserInfo userInfo) {

        PageHelper.startPage(userInfo.getCurrentPage(), userInfo.getPageSize());
        Example example = new Example(UserInfo.class);

        PageInfo<UserInfo> pageInfo = new PageInfo<>(userInfoMapper.selectByExample(example));
        List<UserInfo> list = pageInfo.getList();

        if(CollectionUtils.isEmpty(list)){
            return new Page<>(pageInfo.getTotal(),pageInfo.getList());
        }

        //获取角色信息
        List<Integer> userIds = new ArrayList<>();
        list.forEach(item->{
            userIds.add(item.getId());
        });
        Map<Integer, List<UserRoleInfo>> map = userRoleInfoMapper.getUserRole(userIds)
                .stream().collect(Collectors.groupingBy(UserRoleInfo::getUserId));

        for (UserInfo item : list) {
            if (map.containsKey(item.getId())) {
                List<UserRoleInfo> userRoleInfos = map.get(item.getId());
                item.setRoleIds(userRoleInfos.stream().map(UserRoleInfo::getRoleId).collect(Collectors.toList()));
                item.setRoleNames(userRoleInfos.stream().map(UserRoleInfo::getRoleName).collect(Collectors.toList()));
            }
        }

        return new Page<>(pageInfo.getTotal(),pageInfo.getList());
    }

    /**
     * 删除用户-角色关联表数据
     * @param list 角色id集合或用户id集合
     * @param type 角色id或用户id的判断，1角色，2用户
     */
    public void delUserRoleByIds(int type, List<String> list) {
        Example example = new Example(UserRoleInfo.class);
        // 根据角色id删除
        if (type == 1){
            example.createCriteria().andIn("roleId", list);
        }else{
            // 根据用户id删除
            example.createCriteria().andIn("userId", list);
        }
        userRoleInfoMapper.deleteByExample(example);
    }

    @Override
    @Transactional
    public int insertUser(UserInfo userInfo) {
        userInfo.setAvatar("https://superjing.top/manager/img/1.jpeg");
        userInfo.setPassWord(passwordEncoder.encode("123456"));
        int insert = userInfoMapper.insert(userInfo);
        //角色用户关联新增
        if(insert==1){
            List<Integer> roleIds = userInfo.getRoleIds();
            Map<String, Object> map = new HashMap<>();
            map.put("roleIds",roleIds);
            map.put("userId",userInfo.getId());
            userRoleInfoMapper.insertUR(map);
        }

        return insert;
    }

    @Override
    public int updateUser(UserInfo userInfo) {
        int i = userInfoMapper.updateByPrimaryKey(userInfo);
        return i;
    }

    @Override
    @Transactional
    public int updateUserAndRole(UserInfo userInfo) {
        int i = userInfoMapper.updateByPrimaryKey(userInfo);
        if(i==1){
            List<String> list = new ArrayList<>();
            list.add(userInfo.getId()+"");
            delUserRoleByIds(0,list);

            List<Integer> roleIds = userInfo.getRoleIds();
            Map<String, Object> map = new HashMap<>();
            map.put("roleIds",roleIds);
            map.put("userId",userInfo.getId());
            userRoleInfoMapper.insertUR(map);
        }
        return i;
    }

    @Override
    @Transactional
    public int delUsersByIds(String ids) {
        List<String> list = (List<String>) Arrays.asList(ids.split(","));
        Example example = new Example(UserInfo.class);
        example.createCriteria().andIn("id",list);

        delUserRoleByIds(0,list);

        return  userInfoMapper.deleteByExample(example);
    }

    @Override
    public int resetPassword(UserInfo userInfo) {
        String encode = passwordEncoder.encode("123456");
//        encode = "123456";
        userInfo.setPassWord(encode);

        int i = userInfoMapper.updateByPrimaryKey(userInfo);
        return i;
    }

}
