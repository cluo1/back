package com.jc.soft.service;

import com.google.gson.Gson;
import com.jc.soft.cach.CacheMap;
import com.jc.soft.config.CustomSpringConfigurator;
import com.jc.soft.contants.Constants;
import com.jc.soft.domain.MessageEntity;
import com.jc.soft.domain.MsgRecord;
import com.jc.soft.util.DateUtil;
import com.jc.soft.util.MessageEntityDecode;
import com.jc.soft.util.MessageEntityEncode;
import com.jc.soft.util.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Component
@ServerEndpoint(
    value = "/websocket/{id}",
    decoders = { MessageEntityDecode.class },
    encoders = { MessageEntityEncode.class },
    configurator = CustomSpringConfigurator.class
)
public class WebSocketServer {

    private final static Logger logger = LoggerFactory.getLogger(WebSocketServer.class);

    private Session session;
    private final Gson gson;
    private final RedisUtil redis;
    private static final Map<Integer, Session> WEBSOCKET_MAP = CacheMap.getInstance();

    @Value("${msg.expir.day:1}")
    private String msgExpirDay;

    @Autowired
    private MsgRecordService msgRecordService;

    @Autowired
    public WebSocketServer(Gson gson, RedisUtil redis) {
        this.gson = gson;
        this.redis = redis;
    }

    @OnOpen
    public void onOpen(@PathParam("id") Long id, Session session) {
         this.session = session;
         WEBSOCKET_MAP.put(id.intValue(), session);
    }

    @OnMessage
    public void onMessage(MessageEntity message) throws IOException {

        String msg = message.getMessage();
        //检测连接是否断开
        if("ping".equals(msg)){
            if (this.session != null) {
                this.session.getBasicRemote().sendText(gson.toJson(message));
            }
            return;
        }

        String flag = message.getFlag();
        String key = "";
        if("wxGroup".equals(flag) || "sysGroup".equals(flag)) {
            key = flag + "1";
        }else {
            key = LongStream.of(message.getFromId(), message.getToId())
                    .sorted()
                    .mapToObj(String::valueOf)
                    .collect(Collectors.joining("-"));
            key=flag+"-"+key;
        }

        logger.info("key========：{}==msg:{}",key,message);
        String formatDate = DateUtil.formatDate(new Date(), Constants.YYYYMMDD);
        String time = DateUtil.formatDate(message.getDate(), Constants.MMdd);
        message.setTime(time);
        String dayKey = key+"-" + formatDate;

        String val = redis.get(key) + "";
        if("null".equals(val)){
            redis.set(key, dayKey);
        }else if(!val.contains(dayKey)){
            redis.set(key,val+","+dayKey);
        }

        //按天存消息
        redis.setList(dayKey, message);
        //每天消息设置过期时间
        redis.setExpire(dayKey,60*60*24*Integer.valueOf(msgExpirDay));

        //私聊
        if("wxPrivate".equals(flag) || "sysPrivate".equals(flag)) {
            if (WEBSOCKET_MAP.get(message.getToId()) != null) {
                WEBSOCKET_MAP.get(message.getToId()).getBasicRemote().sendText(gson.toJson(message));
            } else {
                //不在线 设置未读消息
                Integer type = 0;
                if ("sysPrivate".equals(flag)) {
                    type = 1;
                }
                MsgRecord msgRecord = new MsgRecord(message.getFromId(), message.getToId(), 0, 1, 1, 0, type, new Date());
                msgRecordService.saveOrUpdateMsg(msgRecord);
            }
        }else if("cuService".equals(flag)){//客服聊天
            if (WEBSOCKET_MAP.get(message.getToId()) != null) {
                WEBSOCKET_MAP.get(message.getToId()).getBasicRemote().sendText(gson.toJson(message));
            } else {
                //不在线 设置未读消息
                MsgRecord msgRecord = new MsgRecord(message.getFromId(), message.getToId(), 0, 1, 1, 0, 2, new Date());
                msgRecordService.saveOrUpdateMsg(msgRecord);
            }

        }else {//群聊
            WEBSOCKET_MAP.forEach((k,v)->{
                try {
                    //群发消息过滤自己
                    if(!k.equals(message.getFromId())){
                        v.getBasicRemote().sendText(gson.toJson(message));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

        }
    }

    @OnClose
    public void onClose(Session session) {

        for (Map.Entry<Integer, Session> entry : WEBSOCKET_MAP.entrySet()) {
            if (session.getId().equals(entry.getValue().getId())) {
                logger.info("onClose====：{}",entry.getKey());
                WEBSOCKET_MAP.remove(entry.getKey());
                return;
            }
        }
    }

    @OnError
    public void onError(Throwable error) {
        error.printStackTrace();
    }

}
