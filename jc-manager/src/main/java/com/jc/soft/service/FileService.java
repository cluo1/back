package com.jc.soft.service;

import org.springframework.web.multipart.MultipartFile;

public interface FileService {
    String uploadFile(MultipartFile file);

    void delFile(String fileName);
}
