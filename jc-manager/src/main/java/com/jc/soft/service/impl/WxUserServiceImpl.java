package com.jc.soft.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jc.soft.domain.ArchivesInfo;
import com.jc.soft.domain.FavoritesInfo;
import com.jc.soft.domain.WxUserInfo;
import com.jc.soft.domain.Page;
import com.jc.soft.mapper.WxUserMapper;
import com.jc.soft.service.WxUserService;
import com.jc.soft.util.MapperUtils;
import com.jc.soft.util.OkHttpClientUtil;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class WxUserServiceImpl implements WxUserService {

    private final static Logger logger = LoggerFactory.getLogger(WxUserInfo.class);

    @Autowired
    private WxUserMapper wxUserMapper;
    @Autowired
    ArchivesServiceImpl archivesServiceImpl;
    @Autowired
    FavoritesServiceImpl favoritesServiceImpl;

    @Value("${subAppid}")
    private String subAppid;

    @Value("${secret}")
    private String secret;

    @Override
    public String getOpenid(String code, String source) {
        // 发送请求
        String url = "https://api.weixin.qq.com/sns/jscode2session?appid=" + subAppid + "&secret=" + secret + "&js_code=" + code + "&grant_type=authorization_code";
        Response wxResult = OkHttpClientUtil.getInstance().getData(url);
        String result = null;
        Map resultMap = null;
        try {
            result = wxResult.body().string();
            resultMap = MapperUtils.json2map(result);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultMap.get("openid").toString();
//        return "jingjing";
    }

    @Override
    public WxUserInfo addWxUser(WxUserInfo wxUserInfo) {
        Example example = new Example(WxUserInfo.class);
        example.createCriteria().andEqualTo("openid", wxUserInfo.getOpenid());
        WxUserInfo user = wxUserMapper.selectOneByExample(example);
        if(user != null){
            return user;
        }else {
            wxUserInfo.setCtime(new Date());
            wxUserInfo.setCustomer_service(0);
            wxUserMapper.insert(wxUserInfo);
            return  wxUserInfo;

        }
    }

    @Override
    public WxUserInfo findWxUser(WxUserInfo wxUserInfo) {
        String openid = getOpenid(wxUserInfo.getCode(),null);
        wxUserInfo.setOpenid(openid);

        Example example = new Example(WxUserInfo.class);
        example.createCriteria().andEqualTo("openid", openid);
        WxUserInfo user = wxUserMapper.selectOneByExample(example);
        if(user != null){
            wxUserInfo = user;
        }else {
            wxUserMapper.insert(wxUserInfo);
        }
        //获取关注量和头像
        getCollectNumByWxid(wxUserInfo);
        return  wxUserInfo;
    }

    @Override
    public Page<WxUserInfo> findWxUserList(WxUserInfo wxUserInfo) {
        PageHelper.startPage(wxUserInfo.getCurrentPage(), wxUserInfo.getPageSize());
        Example example = new Example(WxUserInfo.class);
        example.setOrderByClause(" ctime desc");
        PageInfo<WxUserInfo> pageInfo = new PageInfo<>(wxUserMapper.selectByExample(example));

        Page<WxUserInfo> wxUserPage = new Page<>(pageInfo.getTotal(), pageInfo.getList());
        return wxUserPage;
    }

    /**
     * 根据微信id查询被收藏数
     * @return
     */
    private void getCollectNumByWxid(WxUserInfo wxUserInfo){
        int count=0;
        ArchivesInfo a=new ArchivesInfo();
        a.setWxId(wxUserInfo.getId());
        ArchivesInfo b=archivesServiceImpl.getArchives(a);
        if(b!=null){
            List<FavoritesInfo> list=favoritesServiceImpl.getFavoritByArchId(b.getId());
            count=list.size();
        }
        wxUserInfo.setCollect(count);
    }

    /**
     * 新增或修改档案时更新微信表
     * @param wxUserInfo
     * @return
     */
    public int updateWxUser(WxUserInfo wxUserInfo) {
        return wxUserMapper.updateByPrimaryKeySelective(wxUserInfo);
    }
}
