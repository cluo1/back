package com.jc.soft.service;

import com.jc.soft.domain.Advertising;
import com.jc.soft.domain.Page;
import io.micrometer.core.instrument.search.Search;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public interface AdvertisingService {

    /**
     * 查询当前所有广告
     * @param advertising
     * @return
     */
    List<Advertising> selectAdvertising(Advertising advertising);

    Page<Advertising> getAdvertList(Advertising advertising);

    int addOrUpdateAdvertising(Advertising advertising);

    void delAdvertising(String ids);

}



