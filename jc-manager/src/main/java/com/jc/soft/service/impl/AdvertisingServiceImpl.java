package com.jc.soft.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jc.soft.domain.Advertising;
import com.jc.soft.domain.Page;
import com.jc.soft.mapper.AdvertisingMapper;
import com.jc.soft.service.AdvertisingService;
import com.jc.soft.service.FileService;
import org.assertj.core.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import tk.mybatis.mapper.entity.Example;
import java.util.Date;
import java.util.List;

/**
 * 个人档案接口实现
 */
@Service
public class AdvertisingServiceImpl implements AdvertisingService {

    @Autowired
    private AdvertisingMapper advertisingMapper;

    @Autowired
    private FileService fileService;

    /**
     * 查询当前所有广告
     * @param advertising
     * @return
     */
    @Override
    public List<Advertising> selectAdvertising(Advertising advertising) {
        Example example = new Example(Advertising.class);
        Example.Criteria criteria = example.createCriteria();
        if(!ObjectUtils.isEmpty(advertising.getStatus())){
            criteria.andEqualTo("status", advertising.getStatus());
        }
        example.setOrderByClause(" order_no asc");
        return advertisingMapper.selectByExample(example);
    }

    @Override
    public Page<Advertising> getAdvertList(Advertising advertising) {

        PageHelper.startPage(advertising.getCurrentPage(), advertising.getPageSize());

        Example example = new Example(Advertising.class);
        Example.Criteria criteria = example.createCriteria();
        if(!ObjectUtils.isEmpty(advertising.getStatus())){
            criteria.andEqualTo("status", advertising.getStatus());
        }
        example.setOrderByClause(" order_no asc");

        PageInfo<Advertising> pageInfo = new PageInfo<>(advertisingMapper.selectByExample(example));

        return new Page<>(pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public int addOrUpdateAdvertising(Advertising advertising) {

        if(advertising.getId()!=null && 0!=advertising.getId()){//修改
            return advertisingMapper.updateByPrimaryKey(advertising);
        }else{
            // 新增
            int maxOrderNo = advertisingMapper.getMaxOrderNo();
            advertising.setOrderNo(maxOrderNo);
            advertising.setCreateTime(new Date());
            return advertisingMapper.insert(advertising);
        }
    }


    @Override
    public void delAdvertising(String ids) {

        Example example = new Example(Advertising.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andIn("id", Arrays.asList(ids.split(",")));
        List<Advertising> advertisings = advertisingMapper.selectByExample(example);

        advertisingMapper.delAdvertising(ids);

        advertisings.forEach(item->{
            String fileName = item.getPicture().substring(item.getPicture().lastIndexOf("/")+1);
            fileService.delFile(fileName);
        });
    }

}
