package com.jc.soft.service;

import com.jc.soft.domain.MenuInfo;

import java.util.List;

/**
 * 菜单接口
 */
public interface MenuService {

    List<MenuInfo> getAllMenuByRole(List<Integer>  roleIds);

}
