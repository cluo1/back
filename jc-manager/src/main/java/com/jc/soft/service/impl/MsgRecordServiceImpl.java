package com.jc.soft.service.impl;

import com.jc.soft.domain.MsgRecord;
import com.jc.soft.mapper.MsgRecordMapper;
import com.jc.soft.service.MsgRecordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class MsgRecordServiceImpl implements MsgRecordService {

    private final static Logger logger = LoggerFactory.getLogger(MsgRecordServiceImpl.class);

    @Autowired
    private MsgRecordMapper msgRecordMapper;

    @Override
    public int saveOrUpdateMsg(MsgRecord msgRecord) {

        MsgRecord msg = msgRecordMapper.selectByPrimaryKey(msgRecord);
        if(msg !=null){
            msgRecord.setCount(msg.getCount()+1);
            msgRecord.setTotal(msg.getTotal()+1);
            Example example = new Example(MsgRecord.class);
            example.createCriteria().andEqualTo("fromId",msgRecord.getFromId())
                    .andEqualTo("toId",msgRecord.getToId()).andEqualTo("groupId",msg.getGroupId());
            return msgRecordMapper.updateByExampleSelective(msgRecord,example);
        }
        return msgRecordMapper.insert(msgRecord);
    }

    @Override
    public int updateMsg(MsgRecord msgRecord) {
        msgRecord.setGroupId(0);
        msgRecord.setCount(0);
        msgRecord.setStatus(1);
        Example example = new Example(MsgRecord.class);
        example.createCriteria().andEqualTo("fromId",msgRecord.getToId())
                .andEqualTo("toId",msgRecord.getFromId()).andEqualTo("type",msgRecord.getType());
        return msgRecordMapper.updateByExampleSelective(msgRecord,example);

    }

    @Override
    public List<MsgRecord> getMsgList(MsgRecord msgRecord) {
        return msgRecordMapper.getMsgList(msgRecord);
    }
}
