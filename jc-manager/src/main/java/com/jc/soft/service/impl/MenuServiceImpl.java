package com.jc.soft.service.impl;

import com.jc.soft.domain.MenuInfo;
import com.jc.soft.mapper.MenuInfoMapper;
import com.jc.soft.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 菜单接口实现
 */
@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuInfoMapper menuInfoMapper;

    @Override
    public List<MenuInfo> getAllMenuByRole(List<Integer> roleIds) {
        List<MenuInfo> tree = new ArrayList<>();

        List<MenuInfo> list = menuInfoMapper.getAllMenuByRole(roleIds);
        if(CollectionUtils.isEmpty(list)){
            return tree;
        }

        List<Integer> mId = list.stream().map(m -> m.getId()).collect(Collectors.toList());
        List<Integer> mPid = list.stream().map(m -> m.getPid()).distinct().collect(Collectors.toList());

        mId.addAll(mPid);
        List<MenuInfo> allMenuById = menuInfoMapper.getAllMenuById(mId);

        if (allMenuById != null && allMenuById.size() > 0) {
            // 返回的树状结构路由
            for (MenuInfo menu : allMenuById) {
                if (menu.getPid() != 0) {
                    continue;
                }
                menu.setChildren(new ArrayList<>());
                recursion(menu, menu.getChildren(), allMenuById);
                tree.add(menu);
            }
        }
        return tree;
    }

    /**
     * 构建树结构，追加子节点数据
     * @param rootMenu 根节点
     * @param list 所有菜单节点
     */
    private void recursion(MenuInfo rootMenu, List<MenuInfo> children, List<MenuInfo> list) {
        // 添加子节点
        for (MenuInfo menuChild : list) {
            // 判断是否是子权限
            if (rootMenu.getId().equals(menuChild.getPid())){
                children.add(menuChild);
                // 递归调用
                menuChild.setChildren(new ArrayList<>());
                recursion(menuChild, menuChild.getChildren(), list);
            }
        }
    }

}
