package com.jc.soft.service;

import com.jc.soft.domain.UserTest;
import com.jc.soft.mapper.UserTestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * UserService
 *
 * @author zjw
 * @createTime 2021/1/23 21:33
 */
@Service
public class UserServiceTest {

    @Autowired
    private UserTestMapper  userTestMapper;

    public UserTest findById(Long uid) {
        UserTest u = userTestMapper.selectByPrimaryKey(uid);
        return u;
    }

    public UserTest findByUsernameAndPassword(String username, String password) {
        Example example = new Example(UserTest.class);
        example.createCriteria().andEqualTo("username",username)
                .andEqualTo("password",password);
         return userTestMapper.selectOneByExample(example);
    }

    public List<UserTest> getFriends(Long uid) {
        return LongStream.of(1L, 2L, 3L, 4L)
                .filter(item -> item != uid)
                .mapToObj(this::findById)
                .collect(Collectors.toList());
    }

}
