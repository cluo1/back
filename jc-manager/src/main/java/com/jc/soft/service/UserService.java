package com.jc.soft.service;

import com.jc.soft.domain.Page;
import com.jc.soft.domain.User;
import com.jc.soft.domain.UserInfo;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;


/**
 * 用户接口
 */

public interface UserService extends UserDetailsService {

    User getUserByName(String userName);

    void delUserRoleByIds(int type, List<String> list);

    Page<UserInfo> getUserList(UserInfo userInfo);

    UserInfo getUserById(Integer id);

    int insertUser(UserInfo userInfo);

    int updateUserAndRole(UserInfo userInfo);

    int updateUser(UserInfo userInfo);

    int delUsersByIds(String ids);

    int resetPassword(UserInfo userInfo);

}
