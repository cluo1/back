package com.jc.soft.service.impl;

import com.alibaba.excel.util.CollectionUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jc.soft.domain.ArchivesInfo;
import com.jc.soft.domain.Page;
import com.jc.soft.domain.WxUserInfo;
import com.jc.soft.mapper.ArchivesInfoMapper;
import com.jc.soft.service.ArchivesService;
import com.jc.soft.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import tk.mybatis.mapper.entity.Example;
import com.jc.soft.contants.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 个人档案接口实现
 */
@Service
public class ArchivesServiceImpl implements ArchivesService {

    @Autowired
    private ArchivesInfoMapper archivesInfoMapper;
    @Autowired
    private WxUserServiceImpl wxUserServiceImpl;

    private static SimpleDateFormat sdf = new SimpleDateFormat(Constants.YYYY_MM_DD);

    @Override
    public int addOrUpdateArchives(ArchivesInfo archivesInfo) {
        if(archivesInfo.getStatus()==null){
            archivesInfo.setStatus("0");
        }
        Example example = new Example(ArchivesInfo.class);
        example.createCriteria().andEqualTo("wxId", archivesInfo.getWxId());
        List<ArchivesInfo> list = archivesInfoMapper.selectByExample(example);
        if(!CollectionUtils.isEmpty(list)){
            Integer id = list.get(0).getId();
            archivesInfo.setId(id);
            archivesInfo.setUpdateTime(new Date());
            archivesInfoMapper.updateByPrimaryKeySelective(archivesInfo);
        }else{
            archivesInfo.setCreateTime(new Date());
            archivesInfo.setUpdateTime(new Date());
            archivesInfoMapper.insert(archivesInfo);
        }

        //更新微信表姓名和头像
        WxUserInfo wxUserInfo=new WxUserInfo();
        wxUserInfo.setId(archivesInfo.getWxId());
        wxUserInfo.setNickName(archivesInfo.getName());
        wxUserInfo.setArchId(archivesInfo.getId());
        if(!"".equals(archivesInfo.getPicture())){
            wxUserInfo.setAvatarUrl(archivesInfo.getPicture());
        }
        return wxUserServiceImpl.updateWxUser(wxUserInfo);
    }

    @Override
    public ArchivesInfo getArchives(ArchivesInfo archivesInfo) {
        Example example = new Example(ArchivesInfo.class);
        Example.Criteria criteria = example.createCriteria();
        if(!ObjectUtils.isEmpty(archivesInfo.getWxId())){
            criteria.andEqualTo("wxId", archivesInfo.getWxId());
        }
        if(!ObjectUtils.isEmpty(archivesInfo.getId())){
            criteria.andEqualTo("id", archivesInfo.getId());
        }
        if(!ObjectUtils.isEmpty(archivesInfo.getStatus())){
            criteria.andEqualTo("status", archivesInfo.getStatus());
        }
        return archivesInfoMapper.selectOneByExample(example);
    }

    @Override
    public Page<ArchivesInfo> getArchivesList(ArchivesInfo archivesInfo) {
        PageHelper.startPage(archivesInfo.getCurrentPage(), archivesInfo.getPageSize());
        Example example = new Example(ArchivesInfo.class);
        Example.Criteria criteria = example.createCriteria();

        if(!ObjectUtils.isEmpty(archivesInfo.getWxId())){
            criteria.andEqualTo("wxId", archivesInfo.getWxId());
        }
        if(!ObjectUtils.isEmpty(archivesInfo.getId())){
            criteria.andEqualTo("id", archivesInfo.getId());
        }
        if(!ObjectUtils.isEmpty(archivesInfo.getStatus())){
            criteria.andEqualTo("status", archivesInfo.getStatus());
        }
        example.setOrderByClause(" update_time desc");
        PageInfo<ArchivesInfo> pageInfo = new PageInfo<>(archivesInfoMapper.selectByExample(example));
        return new Page<ArchivesInfo>(pageInfo.getTotal(),pageInfo.getList());
    }

    @Override
    public int updateArchivesStatus(ArchivesInfo archivesInfo) {
        Example example = new Example(ArchivesInfo.class);
        example.createCriteria().andEqualTo("id", archivesInfo.getId());

        ArchivesInfo ar = new ArchivesInfo();
        ar.setStatus(archivesInfo.getStatus());
        return archivesInfoMapper.updateByExampleSelective(archivesInfo,example);
    }

    @Override
    public List<ArchivesInfo> getAllArchivesList(ArchivesInfo archivesInfo) {
        Example example = new Example(ArchivesInfo.class);
        Example.Criteria criteria = example.createCriteria();
        if(!ObjectUtils.isEmpty(archivesInfo.getWxId())){
            criteria.andEqualTo("wxId", archivesInfo.getWxId());
        }
        if(!ObjectUtils.isEmpty(archivesInfo.getSalary())){
            criteria.andEqualTo("salary", archivesInfo.getSalary());
        }
        if(!ObjectUtils.isEmpty(archivesInfo.getEducation())){
            criteria.andEqualTo("education", archivesInfo.getEducation());
        }
        if(!ObjectUtils.isEmpty(archivesInfo.getStatus())){
            criteria.andEqualTo("status", archivesInfo.getStatus());
        }
        if(!ObjectUtils.isEmpty(archivesInfo.getLocation())){
            criteria.andLike("location","%"+archivesInfo.getLocation()+"%");
        }

       if(!ObjectUtils.isEmpty(archivesInfo.getAge1()) && !ObjectUtils.isEmpty(archivesInfo.getAge2())){
           Date birthday1 = DateUtil.getBirthdayByAge(archivesInfo.getAge1(), 0);
           Date birthday2 = DateUtil.getBirthdayByAge(archivesInfo.getAge2(), 1);

           criteria.andBetween("birthDate",birthday2,birthday1);
       }

       if(!ObjectUtils.isEmpty(archivesInfo.getAge1()) && ObjectUtils.isEmpty(archivesInfo.getAge2())){
            Date birthday1 = DateUtil.getBirthdayByAge(archivesInfo.getAge1(), 0);
            criteria.andLessThanOrEqualTo("birthDate",birthday1);
        }
        if(ObjectUtils.isEmpty(archivesInfo.getAge1()) && !ObjectUtils.isEmpty(archivesInfo.getAge2())){
            Date birthday2 = DateUtil.getBirthdayByAge(archivesInfo.getAge2(), 1);
            criteria.andGreaterThanOrEqualTo("birthDate",birthday2);
        }

        if(!ObjectUtils.isEmpty(archivesInfo.getStatus())){
            criteria.andEqualTo("sex", archivesInfo.getSex());
        }
        example.setOrderByClause(" update_time desc");
        List<ArchivesInfo> list=archivesInfoMapper.selectByExample(example);
        if(list!=null&&list.size()>0){
            for (int i = 0; i <list.size() ; i++) {
                String birthday=sdf.format(list.get(i).getBirthDate());
                int age= DateUtil.getAgeByBirthday(birthday);
                list.get(i).setAge1(age);
            }
        }
        return list;
    }

    
}
