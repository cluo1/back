package com.jc.soft.service.impl;

import com.jc.soft.contants.Constants;
import com.jc.soft.domain.ArchivesInfo;
import com.jc.soft.domain.FavoritesInfo;
import com.jc.soft.mapper.FavoritesInfoMapper;
import com.jc.soft.service.FavoritesService;
import com.jc.soft.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 收藏夹接口实现
 */
@Service
public class FavoritesServiceImpl implements FavoritesService {

    private static SimpleDateFormat sdf = new SimpleDateFormat(Constants.YYYY_MM_DD);

    @Autowired
    private FavoritesInfoMapper mapper;
    @Autowired
    private ArchivesServiceImpl archivesServiceImpl;
    /**
     * 新增收藏
     * @param favoritesInfo
     * @return
     */
    @Override
    public int addFavorites(FavoritesInfo favoritesInfo){
        return mapper.insert(favoritesInfo);
    }

    /**
     * 取消收藏
     * @param favoritesInfo
     * @return
     */
    @Override
    public int delFavorites(FavoritesInfo favoritesInfo){
        Example example = new Example(FavoritesInfo.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("wxid", favoritesInfo.getWxid());
        criteria.andEqualTo("archid", favoritesInfo.getArchid());
        return mapper.deleteByExample(example);
     }

    /**
     * 根据微信id和档案id查询收藏信息
     * @param wxId
     * @param archId
     * @return
     */
    @Override
    public FavoritesInfo getFavorites(int wxId, int archId) {
        Example example = new Example(FavoritesInfo.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("wxid", wxId);
        criteria.andEqualTo("archid", archId);
        return mapper.selectOneByExample(example);
    }

    /**
     * 获取所有我收藏的和收藏我的人
     * @param favoritesInfo
     * @return
     */
    @Override
    public List<ArchivesInfo> getFavoritList(FavoritesInfo favoritesInfo) {
        List<ArchivesInfo> list=new ArrayList<>();
        /*List<FavoritesInfo> fList;
        ArchivesInfo a=new ArchivesInfo();*/
        //我收藏的人
        if(favoritesInfo.getType()==1){
           /* fList=getFavoritByWxid(favoritesInfo.getWxid());
            if(fList!=null&&fList.size()>0){
                for (int i = 0; i <fList.size() ; i++) {
                    a.setId(fList.get(i).getArchid());
                    ArchivesInfo archivesInfo=archivesServiceImpl.getArchives(a);
                    alist.add(archivesInfo);
                }
            }*/
            list = mapper.getMeFavorite(favoritesInfo.getWxid());
            format(list);
        }else if(favoritesInfo.getType()==2){//收藏我的人
            /*a.setWxId(favoritesInfo.getWxid());
            ArchivesInfo b=archivesServiceImpl.getArchives(a);
            if(b!=null){
                fList= getFavoritByArchId(b.getId());
                if(fList!=null&&fList.size()>0){
                    for (int i = 0; i <fList.size() ; i++) {
                        a.setWxId(fList.get(i).getWxid());
                        alist=archivesServiceImpl.getAllArchivesList(a);
                    }
                }
            }*/
            list = mapper.getFavoriteMe(favoritesInfo.getWxid());
            format(list);
        }
        return list;
    }

    @Override
    public List<FavoritesInfo> getFavoritByWxid(int wxId) {
        Example example = new Example(FavoritesInfo.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("wxid", wxId);
        return mapper.selectByExample(example);
    }

    @Override
    public List<FavoritesInfo> getFavoritByArchId(int archid) {
        Example example = new Example(FavoritesInfo.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("archid", archid);
        return mapper.selectByExample(example);
    }


    private void format(List<ArchivesInfo> list){
        if(list!=null&&list.size()>0){
            for (int i = 0; i <list.size() ; i++) {
                String birthday=sdf.format(list.get(i).getBirthDate());
                int age= DateUtil.getAgeByBirthday(birthday);
                list.get(i).setAge1(age);
            }
        }
    }
}


