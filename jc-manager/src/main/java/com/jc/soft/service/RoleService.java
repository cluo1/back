package com.jc.soft.service;

import com.jc.soft.domain.Page;
import com.jc.soft.domain.RoleInfo;

/**
 * 角色管理接口
 */
public interface RoleService {

    int addRole(RoleInfo roleInfo);

    Page<RoleInfo> getRoleList(RoleInfo roleInfo);

    RoleInfo getRole(RoleInfo roleInfo);

    int updateRole(RoleInfo roleInfo);

    int delRole(String ids) ;


}
