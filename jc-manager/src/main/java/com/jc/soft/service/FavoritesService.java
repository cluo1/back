package com.jc.soft.service;

import com.jc.soft.domain.ArchivesInfo;
import com.jc.soft.domain.FavoritesInfo;

import java.util.List;

/**
 * 收藏夹接口实现
 */
public interface FavoritesService {
    /**
     * 新增收藏
     * @param favoritesInfo
     * @return
     */
   int addFavorites(FavoritesInfo favoritesInfo);

    /**
     * 取消收藏
     * @param favoritesInfo
     * @return
     */
   int delFavorites(FavoritesInfo favoritesInfo);

    /**
     * 查找数据
     * @param wxId
     * @param archId
     * @return
     */
    FavoritesInfo getFavorites(int wxId,int archId);

    /**
     * 获取收藏的档案信息
     * @param favoritesInfo
     * @return
     */
    List<ArchivesInfo> getFavoritList(FavoritesInfo favoritesInfo);

    /**
     * 通过微信id获取所有收藏信息
     * @param wxId
     * @return
     */
    List<FavoritesInfo> getFavoritByWxid(int wxId);
    /**
     * 通过档案d获取所有被收藏信息
     * @param archId
     * @return
     */
    List<FavoritesInfo> getFavoritByArchId(int archId);
}
