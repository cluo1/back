package com.jc.soft.util;

import com.jc.soft.contants.Constants;
import org.apache.ibatis.reflection.ExceptionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateUtil {

    private static SimpleDateFormat sdf = new SimpleDateFormat(Constants.YYYY_MM_DD);
    private static Logger logger =  LoggerFactory.getLogger(DateUtil.class);

    public static List<String> getDateDaysByMonth(Date month){
        List<String> result  = new ArrayList<>();
        Calendar c = Calendar.getInstance();
        c.setTime(month);
        int val_max = c.getActualMaximum(Calendar.DAY_OF_MONTH);
        int val_min  = c.getActualMinimum(Calendar.DAY_OF_MONTH);

        while (val_min<=val_max){
            result.add(val_min+"");
            val_min++;
        }
        return result;
    }
    /**
     * 根据传入的参数，来对日期区间进行拆分，返回拆分后的日期List
     * @param type 报表类型，1年度，2月份
     * @param start 开始时间
     * @param end 结束时间
     * @return 时间集合
     * @throws ParseException
     */
    public static List<String> doDateByStatisticsType(Integer type, Date start, Date end) throws ParseException {
        List<String> list = new ArrayList<>();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
        if (type == 1) dateFormat = new SimpleDateFormat("yyyy");
        Calendar sCalendar = Calendar.getInstance();
        sCalendar.setTime(start);

        Calendar eCalendar = Calendar.getInstance();
        eCalendar.setTime(end);

        if (type == 1) { // 年度拆分
            while(sCalendar.getTime().getTime()<eCalendar.getTime().getTime()){
                list.add(dateFormat.format(sCalendar.getTime()));
                sCalendar.add(Calendar.YEAR, 1);
            }
            list.add(dateFormat.format(eCalendar.getTime()));
        } else if (type == 2){ // 月度拆分
            while(sCalendar.getTime().getTime()<eCalendar.getTime().getTime()){
                list.add(dateFormat.format(sCalendar.getTime()));
                sCalendar.add(Calendar.MONTH, 1);
            }
            list.add(dateFormat.format(eCalendar.getTime()));
        }
        return list;
    }

    /**
     * 根据出生年月计算年龄
     *
     * @param birthday(可以为yyyy yyyy-MM yyyy-MM-dd)
     * @return返回年龄
     */
    public static Integer getAgeByBirthday(String birthday) {
        if ("".equals(birthday) || birthday == null) {
            return null;
        }
        String nowDate = sdf.format(new Date());
        String[] nowDates = nowDate.split("-");// 当前时间
        String[] dates = birthday.split("-");// 分割时间线 - 年[0],月[1],日[2]
        int age = Integer.parseInt(nowDates[0]) - Integer.parseInt(dates[0]);// 年龄默认为当前时间和生日相减
       /* if (dates.length >= 2) {// 根据月推算出年龄是否需要增加
            // 如果当前月份大于生日月份，岁数不变，否则加一
            Integer nowMonth = Integer.parseInt(nowDate.substring(5, 7));
            Integer birthMonth = Integer.parseInt(birthday.substring(5, 7));
            if (nowMonth < birthMonth)
                age++;
            if (dates.length >= 3 && nowMonth == birthMonth) {// 月份相同才计算对应年龄
                // 如果天数大于当前生日月份,岁数不变,否则加一
                Integer nowDay = Integer.parseInt(nowDates[2]);// 当前天数
                Integer birDay = Integer.parseInt(dates[2]);// 生日天数
                if (nowDay < birDay)
                    age++;
            }
        }*/
        return age<=0?1:age;
    }


    /**
     *  根据年龄获取时间戳（开始年龄key取0，返回一年最后一秒时间戳，时间戳大；反之结束年龄获取一年初始时间戳）
     *
     * @param age 年龄
     * @param key 判断键，0-年龄小，1-年龄大
     *
     * @return 时间戳
     */
    public static Date getBirthdayByAge(Integer age, Integer key) {

        Date d = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        c.add(Calendar.YEAR, 0 - age);
        Date birthDate1 = c.getTime();
        if (key == 0) {
            c.set(Calendar.MONTH, 11);//十二月
            c.set(Calendar.DATE, 31);
            c.set(Calendar.HOUR, 23);
            c.set(Calendar.MINUTE, 59);
            c.set(Calendar.SECOND, 59);
        } else {
            c.set(Calendar.MONTH, 0);//一月
            c.set(Calendar.DATE, 1);
            c.set(Calendar.HOUR, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
        }

        Date birthDate = c.getTime();

        return birthDate;
    }


    public static Integer getWeekOfYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTime(date);
        int week = calendar.get(Calendar.WEEK_OF_YEAR);
        int mouth = calendar.get(Calendar.MONTH);
        // JDK think 2015-12-31 as 2016 1th week
        if (mouth >= 11 && week <= 1) {
            week += 52;
        }
        return week;
    }

    public static Date getDateByTimestamps(long timestamps) {
        return new Date(timestamps);
    }

    public static Calendar getCalendar(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    public static String getField(Calendar calendar, int fieldInt) {
        int value = 0;
        if (fieldInt == Calendar.MONTH) {
            value = calendar.get(fieldInt) + 1;
        } else {
            value = calendar.get(fieldInt);
        }

        if (value < 10) {
            return "0" + value;
        }
        return value + "";
    }

    public static String getNextMonth(Date dateNow,Integer afterMonth) {
        String result = "";
        Calendar cl = Calendar.getInstance();
        cl.setTime(dateNow);
        Integer month = cl.get(Calendar.MONTH) + 1 + afterMonth;
        while (month > 12){
            month = month - 12;
        }
        if (month <= 0){
            month += 12;
        }
        if (month < 10){
            result =  "0" + month;
        }else {
            result =  month + "";
        }
        return result;
    }

    public static String formatDate(Date date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

    public static String getQuarter(Calendar calendar) {
        int month = calendar.get(Calendar.MONTH);
        if (month == 1 || month == 2 || month == 3) {
            return "1";
        } else if (month == 4 || month == 5 || month == 6) {
            return "2";
        } else if (month == 7 || month == 8 || month == 9) {
            return "3";
        } else {
            return "4";
        }
    }

    public static long dateToTimestamp(String dateStr, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date date = null;
        try {
            date = sdf.parse(dateStr);
        } catch (ParseException e) {
        }
        return date.getTime();
    }

    public static TimeTerm getTimeTerm(long timestamp) throws Exception {

        Date date = new Date(timestamp);
//		String dayStr = simpleDateFormat.format(date);
        String dayStr = getSdf(DAY_FORMAT).format(date);
        String beginDateStr = dayStr + BEGIN;
        String endDateStr = dayStr + END;
        TimeTerm timeTerm = new TimeTerm();
//		timeTerm.beginTime = simpleDateFormatDay.parse(beginDateStr).getTime();
//		timeTerm.endTime = simpleDateFormatDay.parse(endDateStr).getTime();
        timeTerm.beginTime = getSdf(TIME_FORMAT).parse(beginDateStr).getTime();
        timeTerm.endTime = getSdf(TIME_FORMAT).parse(endDateStr).getTime();
        timeTerm.dateStr = dayStr;
        timeTerm.yearMonth = dayStr.substring(0, 4) + dayStr.substring(5, 7);
//		timeTerm.dayDate = simpleDateFormat.parse(dayStr);
        timeTerm.dayDate = getSdf(DAY_FORMAT).parse(dayStr);
        return timeTerm;
    }

    public static long dateDayStrToLong(String dateStr) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TIME_FORMAT);
        return simpleDateFormat.parse(dateStr).getTime();
    }

    // 根据日期生成文件夹相对路径
    public static String dateToFilePathStr(Date date) throws ParseException {
        Calendar now = Calendar.getInstance();
        now.setTime(date);
        return File.separator + now.get(Calendar.YEAR) + File.separator + DateUtil.getField(now, Calendar.MONTH)
                + File.separator + now.get(Calendar.DAY_OF_MONTH);
    }

    // 根据日期生成文件夹相对路径
    public static String dateToFilePathStrBak(int backDay) throws ParseException {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.DAY_OF_MONTH, backDay);
        return now.get(Calendar.YEAR) + DateUtil.getField(now, Calendar.MONTH) + DateUtil.getField(now, Calendar.DAY_OF_MONTH);
    }

    /**
     * 获取当前时间按周字段 202018
     *
     * @return
     */
    public static String getPartitionField() {
        Date dateNow = new Date();
        Calendar cl = Calendar.getInstance();
        cl.setTime(dateNow);
        cl.add(Calendar.DAY_OF_YEAR, -1);
        Date date = cl.getTime();
        String weekOfYear = DateUtil.getWeekOfYear(date).toString();
        weekOfYear = weekOfYear.length() < 2 ? "0" + weekOfYear : weekOfYear;
        return DateUtil.getField(cl, Calendar.YEAR) + weekOfYear;
    }

    public static String getMonth() {
        Date dateNow = new Date();
        Calendar cl = Calendar.getInstance();
        cl.setTime(dateNow);
        Integer month = cl.get(Calendar.MONTH) + 1;
        if (month < 10){
            return "0" + month;
        }else {
            return month + "";
        }
    }

    public static String getMonth(Date dateNow) {
        Calendar cl = Calendar.getInstance();
        cl.setTime(dateNow);
        Integer month = cl.get(Calendar.MONTH) + 1;
        if (month < 10){
            return "0" + month;
        }else {
            return month + "";
        }
    }


    /**
     * 常规关联任务需要获取当前时间前天的0~24点时间戳
     * @param day
     * @return
     * @throws ParseException
     */
    public static TimeTerm getCallTimeRangeTerm(Date nowD,Integer day) throws ParseException {
        Calendar now = Calendar.getInstance();
        now.setTime(nowD);
        now.set(Calendar.DATE, now.get(Calendar.DATE) - day);
        nowD = now.getTime();
        String startDStr = getSdf(DAY_FORMAT).format(nowD);

        String beginDateStr = startDStr + BEGIN;
        String endDateStr = startDStr + END;
        TimeTerm timeTerm = new TimeTerm();
        timeTerm.beginTime = getSdf(TIME_FORMAT).parse(beginDateStr).getTime();
        timeTerm.endTime = getSdf(TIME_FORMAT).parse(endDateStr).getTime();
        timeTerm.dateStr = startDStr;
        return timeTerm;
    }


    public static String getMonthByDay(Date nowD,Integer day) throws ParseException {
        Calendar now = Calendar.getInstance();
        now.setTime(nowD);
        now.set(Calendar.DATE, now.get(Calendar.DATE) - day);
        Integer month = now.get(Calendar.MONTH) + 1;
        if (month < 10){
            return "0" + month;
        }else {
            return month + "";
        }
    }


    /**
     * 根据时间获取TimeTerm
     * @param day
     * @return
     * @throws ParseException
     */
    public static TimeTerm getCallTimeRangeTermBak(Date date,Integer day) throws ParseException {
        // 根据开始时间往前推 day 天
        Calendar now = Calendar.getInstance();
        now.setTime(date);
        now.set(Calendar.DATE, now.get(Calendar.DATE) - day);
        date = now.getTime();
        String startDStr = getSdf(DAY_FORMAT).format(date);

        String beginDateStr = startDStr + BEGIN;
        String endDateStr = startDStr + END;
        TimeTerm timeTerm = new TimeTerm();
        timeTerm.beginTime = getSdf(TIME_FORMAT).parse(beginDateStr).getTime();
        timeTerm.endTime = getSdf(TIME_FORMAT).parse(endDateStr).getTime();
        timeTerm.dateStr = startDStr;
        return timeTerm;
    }

    /**
     * 根据时间(yyyy-MM-dd)获取TimeTerm
     * @param day
     * @return
     * @throws ParseException
     */
    public static TimeTerm getCallTimeRangeTermByString(String date,Integer day) throws ParseException {
        // 根据开始时间往前推 day 天
        Date dateTime = getSdf(DAY_FORMAT).parse(date);
        Calendar now = Calendar.getInstance();
        now.setTime(dateTime);
        now.set(Calendar.DATE, now.get(Calendar.DATE) - day);
        dateTime = now.getTime();
        String startDStr = getSdf(DAY_FORMAT).format(dateTime);

        String beginDateStr = startDStr + BEGIN;
        String endDateStr = startDStr + END;
        TimeTerm timeTerm = new TimeTerm();
        timeTerm.beginTime = getSdf(TIME_FORMAT).parse(beginDateStr).getTime();
        timeTerm.endTime = getSdf(TIME_FORMAT).parse(endDateStr).getTime();
        timeTerm.dateStr = startDStr;
        return timeTerm;
    }

    /**
     * 获取关联的开始和结束时间
     *
     * @param start
     * @param end
     * @param day
     * @return
     * @throws ParseException
     */
    public static TimeTerm getTimeRangeTerm(long start, long end, int day) throws ParseException {
        // 根据开始时间往前推 day 天
        Date startD = new Date(start);
        Calendar now = Calendar.getInstance();
        now.setTime(startD);
        now.set(Calendar.DATE, now.get(Calendar.DATE) - day);
        startD = now.getTime();
//		String startDStr = simpleDateFormat.format(startD);
        String startDStr = getSdf(DAY_FORMAT).format(startD);
        // 结束时间
        Date endE = new Date(end);
        String endEStr = getSdf(DAY_FORMAT).format(endE);

        String beginDateStr = startDStr + BEGIN;
        String endDateStr = endEStr + END;
        TimeTerm timeTerm = new TimeTerm();
//		timeTerm.beginTime = simpleDateFormatDay.parse(beginDateStr).getTime();
//		timeTerm.endTime = simpleDateFormatDay.parse(endDateStr).getTime();
        timeTerm.beginTime = getSdf(TIME_FORMAT).parse(beginDateStr).getTime();
        timeTerm.endTime = getSdf(TIME_FORMAT).parse(endDateStr).getTime();
        return timeTerm;
    }

    public static Date convertString2Date(String dateStr) throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TIME_FORMAT);
        Date date = simpleDateFormat.parse(dateStr);
        return date;
    }

    public static String convertLong2String(Long date) throws Exception {
        return getSdf(TIME_FORMAT).format(date);
    }

    public static String convertDate2String(Date date) throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TIME_FORMAT);
        return simpleDateFormat.format(date);
    }


    /** 锁对象 */
    private static final Object lockObj = new Object();

    /** 存放不同的日期模板格式的sdf的Map */
    private static Map<String, ThreadLocal<SimpleDateFormat>> sdfMap = new HashMap<String, ThreadLocal<SimpleDateFormat>>();

    /**
     * 返回一个ThreadLocal的sdf,每个线程只会new一次sdf
     *
     * @param pattern
     * @return
     */
    public static SimpleDateFormat getSdf(final String pattern) {
        ThreadLocal<SimpleDateFormat> tl = sdfMap.get(pattern);
        synchronized (lockObj) {
            tl = sdfMap.get(pattern);
            if (tl == null) {
                tl = new ThreadLocal<SimpleDateFormat>() {
                    @Override
                    protected SimpleDateFormat initialValue() {
                        return new SimpleDateFormat(pattern);
                    }
                };
                sdfMap.put(pattern, tl);
            }
        }
        return tl.get();
    }

    public static final String DAY_FORMAT = "yyyy-MM-dd";
    public static final String SIM_DAY_FORMAT = "yyyyMMdd";
    public static final String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String SIM_TIME_FORMAT = "yyyyMMdd HH:mm:ss";
    public static final String BEGIN = " 00:00:00";
    public static final String END = " 23:59:59";
//	public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DAY_FORMAT);
//	public static SimpleDateFormat simDateFormat = new SimpleDateFormat(SIM_DAY_FORMAT);
//	public static SimpleDateFormat simpleDateFormatDay = new SimpleDateFormat(TIME_FORMAT);
//	public static SimpleDateFormat simFormatDay = new SimpleDateFormat(SIM_TIME_FORMAT);

//	public static void main(String[] args) {
//		System.out.println(new Date(1444428548348L));
//		try {
//			TimeTerm timeTerm = getTimeTerm(1396449503000L);
//			System.out.println(timeTerm.beginTime);
//			System.out.println(timeTerm.endTime);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

    public static synchronized String getDateFormat(Date date) {
        return getSdf("yyyy.MM.dd HH:mm:ss").format(date);
    }

    public static Date getBeforeDayBegin(Integer before,String time){
        try {
            String today =  getSdf(SIM_DAY_FORMAT).format(new Date());
            Calendar calendar = new GregorianCalendar();
            Date date = new Date();
            try {
                date = getSdf(SIM_DAY_FORMAT).parse(today);
            } catch (Exception e) {
                e.printStackTrace();
            }
            calendar.setTime(date);
            //把日期往后增加一天.整数往后推,负数往前移动
            calendar.add(calendar.DATE, -before);
            //这个时间就是日期往后推一天的结果
            String beginTime = getSdf(DAY_FORMAT).format(calendar.getTime()) + " " + time;
            Date result = getSdf(TIME_FORMAT).parse(beginTime);
            return result;
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            return null;
        }
    }

    public static Date getYesterday(){
        String today =  getSdf(SIM_DAY_FORMAT).format(new Date());
        Calendar calendar = new GregorianCalendar();
        Date date = new Date();
        try {
            date = getSdf(SIM_DAY_FORMAT).parse(today);
        } catch (Exception e) {
            e.printStackTrace();
        }
        calendar.setTime(date);
        //把日期往后增加一天.整数往后推,负数往前移动
        calendar.add(calendar.DATE, -1);
        //这个时间就是日期往后推一天的结果
        Date result = calendar.getTime();
        return result;
    }

    public static String getYesterdayBak(){
        String today =  getSdf(SIM_DAY_FORMAT).format(new Date());
        Calendar calendar = new GregorianCalendar();
        Date date = new Date();
        try {
            date = getSdf(SIM_DAY_FORMAT).parse(today);
        } catch (Exception e) {
            e.printStackTrace();
        }
        calendar.setTime(date);
        //把日期往后增加一天.整数往后推,负数往前移动
        calendar.add(calendar.DATE, -1);
        //这个时间就是日期往后推一天的结果
        Date result = calendar.getTime();
        return  getSdf(SIM_DAY_FORMAT).format(result);
    }

    public static String getBeforeDay(Integer before){
        String today =  getSdf(SIM_DAY_FORMAT).format(new Date());
        Calendar calendar = new GregorianCalendar();
        Date date = new Date();
        try {
            date = getSdf(SIM_DAY_FORMAT).parse(today);
        } catch (Exception e) {
            e.printStackTrace();
        }
        calendar.setTime(date);
        //把日期往后增加一天.整数往后推,负数往前移动
        calendar.add(calendar.DATE, -before);
        //这个时间就是日期往后推一天的结果
        Date result = calendar.getTime();
        return getSdf(SIM_DAY_FORMAT).format(result);
    }
    public static void main(String[] args) {
        DateUtil.getDateDaysByMonth(new Date());
    }
}
