package com.jc.soft.util;

import com.jc.soft.config.SpringUtil;
import com.jc.soft.domain.User;
import com.jc.soft.service.UserService;
import com.jc.soft.jwt.JWTTokenUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.ConcurrentHashMap;

public class BaseUtil {

    private static ConcurrentHashMap cachMap = new ConcurrentHashMap();

    public static String getJwt(HttpServletRequest request){
        String authorizationHeader = request.getHeader("Authorization");

        String jwt = "";
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            jwt = authorizationHeader.substring(7);
        }

        return jwt;
    }

    public static User getUserByReq(String jwt){
        String username = null;

        if((cachMap.get("jwt")+"").equals(jwt)){
            return (User)cachMap.get("user");
        }

        JWTTokenUtil jwtUtil = SpringUtil.getBean(JWTTokenUtil.class);
        username = jwtUtil.extractUsername(jwt);
        UserService userService = SpringUtil.getBean(UserService.class);
        User user = userService.getUserByName(username);

        cachMap.put("jwt",jwt);
        cachMap.put("user",user);
        return user;
    }

}
