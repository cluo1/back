package com.jc.soft.util;

import com.google.gson.GsonBuilder;
import com.jc.soft.domain.MessageEntity;

import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

/**
 * MessageEntityEncode
 *
 * @author zjw
 * @createTime 2021/2/3 20:54
 */
public class MessageEntityEncode implements Encoder.Text<MessageEntity> {

    @Override
    public String encode(MessageEntity messageEntity) {
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create()
                .toJson(messageEntity);
    }

    @Override
    public void init(EndpointConfig endpointConfig) {}

    @Override
    public void destroy() {}

}
