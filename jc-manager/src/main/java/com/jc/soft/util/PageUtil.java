package com.jc.soft.util;

import java.util.List;

public class PageUtil {
    /**
     * 开始分页
     * @param list
     * @param pageNum 页码
     * @param pageSize 每页多少条数据
     * @return
     */
    public static List startPage(List list, Integer pageNum,
                                 Integer pageSize) {
        if (list == null) {
            return null;
        }
        if (list.size() == 0) {
            return list;
        }

        Integer count = list.size(); // 记录总数

        Integer pageCount = 0; // 页数
        if (count % pageSize == 0) {
            pageCount = count / pageSize;
        } else {
            pageCount = count / pageSize + 1;
        }

        int fromIndex = 0; // 开始索引
        int toIndex = 0; // 结束索引

        if (pageNum != pageCount) {
            fromIndex = (pageNum - 1) * pageSize;
            toIndex = fromIndex + pageSize;
        } else {
            fromIndex = (pageNum - 1) * pageSize;
            toIndex = count;
        }

        List pageList = list.subList(fromIndex, toIndex);

        return pageList;
    }

    /**
     * 开始分页
     * @param list
     * @param pageNum 页码
     * @param pageSize 每页多少条数据
     * @return
     */
    public static List endPage(List list, Integer pageNum,
                                 Integer pageSize) {
        if (list == null) {
            return null;
        }
        if (list.size() == 0) {
            return list;
        }
        Integer count = list.size(); // 记录总数
        int startNum = count - (pageNum * pageSize);
        int endNum = count-(pageSize * (pageNum-1));
        if(endNum<0){
            startNum =0;
            endNum = count;
        }
        if(endNum>0 && startNum<0){
            startNum = 0;
        }

        List pageList = list.subList(startNum, endNum);

        return pageList;
    }
}
