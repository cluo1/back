package com.jc.soft.util;

import java.util.Date;

/**
 * Created by jwyuan on 2015/12/21.
 */
public class TimeTerm {
    public long beginTime;
    public long endTime;
    public String yearMonth;
    public String dateStr;
    public Date dayDate;
}
