package com.jc.soft.mapper;

import com.jc.soft.domain.UserRoleInfo;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface UserRoleInfoMapper extends Mapper<UserRoleInfo> {
    List<UserRoleInfo> getUserRole(@Param("userIds") List<Integer> userIds);

    int insertUR(@Param("map") Map map);
}