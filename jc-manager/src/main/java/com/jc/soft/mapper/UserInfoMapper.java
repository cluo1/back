package com.jc.soft.mapper;

import com.jc.soft.domain.UserInfo;
import tk.mybatis.mapper.common.Mapper;

public interface UserInfoMapper extends Mapper<UserInfo> {
}