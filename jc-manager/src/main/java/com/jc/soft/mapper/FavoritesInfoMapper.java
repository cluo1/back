package com.jc.soft.mapper;

import com.jc.soft.domain.ArchivesInfo;
import com.jc.soft.domain.FavoritesInfo;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface FavoritesInfoMapper extends Mapper<FavoritesInfo> {

    //我收藏的人
    List<ArchivesInfo> getMeFavorite(Integer wxId);

    //收藏我的人
    List<ArchivesInfo> getFavoriteMe(Integer wxId);
}