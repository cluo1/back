package com.jc.soft.mapper;

import com.jc.soft.domain.RoleInfo;
import com.jc.soft.domain.UserInfo;

import java.util.List;

public interface UserCachMapper {
    UserInfo getUserByName(String userName);

   List<RoleInfo> getRolesByUserId(Integer userId);
}