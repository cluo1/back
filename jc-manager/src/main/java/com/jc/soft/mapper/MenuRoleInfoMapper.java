package com.jc.soft.mapper;

import com.jc.soft.domain.MenuRoleInfo;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface MenuRoleInfoMapper extends Mapper<MenuRoleInfo> {

    int insertMR(@Param("map") Map map);
}