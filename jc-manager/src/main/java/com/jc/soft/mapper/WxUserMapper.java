package com.jc.soft.mapper;

import com.jc.soft.domain.WxUserInfo;
import tk.mybatis.mapper.common.Mapper;


public interface WxUserMapper extends Mapper<WxUserInfo> {
}