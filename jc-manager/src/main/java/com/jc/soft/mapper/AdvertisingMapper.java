package com.jc.soft.mapper;

import com.jc.soft.domain.Advertising;
import io.micrometer.core.instrument.search.Search;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface AdvertisingMapper extends Mapper<Advertising> {

    /**
     * 获取最大的序号
     *
     * @return {@code int}
     */
    int getMaxOrderNo();

    void delAdvertising(@Param("ids") String ids);
}