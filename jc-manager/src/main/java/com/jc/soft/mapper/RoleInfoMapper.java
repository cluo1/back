package com.jc.soft.mapper;

import com.jc.soft.domain.RoleInfo;
import tk.mybatis.mapper.common.Mapper;

public interface RoleInfoMapper extends Mapper<RoleInfo> {
}