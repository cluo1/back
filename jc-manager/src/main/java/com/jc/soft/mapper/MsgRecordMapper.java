package com.jc.soft.mapper;

import com.jc.soft.domain.MsgRecord;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface MsgRecordMapper extends Mapper<MsgRecord> {
    List<MsgRecord> getMsgList(MsgRecord msgRecord);
}