package com.jc.soft.mapper;

import com.jc.soft.domain.MenuInfo;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface MenuInfoMapper extends Mapper<MenuInfo> {

    public List<MenuInfo> getAllMenuByRole(@Param("roleIds") List<Integer> roleIds);

    public List<MenuInfo> getAllMenuById(@Param("mIds") List<Integer> mIds);
}