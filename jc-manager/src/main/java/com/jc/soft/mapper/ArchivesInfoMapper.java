package com.jc.soft.mapper;

import com.jc.soft.domain.ArchivesInfo;
import tk.mybatis.mapper.common.Mapper;

public interface ArchivesInfoMapper extends Mapper<ArchivesInfo> {
}