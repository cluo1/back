package com.jc.soft.mapper;

import com.jc.soft.domain.UserTest;
import tk.mybatis.mapper.common.Mapper;

public interface UserTestMapper extends Mapper<UserTest> {
}