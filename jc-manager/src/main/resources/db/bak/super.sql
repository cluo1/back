/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50730
Source Host           : localhost:3306
Source Database       : super

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2021-10-25 09:24:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for jc_archives
-- ----------------------------
DROP TABLE IF EXISTS `jc_archives`;
CREATE TABLE `jc_archives` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) DEFAULT '' COMMENT '姓名',
  `sex` varchar(8) DEFAULT '' COMMENT '性别（男，女）',
  `picture` varchar(255) DEFAULT '' COMMENT '照片（存照片位置路径）',
  `birth_date` date DEFAULT NULL COMMENT '出生日期',
  `location` varchar(255) DEFAULT '' COMMENT '所在地',
  `work` varchar(128) DEFAULT NULL COMMENT '工作',
  `introduction` varchar(255) DEFAULT NULL COMMENT '个人简介',
  `requirement` varchar(255) DEFAULT NULL COMMENT '择偶要求',
  `wx_id` int(11) DEFAULT NULL COMMENT '微信用户表id',
  `contact` varchar(32) DEFAULT NULL COMMENT '联系方式',
  `status` varchar(255) DEFAULT NULL COMMENT '隐藏标识（0不隐藏 1隐藏）',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jc_archives
-- ----------------------------
INSERT INTO `jc_archives` VALUES ('1', 'jingjing', '1', null, '2021-10-01', null, null, null, null, '1', null, null, '2021-10-07 22:13:32', '2021-10-07 22:13:32');

-- ----------------------------
-- Table structure for jc_favorites
-- ----------------------------
DROP TABLE IF EXISTS `jc_favorites`;
CREATE TABLE `jc_favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wxId` int(11) DEFAULT NULL,
  `archId` int(11) DEFAULT NULL COMMENT '档案表id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jc_favorites
-- ----------------------------

-- ----------------------------
-- Table structure for jc_menu
-- ----------------------------
DROP TABLE IF EXISTS `jc_menu`;
CREATE TABLE `jc_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(100) DEFAULT NULL COMMENT '菜单名称',
  `path` varchar(50) DEFAULT NULL COMMENT '路径',
  `component` varchar(100) DEFAULT NULL COMMENT '组件',
  `redirect` varchar(200) DEFAULT '' COMMENT '重定向',
  `always_show` tinyint(1) DEFAULT '0' COMMENT '仅有一个子节点时是否显示根目录（1:是0：否）',
  `pid` int(11) DEFAULT '0' COMMENT '父ID',
  `meta_title` varchar(50) DEFAULT '' COMMENT 'meta标题',
  `meta_icon` varchar(50) DEFAULT '' COMMENT 'meta icon',
  `meta_nocache` tinyint(1) DEFAULT '0' COMMENT '是否缓存（1:是 0:否）',
  `meta_affix` tinyint(1) DEFAULT '0' COMMENT '是否加固（1:是0：否）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1026 DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of jc_menu
-- ----------------------------
INSERT INTO `jc_menu` VALUES ('1000', 'SystemManage', '/permission', 'Layout', '/permission/user', '0', '0', '系统管理', 'setting', '0', '0');
INSERT INTO `jc_menu` VALUES ('1005', 'Permission', 'role', '/system/role', '', '0', '1000', '角色管理', '', '0', '0');
INSERT INTO `jc_menu` VALUES ('1010', 'WxUser', 'wx', '/system/wxmanage', '', '0', '1000', '微信管理', '', '0', '0');
INSERT INTO `jc_menu` VALUES ('1015', 'UserSetting', 'user', '/system/user', '', '0', '1000', '用户管理', '', '0', '0');
INSERT INTO `jc_menu` VALUES ('1020', 'ClubUser', 'clubuser', '/system/clubuser', '', '0', '1000', '已绑定用户管理', '', '0', '0');
INSERT INTO `jc_menu` VALUES ('1025', 'Advertising', 'advertising', '/system/advertising', '', '0', '1000', '首页广告位管理', '', '0', '0');

-- ----------------------------
-- Table structure for jc_menu_role
-- ----------------------------
DROP TABLE IF EXISTS `jc_menu_role`;
CREATE TABLE `jc_menu_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menu_id` int(11) DEFAULT NULL COMMENT '菜单id',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jc_menu_role
-- ----------------------------
INSERT INTO `jc_menu_role` VALUES ('1', '1000', '1');
INSERT INTO `jc_menu_role` VALUES ('2', '1005', '1');
INSERT INTO `jc_menu_role` VALUES ('3', '1010', '1');
INSERT INTO `jc_menu_role` VALUES ('4', '1015', '1');
INSERT INTO `jc_menu_role` VALUES ('5', '1020', '1');
INSERT INTO `jc_menu_role` VALUES ('6', '1025', '1');

-- ----------------------------
-- Table structure for jc_role
-- ----------------------------
DROP TABLE IF EXISTS `jc_role`;
CREATE TABLE `jc_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `type` int(2) DEFAULT '1' COMMENT '角色类型，1普通角色，2后台系统角色',
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '角色说明',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户角色表';

-- ----------------------------
-- Records of jc_role
-- ----------------------------
INSERT INTO `jc_role` VALUES ('1', '2', '管理员', '后台权限', '2021-10-07 13:51:10', '2021-10-07 13:51:10');
INSERT INTO `jc_role` VALUES ('2', '2', '管理员', '后台权限', '2021-10-07 13:51:10', '2021-10-07 13:51:10');

-- ----------------------------
-- Table structure for jc_user
-- ----------------------------
DROP TABLE IF EXISTS `jc_user`;
CREATE TABLE `jc_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `real_name` varchar(255) DEFAULT NULL COMMENT '真实姓名',
  `user_name` varchar(50) DEFAULT NULL COMMENT '登录名称',
  `pass_word` varchar(100) DEFAULT NULL COMMENT '登录密码',
  `gender` int(2) DEFAULT '1' COMMENT '性别：0未知，1男，2女',
  `phone` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱地址',
  `avatar` text COMMENT '头像照片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of jc_user
-- ----------------------------
INSERT INTO `jc_user` VALUES ('1', '后台管理', 'admin', '$2a$10$qi1h.fJ3dmfBMV2C7R0cmOgIWPwfk7KH6vuUTU4IwTndw.wVEM3zi', '1', '13688889999', '1345@gmail.com', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/unlogin.jpg');

-- ----------------------------
-- Table structure for jc_user_role
-- ----------------------------
DROP TABLE IF EXISTS `jc_user_role`;
CREATE TABLE `jc_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户角色关联表';

-- ----------------------------
-- Records of jc_user_role
-- ----------------------------
INSERT INTO `jc_user_role` VALUES ('1', '1', '1');
INSERT INTO `jc_user_role` VALUES ('2', '1', '2');

-- ----------------------------
-- Table structure for jc_wx_user
-- ----------------------------
DROP TABLE IF EXISTS `jc_wx_user`;
CREATE TABLE `jc_wx_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `openid` varchar(28) DEFAULT NULL COMMENT '小程序用户的openid',
  `nickname` varchar(100) DEFAULT NULL COMMENT '用户头像',
  `avatarurl` varchar(200) DEFAULT NULL COMMENT '用户头像',
  `sex` varchar(1) DEFAULT NULL COMMENT '性别  0-男、1-女',
  `country` varchar(100) DEFAULT NULL COMMENT '所在国家',
  `province` varchar(100) DEFAULT NULL COMMENT '省份',
  `city` varchar(100) DEFAULT NULL COMMENT '城市',
  `ctime` datetime DEFAULT NULL COMMENT '创建/注册时间',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机号码',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `openIdInx` (`openid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jc_wx_user
-- ----------------------------
INSERT INTO `jc_wx_user` VALUES ('3', 'oBGga4-aCr8zIjC699xk92NQ2_lM', 'Z', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJt8zwhyicb0MAGkGEK2nfWCZrdGu5PS8TtMDR1o9NBdzOCzdXYK2mCZ1W0jlt1Cu51ZIcWl0CFaxw/132', '女', 'Iceland', '', '', '2021-10-07 21:41:25', null);
INSERT INTO `jc_wx_user` VALUES ('4', 'oBGga41Pr2eN3tb6kzl8x-YuV1GA', '微信用户', 'https://thirdwx.qlogo.cn/mmopen/vi_32/POgEwh4mIHO4nibH0KlMECNjjGxQUq24ZEaGT4poC6icRiccVGKSyXwibcPq4BWmiaIGuG1icwxaQX6grC9VemZoJ8rg/132', null, '', '', '', '2021-10-07 21:57:26', null);

-- ----------------------------
-- Table structure for super_menu
-- ----------------------------
DROP TABLE IF EXISTS `super_menu`;
CREATE TABLE `super_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(64) DEFAULT NULL,
  `path` varchar(64) DEFAULT NULL,
  `component` varchar(64) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `iconCls` varchar(64) DEFAULT NULL,
  `keepAlive` tinyint(1) DEFAULT NULL,
  `requireAuth` tinyint(1) DEFAULT NULL,
  `parent_Id` int(11) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parent_Id` (`parent_Id`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent_Id`) REFERENCES `super_menu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of super_menu
-- ----------------------------
INSERT INTO `super_menu` VALUES ('1', '/', null, null, '所有', null, null, null, null, '0');
INSERT INTO `super_menu` VALUES ('2', '/', '/home', 'Home', '员工资料', 'fa fa-user-circle-o', null, '1', '1', '0');
INSERT INTO `super_menu` VALUES ('3', '/', '/home', 'Home', '人事管理', 'fa fa-address-card-o', null, '1', '1', '1');
INSERT INTO `super_menu` VALUES ('4', '/', '/home', 'Home', '薪资管理', 'fa fa-money', null, '1', '1', '0');
INSERT INTO `super_menu` VALUES ('5', '/', '/home', 'Home', '统计管理', 'fa fa-bar-chart', null, '1', '1', '0');
INSERT INTO `super_menu` VALUES ('6', '/', '/home', 'Home', '系统管理', 'fa fa-windows', null, '1', '1', '1');
INSERT INTO `super_menu` VALUES ('7', '/employee/basic/**', '/emp/basic', 'EmpBasic', '基本资料', null, null, '1', '2', '0');
INSERT INTO `super_menu` VALUES ('8', '/employee/advanced/**', '/emp/adv', 'EmpAdv', '高级资料', null, null, '1', '2', '0');
INSERT INTO `super_menu` VALUES ('9', '/personnel/emp/**', '/per/emp', 'PerEmp', '员工资料', null, null, '1', '3', '0');
INSERT INTO `super_menu` VALUES ('10', '/personnel/ec/**', '/per/ec', 'PerEc', '员工奖惩', null, null, '1', '3', '0');
INSERT INTO `super_menu` VALUES ('11', '/personnel/train/**', '/per/train', 'PerTrain', '员工培训', null, null, '1', '3', '0');
INSERT INTO `super_menu` VALUES ('12', '/personnel/salary/**', '/per/salary', 'PerSalary', '员工调薪', null, null, '1', '3', '0');
INSERT INTO `super_menu` VALUES ('13', '/personnel/remove/**', '/per/mv', 'PerMv', '员工调动', null, null, '1', '3', '0');
INSERT INTO `super_menu` VALUES ('14', '/salary/sob/**', '/sal/sob', 'SalSob', '工资账套管理', null, null, '1', '4', '0');
INSERT INTO `super_menu` VALUES ('15', '/salary/sobcfg/**', '/sal/sobcfg', 'SalSobCfg', '员工账套设置', null, null, '1', '4', '0');
INSERT INTO `super_menu` VALUES ('16', '/salary/table/**', '/sal/table', 'SalTable', '工资表管理', null, null, '1', '4', '0');
INSERT INTO `super_menu` VALUES ('17', '/salary/month/**', '/sal/month', 'SalMonth', '月末处理', null, null, '1', '4', '0');
INSERT INTO `super_menu` VALUES ('18', '/salary/search/**', '/sal/search', 'SalSearch', '工资表查询', null, null, '1', '4', '0');
INSERT INTO `super_menu` VALUES ('19', '/statistics/all/**', '/sta/all', 'StaAll', '综合信息统计', null, null, '1', '5', '0');
INSERT INTO `super_menu` VALUES ('20', '/statistics/score/**', '/sta/score', 'StaScore', '员工积分统计', null, null, '1', '5', '0');
INSERT INTO `super_menu` VALUES ('21', '/statistics/personnel/**', '/sta/pers', 'StaPers', '人事信息统计', null, null, '1', '5', '0');
INSERT INTO `super_menu` VALUES ('22', '/statistics/recored/**', '/sta/record', 'StaRecord', '人事记录统计', null, null, '1', '5', '0');
INSERT INTO `super_menu` VALUES ('23', '/system/basic/**', '/sys/basic', 'SysBasic', '基础信息设置', null, null, '1', '6', '1');
INSERT INTO `super_menu` VALUES ('24', '/system/cfg/**', '/sys/cfg', 'SysCfg', '系统管理', null, null, '1', '6', '1');
INSERT INTO `super_menu` VALUES ('25', '/system/log/**', '/sys/log', 'SysLog', '操作日志管理', null, null, '1', '6', '1');
INSERT INTO `super_menu` VALUES ('26', '/system/hr/**', '/sys/hr', 'SysHr', '操作员管理', null, null, '1', '6', '1');
INSERT INTO `super_menu` VALUES ('27', '/system/data/**', '/sys/data', 'SysData', '备份恢复数据库', null, null, '1', '6', '1');
INSERT INTO `super_menu` VALUES ('28', '/system/init/**', '/sys/init', 'SysInit', '初始化数据库', null, null, '1', '6', '1');

-- ----------------------------
-- Table structure for super_menu_role
-- ----------------------------
DROP TABLE IF EXISTS `super_menu_role`;
CREATE TABLE `super_menu_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_id` (`menu_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `sm_id` FOREIGN KEY (`menu_id`) REFERENCES `super_menu` (`id`),
  CONSTRAINT `sr_id` FOREIGN KEY (`role_id`) REFERENCES `super_role` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of super_menu_role
-- ----------------------------
INSERT INTO `super_menu_role` VALUES ('1', '6', '1');
INSERT INTO `super_menu_role` VALUES ('2', '23', '1');
INSERT INTO `super_menu_role` VALUES ('3', '24', '1');
INSERT INTO `super_menu_role` VALUES ('4', '25', '1');
INSERT INTO `super_menu_role` VALUES ('5', '26', '1');
INSERT INTO `super_menu_role` VALUES ('6', '27', '1');
INSERT INTO `super_menu_role` VALUES ('7', '28', '1');

-- ----------------------------
-- Table structure for super_properties
-- ----------------------------
DROP TABLE IF EXISTS `super_properties`;
CREATE TABLE `super_properties` (
  `application` varchar(64) NOT NULL,
  `property_name` varchar(128) NOT NULL,
  `property_value` varchar(500) NOT NULL,
  `profile` varchar(32) NOT NULL,
  `label` varchar(32) NOT NULL,
  PRIMARY KEY (`application`,`profile`,`property_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of super_properties
-- ----------------------------
INSERT INTO `super_properties` VALUES ('demo', 'server.port', '8001', 'dev', 'master');
INSERT INTO `super_properties` VALUES ('feign', 'feign.hystrix.enabled', 'true', 'dev', 'master');
INSERT INTO `super_properties` VALUES ('feign', 'server.connection-timeout', '10000', 'dev', 'master');
INSERT INTO `super_properties` VALUES ('feign', 'server.port', '8030', 'dev', 'master');
INSERT INTO `super_properties` VALUES ('gateway', 'management.endpoints.web.cors.allowed-methods', '*', 'dev', 'master');
INSERT INTO `super_properties` VALUES ('gateway', 'management.endpoints.web.cors.allowed-origins', '*', 'dev', 'master');
INSERT INTO `super_properties` VALUES ('gateway', 'management.endpoints.web.exposure.include', '*', 'dev', 'master');
INSERT INTO `super_properties` VALUES ('gateway', 'server.port', '8020', 'dev', 'master');
INSERT INTO `super_properties` VALUES ('gateway', 'spring.cloud.gateway.discovery.locator.enabled', 'true', 'dev', 'master');
INSERT INTO `super_properties` VALUES ('gateway', 'spring.cloud.gateway.discovery.locator.lowerCaseServiceId', 'true', 'dev', 'master');
INSERT INTO `super_properties` VALUES ('system', 'mybatis.mapperLocations', 'classpath:mapper/*Mapper.xml', 'dev', 'master');
INSERT INTO `super_properties` VALUES ('system', 'server.port', '8800', 'dev', 'master');
INSERT INTO `super_properties` VALUES ('system', 'spring.datasource.driver-class-name', 'com.mysql.jdbc.Driver', 'dev', 'master');
INSERT INTO `super_properties` VALUES ('system', 'spring.datasource.password', '!Flytek@2020', 'dev', 'master');
INSERT INTO `super_properties` VALUES ('system', 'spring.datasource.url', 'jdbc:mysql://172.31.132.19:3306/super?useUnicode=true&characterEncoding=utf8&serverTimezone=GMT%2B8', 'dev', 'master');
INSERT INTO `super_properties` VALUES ('system', 'spring.datasource.username', 'root', 'dev', 'master');

-- ----------------------------
-- Table structure for super_role
-- ----------------------------
DROP TABLE IF EXISTS `super_role`;
CREATE TABLE `super_role` (
  `role_id` int(10) NOT NULL AUTO_INCREMENT,
  `role_name_en` varchar(64) DEFAULT NULL COMMENT '角色名称',
  `role_name_zh` varchar(64) DEFAULT NULL COMMENT '角色名称',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of super_role
-- ----------------------------
INSERT INTO `super_role` VALUES ('1', 'ROLE_admin', '系统管理员');
INSERT INTO `super_role` VALUES ('2', 'ROLE_test2', '测试角色2');
INSERT INTO `super_role` VALUES ('3', 'ROLE_test1', '测试角色1');

-- ----------------------------
-- Table structure for super_user
-- ----------------------------
DROP TABLE IF EXISTS `super_user`;
CREATE TABLE `super_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` char(11) DEFAULT NULL COMMENT '手机号码',
  `address` varchar(64) DEFAULT NULL COMMENT '联系地址',
  `enabled` tinyint(1) DEFAULT '1',
  `userface` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of super_user
-- ----------------------------
INSERT INTO `super_user` VALUES ('1', 'super', '$2a$10$ySG2lkvjFHY5O0./CPIE1OI8VJsuKYEzOYzqIa7AJR6sEgSzUFOAm', '18656692693', '安徽', '1', 'http://bpic.588ku.com/element_pic/01/40/00/64573ce2edc0728.jpg', null);
INSERT INTO `super_user` VALUES ('2', 'super2', '$2a$10$ySG2lkvjFHY5O0./CPIE1OI8VJsuKYEzOYzqIa7AJR6sEgSzUFOAm', '18656692693', '安徽', '1', 'http://bpic.588ku.com/element_pic/01/40/00/64573ce2edc0728.jpg', null);
INSERT INTO `super_user` VALUES ('3', 'super3', '$2a$10$ySG2lkvjFHY5O0./CPIE1OI8VJsuKYEzOYzqIa7AJR6sEgSzUFOAm', '18656692693', '安徽', '1', 'http://bpic.588ku.com/element_pic/01/40/00/64573ce2edc0728.jpg', null);

-- ----------------------------
-- Table structure for super_user_role
-- ----------------------------
DROP TABLE IF EXISTS `super_user_role`;
CREATE TABLE `super_user_role` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_Id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `u_idx` (`user_Id`),
  KEY `r_idx` (`role_id`),
  CONSTRAINT `sr_rid` FOREIGN KEY (`role_id`) REFERENCES `super_role` (`role_id`),
  CONSTRAINT `su_uid` FOREIGN KEY (`user_Id`) REFERENCES `super_user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of super_user_role
-- ----------------------------
INSERT INTO `super_user_role` VALUES ('1', '1', '1');
INSERT INTO `super_user_role` VALUES ('2', '2', '2');
INSERT INTO `super_user_role` VALUES ('3', '3', '3');

-- ----------------------------
-- Table structure for t_fbc_advertising
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_advertising`;
CREATE TABLE `t_fbc_advertising` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `activity_id` int(11) DEFAULT NULL COMMENT '活动id',
  `picture` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '图片url',
  `url` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '超链接url',
  `type` varchar(2) CHARACTER SET utf8 DEFAULT NULL COMMENT '类型 1超链接 2赛事活动',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `status` varchar(2) CHARACTER SET utf8 DEFAULT NULL COMMENT '状态 1开 2关',
  `author` varchar(50) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='广告位表';

-- ----------------------------
-- Records of t_fbc_advertising
-- ----------------------------
INSERT INTO `t_fbc_advertising` VALUES ('28', '1', '11', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1603854010_banner.jpg', '', '2', '2020-10-28 09:17:26', '1', null);
INSERT INTO `t_fbc_advertising` VALUES ('29', '1', '11', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1603854967_banner1.jpg', '', '2', '2020-10-28 11:13:09', '1', null);
INSERT INTO `t_fbc_advertising` VALUES ('32', '1', '16', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1612493167_赛事图片.jpg', '', '2', '2021-02-05 10:46:09', '1', 'zy');

-- ----------------------------
-- Table structure for t_fbc_apply
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_apply`;
CREATE TABLE `t_fbc_apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `game_id` int(11) DEFAULT NULL COMMENT '赛事id',
  `team_id` int(11) DEFAULT NULL COMMENT '队伍id',
  `cap_name` varchar(255) DEFAULT NULL COMMENT '领队信息，姓名及手机号',
  `coa_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '教练信息',
  `coach_number` int(11) DEFAULT NULL COMMENT '教练人数',
  `pay_status` int(2) DEFAULT '1' COMMENT '缴费状态，1未支付，2已支付',
  `pay_request` varchar(1000) DEFAULT NULL COMMENT '接口发起数据',
  `pay_response` varchar(1000) DEFAULT NULL COMMENT '接口返回数据',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `status` int(11) DEFAULT NULL COMMENT '报名状态 0未审核 1审核通过 2审核不通过',
  `remark` varchar(100) DEFAULT NULL COMMENT '审核不通过理由',
  `team_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '球队名称，报名比赛页面填写的队名',
  `picture` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '队伍照片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='赛事报名表';

-- ----------------------------
-- Records of t_fbc_apply
-- ----------------------------
INSERT INTO `t_fbc_apply` VALUES ('136', '1', '51', '57', '刘小帅17681099240', '', '0', '2', null, null, '2020-12-28 13:36:49', '1', '', '帅帅球队', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607654813_97.jpg');
INSERT INTO `t_fbc_apply` VALUES ('137', '1', '51', '58', '刘小帅17681099240', null, '0', '1', null, null, null, '1', '', '刀锋球队', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607654820_99.jpg');
INSERT INTO `t_fbc_apply` VALUES ('164', '1', '52', '72', '王环15375490360', null, null, '1', null, null, null, '1', null, '合肥芙水源FC', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607916201_合肥芙水源FC.jpg');
INSERT INTO `t_fbc_apply` VALUES ('165', '1', '52', '73', '王环15375490360', null, null, '1', null, null, null, '1', null, '青岛鲲鹏', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607916133_青岛鲲鹏B.jpg');
INSERT INTO `t_fbc_apply` VALUES ('166', '1', '52', '74', '王环15375490360', null, null, '1', null, null, null, '1', null, '合肥四方', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607916163_合肥四方.jpg');
INSERT INTO `t_fbc_apply` VALUES ('167', '1', '52', '75', '王环15375490360', null, null, '1', null, null, null, '1', null, '淮北派菲特', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607916174_淮北派菲特.jpg');
INSERT INTO `t_fbc_apply` VALUES ('168', '1', '52', '76', '王环15375490360', null, null, '1', null, null, null, '1', null, '合肥师范附二小', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607916185_合肥师范附二小.jpg');
INSERT INTO `t_fbc_apply` VALUES ('169', '1', '52', '82', '王环15375490360', null, null, '1', null, null, null, '1', null, '合肥师范3', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607999181_1607916185_合肥师范附二小.jpg');
INSERT INTO `t_fbc_apply` VALUES ('170', '1', '52', '83', '王环15375490361', null, null, '1', null, null, null, '1', null, '合肥师范4', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607999193_1607916185_合肥师范附二小.jpg');
INSERT INTO `t_fbc_apply` VALUES ('171', '1', '52', '84', '王环15375490362', null, null, '1', null, null, null, '1', null, '合肥师范5', null);
INSERT INTO `t_fbc_apply` VALUES ('172', '1', '52', '85', '王环15375490363', null, null, '1', null, null, null, '1', null, '合肥师范6', null);
INSERT INTO `t_fbc_apply` VALUES ('173', '1', '53', '66', '孙正奇13856951396', null, '0', '1', null, null, null, '1', '', '合肥众鑫', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607912928_u=1871560953,315592401&fm=26&gp=0.jpg');
INSERT INTO `t_fbc_apply` VALUES ('177', '1', '57', '66', '孙正奇13856951396', null, '0', '1', null, null, null, '1', '', '合肥众鑫', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607912928_u=1871560953,315592401&fm=26&gp=0.jpg');
INSERT INTO `t_fbc_apply` VALUES ('179', '1', '51', '66', '孙正奇13856951396', null, '0', '1', null, null, null, '1', '', '合肥众鑫', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607912928_u=1871560953,315592401&fm=26&gp=0.jpg');
INSERT INTO `t_fbc_apply` VALUES ('180', '1', '57', '57', '刘小帅17681099240', null, '0', '1', null, null, null, '1', '', '雷霆球队', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607654813_97.jpg');
INSERT INTO `t_fbc_apply` VALUES ('181', '1', '51', '0', '', null, '0', '1', null, null, null, '1', null, 'tt', '');
INSERT INTO `t_fbc_apply` VALUES ('182', '1', '58', '66', '孙正奇13856951396', null, '0', '1', null, null, null, '1', '', '合肥众鑫', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607912928_u=1871560953,315592401&fm=26&gp=0.jpg');
INSERT INTO `t_fbc_apply` VALUES ('183', '1', '58', '74', null, null, null, '1', null, null, null, '1', '', '合肥四方', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607916163_合肥四方.jpg');
INSERT INTO `t_fbc_apply` VALUES ('184', '1', '58', '73', null, null, null, '1', null, null, null, '1', '', '青岛鲲鹏', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607916133_青岛鲲鹏B.jpg');
INSERT INTO `t_fbc_apply` VALUES ('185', '1', '57', '87', '李四13856951396', '', '0', '2', null, null, '2021-01-19 14:19:13', '1', '', '飞流队', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1611796885_2854');
INSERT INTO `t_fbc_apply` VALUES ('186', '1', '59', '90', '唐队13856951396', '', '0', '1', null, null, null, '1', '', '合云队', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1612342000_9984');
INSERT INTO `t_fbc_apply` VALUES ('187', '1', '59', '58', '刘小帅17681099240', null, '0', '1', null, null, null, '1', '', '刀锋球队', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607654820_99.jpg');

-- ----------------------------
-- Table structure for t_fbc_apply_detail
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_apply_detail`;
CREATE TABLE `t_fbc_apply_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `team_id` int(11) DEFAULT NULL COMMENT '领队id',
  `apply_id` int(11) DEFAULT NULL COMMENT '申请表id',
  `ftb_id` int(11) DEFAULT NULL COMMENT '球员id',
  `name` varchar(100) DEFAULT NULL COMMENT '领队、教练、球员姓名',
  `type` int(11) DEFAULT NULL COMMENT '人员类型: 1球员；2教练员，3领队',
  `sex` char(2) DEFAULT NULL COMMENT '性别',
  `phone` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `height` double(11,0) DEFAULT NULL COMMENT '身高',
  `weight` double(20,2) DEFAULT NULL COMMENT '体重',
  `number` int(11) DEFAULT NULL COMMENT '球员号码',
  `location` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '球员在球队中位置',
  `picture` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '球员照片',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1126 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='赛事报名明细表';

-- ----------------------------
-- Records of t_fbc_apply_detail
-- ----------------------------
INSERT INTO `t_fbc_apply_detail` VALUES ('725', '1', '57', '136', '467', '刘帅帅', '1', '男', '17681099240', '170', '50.00', '0', '先锋', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1608001344_9.jpg', null);
INSERT INTO `t_fbc_apply_detail` VALUES ('726', '1', '58', '137', '468', '刘丽丽', '1', '女', '17681104171', '165', '50.00', '2', '守门员', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607654857_7.jpg', null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1029', '1', '72', '164', '610', '吴孟泽', '1', '男', '15375490360', '160', '50.00', '0', '前锋', null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1030', '1', '72', '164', '611', '刘博瑞', '1', '男', '15375490361', '160', '50.00', '0', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1031', '1', '72', '164', '612', '叶志远', '1', '男', '15375490362', '160', '50.00', '0', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1032', '1', '72', '164', '613', '朱李建成', '1', '男', '15375490363', '160', '50.00', '1', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1033', '1', '72', '164', '614', '胡曦元', '1', '男', '15375490364', '160', '50.00', '4', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1034', '1', '72', '164', '615', '李湖', '1', '男', '15375490365', '160', '50.00', '6', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1035', '1', '72', '164', '616', '连鹏吉', '1', '男', '15375490366', '160', '50.00', '7', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1036', '1', '72', '164', '617', '马陈哲', '1', '男', '15375490367', '160', '50.00', '9', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1037', '1', '72', '164', '618', '金铭凯', '1', '男', '15375490368', '160', '50.00', '11', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1038', '1', '72', '164', '619', '王金洋', '1', '男', '15375490369', '160', '50.00', '18', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1039', '1', '72', '164', '620', '朱嘉瑞', '1', '男', '15375490370', '160', '50.00', '20', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1040', '1', '72', '164', '621', '葛皓天', '1', '男', '15375490371', '160', '50.00', '21', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1041', '1', '72', '164', '622', '方明宇', '1', '男', '15375490372', '160', '50.00', '26', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1042', '1', '72', '164', '623', '崔智博', '1', '男', '15375490373', '160', '50.00', '29', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1043', '1', '73', '165', '624', '苏麒霖', '1', '男', '15375490374', '160', '50.00', '2', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1044', '1', '73', '165', '625', '刘戴铖', '1', '男', '15375490375', '160', '50.00', '3', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1045', '1', '73', '165', '626', '孙瑞阳', '1', '男', '15375490376', '160', '50.00', '6', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1046', '1', '73', '165', '627', '郭弈泽', '1', '男', '15375490377', '160', '50.00', '7', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1047', '1', '73', '165', '628', '许梓睿', '1', '男', '15375490378', '160', '50.00', '8', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1048', '1', '73', '165', '629', '董子铭', '1', '男', '15375490379', '160', '50.00', '9', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1049', '1', '73', '165', '630', '丁一龙', '1', '男', '15375490380', '160', '50.00', '10', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1050', '1', '73', '165', '631', '高梓宸', '1', '男', '15375490381', '160', '50.00', '12', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1051', '1', '73', '165', '632', '王逍羽', '1', '男', '15375490382', '160', '50.00', '15', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1052', '1', '73', '165', '633', '姜弈帆', '1', '男', '15375490383', '160', '50.00', '18', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1053', '1', '73', '165', '634', '张雨腾', '1', '男', '15375490384', '160', '50.00', '19', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1054', '1', '73', '165', '635', '王弈辰', '1', '男', '15375490385', '160', '50.00', '21', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1055', '1', '73', '165', '636', '王浩然', '1', '男', '15375490386', '160', '50.00', '26', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1056', '1', '73', '165', '637', '辛梓扬', '1', '男', '15375490387', '160', '50.00', '27', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1057', '1', '73', '165', '638', '刘鑫宇', '1', '男', '15375490388', '160', '50.00', null, null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1058', '1', '73', '165', '639', '王迦诺', '1', '男', '15375490389', '160', '50.00', '35', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1059', '1', '74', '166', '640', '夏浩然', '1', '男', '15375490390', '160', '50.00', '0', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1060', '1', '74', '166', '641', '孟航宇', '1', '男', '15375490391', '160', '50.00', '0', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1061', '1', '74', '166', '642', '余中昊', '1', '男', '15375490392', '160', '50.00', '0', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1062', '1', '74', '166', '643', '茹景颢', '1', '男', '15375490393', '160', '50.00', '0', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1063', '1', '74', '166', '644', '刘修泽', '1', '男', '15375490394', '160', '50.00', '0', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1064', '1', '74', '166', '645', '吴天奥', '1', '男', '15375490395', '160', '50.00', '0', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1065', '1', '74', '166', '646', '付炜航', '1', '男', '15375490396', '160', '50.00', '0', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1066', '1', '74', '166', '647', '谢振华', '1', '男', '15375490397', '160', '50.00', '0', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1067', '1', '74', '166', '648', '孙乐天', '1', '男', '15375490398', '160', '50.00', '1', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1068', '1', '74', '166', '649', '赵浩钦', '1', '男', '15375490399', '160', '50.00', '24', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1069', '1', '74', '166', '650', '吴赵隽皓', '1', '男', '15375490400', '160', '50.00', '40', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1070', '1', '75', '167', '651', '闫欣泽', '1', '男', '15375490401', '160', '50.00', '1', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1071', '1', '75', '167', '652', '李唯赫', '1', '男', '15375490402', '160', '50.00', '2', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1072', '1', '75', '167', '653', '周恒立', '1', '男', '15375490403', '160', '50.00', '5', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1073', '1', '75', '167', '654', '韩梓旭', '1', '男', '15375490404', '160', '50.00', '6', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1074', '1', '75', '167', '655', '张旭', '1', '男', '15375490405', '160', '50.00', '7', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1075', '1', '75', '167', '656', '蒋鸣', '1', '男', '15375490406', '160', '50.00', '8', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1076', '1', '75', '167', '657', '郑思汉', '1', '男', '15375490407', '160', '50.00', '9', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1077', '1', '75', '167', '658', '朱晨铭', '1', '男', '15375490408', '160', '50.00', '11', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1078', '1', '75', '167', '659', '王文昊', '1', '男', '15375490409', '160', '50.00', '12', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1079', '1', '75', '167', '660', '张嘉逸', '1', '男', '15375490410', '160', '50.00', '15', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1080', '1', '75', '167', '661', '郑博宇', '1', '男', '15375490411', '160', '50.00', '16', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1081', '1', '75', '167', '662', '张富豪', '1', '男', '15375490412', '160', '50.00', '33', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1082', '1', '75', '167', '663', '朱浩然', '1', '男', '15375490413', '160', '50.00', '53', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1083', '1', '76', '168', '664', '邵诗彤', '1', '女', '15375490414', '160', '50.00', '6', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1084', '1', '76', '168', '665', '崔雅玥', '1', '女', '15375490415', '160', '50.00', '6', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1085', '1', '76', '168', '666', '吕欣怡', '1', '女', '15375490416', '160', '50.00', '7', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1086', '1', '76', '168', '667', '顾昕玥', '1', '女', '15375490417', '160', '50.00', '11', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1087', '1', '76', '168', '668', '付芷伊', '1', '女', '15375490418', '160', '50.00', '12', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1088', '1', '76', '168', '669', '邹帆', '1', '女', '15375490419', '160', '50.00', '15', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1089', '1', '76', '168', '670', '梁艺芯', '1', '女', '15375490420', '160', '50.00', '16', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1090', '1', '76', '168', '671', '钱雅琳', '1', '女', '15375490421', '160', '50.00', '19', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1091', '1', '76', '168', '672', '刘文曦', '1', '女', '15375490422', '160', '50.00', '20', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1092', '1', '76', '168', '673', '王柔瑾', '1', '女', '15375490423', '160', '50.00', '21', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1093', '1', '76', '168', '674', '夏书韵', '1', '女', '15375490424', '160', '50.00', '23', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1094', '1', '76', '168', '675', '张心然', '1', '女', '15375490425', '160', '50.00', '25', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1095', '1', '76', '168', '676', '王柔瑜', '1', '女', '15375490426', '160', '50.00', '26', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1096', '1', '76', '168', '677', '王佳欣', '1', '女', '15375490427', '160', '50.00', '28', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1097', '1', '76', '168', '678', '王艺菲', '1', '女', '15375490428', '160', '50.00', '35', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1098', '1', '76', '168', '679', '王思彤', '1', '女', '15375490429', '160', '50.00', null, null, 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607930300_2.jpg', null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1099', '1', '82', '169', '684', '王思2', '1', '女', '15375490430', '160', '50.00', '37', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1100', '1', '83', '170', '685', '王思3', '1', '女', '15375490431', '161', '51.00', '38', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1101', '1', '84', '171', '686', '王思4', '1', '女', '15375490432', '162', '52.00', '39', null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1103', '1', '66', '173', '469', '王明琪', '1', '男', '13856951396', '182', '80.00', '2', '中锋', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/tmp_5941016a3fee1a18f1e73050940dff9b.png', null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1104', '1', '66', '173', '467', '刘帅帅', '1', '男', '17681099240', '170', '50.00', '1', '先锋', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1608000988_1.jpg', null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1106', '1', '66', '175', '467', '刘帅帅', '1', '男', '17681099240', '170', '50.00', '1', '先锋', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1608001344_9.jpg', null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1107', '1', '66', '175', '469', '王明琪', '1', '男', '13856951396', '182', '80.00', '2', '中锋', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/tmp_5941016a3fee1a18f1e73050940dff9b.png', null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1108', '1', '57', '176', '467', '刘帅帅', '1', '男', '17681099240', '170', '50.00', '0', '先锋', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1608001344_9.jpg', null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1109', '1', '66', '177', '467', '刘帅帅', '1', '男', '17681099240', '170', '50.00', '1', '先锋', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1608001344_9.jpg', null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1112', '1', '66', '179', '467', '刘帅帅', '1', '男', '17681099240', '170', '50.00', '1', '先锋', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1608001344_9.jpg', null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1113', '1', '66', '179', '469', '王明琪', '1', '男', '13856951396', '182', '80.00', '2', '中锋', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/tmp_5941016a3fee1a18f1e73050940dff9b.png', null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1114', '1', '57', '180', '467', '刘帅帅', '1', '男', '17681099240', '170', '50.00', '0', '先锋', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1608001344_9.jpg', null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1115', '1', '85', '172', '687', '王思5', '1', '女', '15375490433', '163', '52.00', '40', null, 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607933030_7.jpg', null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1116', '1', '0', '181', '29', '王思佳', '1', '男', '13856951396', '180', '60.00', null, null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1117', '1', '0', '181', '26', '汤子晨', '1', '男', '13856951396', '162', '58.00', null, null, null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1118', '1', '66', '182', '467', '刘帅帅', '1', '男', '17681099240', '170', '50.00', '1', '先锋', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1608001344_9.jpg', null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1119', '1', '66', '182', '469', '王明琪', '1', '男', '13856951396', '182', '80.00', '2', '中锋', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/tmp_5941016a3fee1a18f1e73050940dff9b.png', null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1120', '1', '66', '182', '610', '吴孟泽', '1', '男', '15375490360', '160', '50.00', '1', '前锋', null, null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1123', '1', '90', '186', '688', '唐斯佳', '1', '男', '13856951396', '169', '53.00', '15', '守门员', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/tmp_77812df305245372b8b07adbed4dba50.jpg', null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1124', '1', '58', '187', '468', '刘丽丽', '1', '女', '17681104171', '165', '50.00', '2', '守门员', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607654857_7.jpg', null);
INSERT INTO `t_fbc_apply_detail` VALUES ('1125', '1', '58', '187', '467', '刘帅帅', '1', '男', '17681099240', '170', '50.00', '1', '前卫', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1608001344_9.jpg', null);

-- ----------------------------
-- Table structure for t_fbc_captain
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_captain`;
CREATE TABLE `t_fbc_captain` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户中心表id',
  `name` varchar(100) DEFAULT NULL COMMENT '领队姓名',
  `sex` char(2) DEFAULT NULL COMMENT '性别，男，女',
  `phone` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `status` int(11) DEFAULT NULL COMMENT '审核状态 0 未审核 1审核通过 2审核未通过',
  `remark` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注',
  `id_card` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '身份证',
  `picture` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '领队照片',
  `lastoptime` datetime DEFAULT NULL COMMENT '最后操作时间',
  `expire` int(11) DEFAULT NULL COMMENT '删除标识 1未删除 2已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_fbc_captain
-- ----------------------------
INSERT INTO `t_fbc_captain` VALUES ('15', '1', '142', '刘小帅', '男', '17681099240', '1', '', '341226199808255123', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/wxcb53c5d3e83c6b82.o6zAJs88nMiB6hYVJzxuKtJNov-g.7CGRyPgwmZdRe2024262cc14ad2c1f1e230fc4c695ba.jpg', null, '1');
INSERT INTO `t_fbc_captain` VALUES ('19', '1', '0', '王环', null, '15375490360', '1', null, null, null, '2020-12-14 11:19:57', '2');
INSERT INTO `t_fbc_captain` VALUES ('20', '1', '145', '王环', null, '15375490360', '1', null, null, null, '2020-12-28 10:36:44', '2');
INSERT INTO `t_fbc_captain` VALUES ('21', '1', '0', '王环', null, '15375490361', '1', null, null, null, '2020-12-28 10:36:43', '2');
INSERT INTO `t_fbc_captain` VALUES ('22', '1', '0', '王环', null, '15375490362', '1', null, null, null, '2020-12-28 10:36:41', '2');
INSERT INTO `t_fbc_captain` VALUES ('23', '1', '0', '王环', null, '15375490363', '1', null, null, null, '2020-12-28 10:36:38', '2');
INSERT INTO `t_fbc_captain` VALUES ('26', '1', '154', '唐队', '男', '13856951396', '1', '', '352685202008061163', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/tmp_cbad5f71c867c0ffd5985740ca30fab9.jpg', null, '1');
INSERT INTO `t_fbc_captain` VALUES ('27', '1', '156', '陈宝光', '男', '18933787697', '0', null, '440883198703300038', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/tmp_cde50fdb32f0dc0274872dbb8b66e0a5be71a87967e7a056.jpg', null, '1');
INSERT INTO `t_fbc_captain` VALUES ('28', '1', '157', 'ECLAIR', '男', '18640433798', '0', null, '440785200410080016', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/tmp_29afbf30cebb9a2bc0b6453262b56211.jpg', null, '1');

-- ----------------------------
-- Table structure for t_fbc_child
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_child`;
CREATE TABLE `t_fbc_child` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部ID',
  `parent_id` int(11) DEFAULT NULL COMMENT '家长表id',
  `school_id` varchar(255) DEFAULT NULL COMMENT '学校id',
  `grade` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '年级',
  `class_id` int(11) DEFAULT NULL COMMENT '班级ID',
  `name` varchar(20) DEFAULT NULL COMMENT '学员/小孩名称',
  `sex` char(2) DEFAULT NULL COMMENT '性别 男 女',
  `id_card` varchar(20) DEFAULT NULL COMMENT '身份证',
  `birth_date` date DEFAULT NULL COMMENT '出生日期',
  `height` double(11,2) DEFAULT NULL COMMENT '身高',
  `weight` double(11,2) DEFAULT NULL COMMENT '体重',
  `picture` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '球员照片',
  `status` int(11) DEFAULT NULL COMMENT '报名状态 0审核中 1审核通过 2审核不通过',
  `remark` varchar(255) DEFAULT NULL COMMENT '拒绝原因',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='学员/小孩';

-- ----------------------------
-- Records of t_fbc_child
-- ----------------------------
INSERT INTO `t_fbc_child` VALUES ('25', '1', null, 'SC0001', 'GR0001', '23', '刘帅', '男', '341226199808255123', '1998-08-25', '170.00', '50.00', null, '2', '信息错误');
INSERT INTO `t_fbc_child` VALUES ('26', '1', null, 'SC0001', 'GR0005', '19', '汤子晨', '男', '345810199908023542', '1999-08-02', '162.00', '58.00', null, '1', '');
INSERT INTO `t_fbc_child` VALUES ('27', '1', null, 'SC0001', 'GR0001', '19', '李余则', '男', '258698199608031856', '1996-08-03', '163.00', '58.00', null, '2', '1');
INSERT INTO `t_fbc_child` VALUES ('28', '1', null, 'SC0001', 'GR0001', '19', '高清游', '男', '528096201008071234', '2010-08-07', '168.00', '90.00', null, '1', '');
INSERT INTO `t_fbc_child` VALUES ('29', '1', null, 'SC0001', 'GR0001', '19', '王思佳', '男', '258079201708072580', '2017-08-07', '180.00', '60.00', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1609294247_9.jpg', '1', '');
INSERT INTO `t_fbc_child` VALUES ('30', '1', null, 'SC0001', 'GR0007', '51', '刘星', '男', '356901201007041234', '2010-07-04', '163.00', '63.00', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/tmp_9e155f4bac113c584a4299332a4cd890.jpg', '2', '顺序');
INSERT INTO `t_fbc_child` VALUES ('31', '1', null, 'SC0001', 'GR0007', '51', '张三', '男', '235801201008053698', '2010-08-05', '135.00', '63.00', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1609901658_timg.jpg', '2', '大大');
INSERT INTO `t_fbc_child` VALUES ('35', '1', null, 'SC0001', 'GR0007', '51', '阿尔卑斯', '男', '341226199805225789', '1998-05-22', '170.00', '50.00', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/tmp_f0cfa042eaa3b679127ca0815c04b4dc489598d20be9b258.jpg', '2', '45645');
INSERT INTO `t_fbc_child` VALUES ('39', '1', null, 'SC0001', 'GR0005', '19', '唐晏', '男', '325896200805029625', '2008-05-02', '169.00', '80.00', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/tmp_62e9991b626d83e2696c2e1eceafc236.jpg', '1', '');
INSERT INTO `t_fbc_child` VALUES ('40', '1', null, 'SC0001', 'GR0007', '34', '张丹龙', '男', '352804201302018526', '2013-02-01', '190.00', '85.00', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/tmp_f6882e322ae9b7c7626fe923d0746f94.jpg', '2', '信息错误');

-- ----------------------------
-- Table structure for t_fbc_child_parents
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_child_parents`;
CREATE TABLE `t_fbc_child_parents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `child_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `club_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`child_id`,`parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_fbc_child_parents
-- ----------------------------
INSERT INTO `t_fbc_child_parents` VALUES ('20', '25', '512', '1');
INSERT INTO `t_fbc_child_parents` VALUES ('21', '26', '513', '1');
INSERT INTO `t_fbc_child_parents` VALUES ('22', '27', '513', '1');
INSERT INTO `t_fbc_child_parents` VALUES ('23', '28', '513', '1');
INSERT INTO `t_fbc_child_parents` VALUES ('24', '29', '513', '1');
INSERT INTO `t_fbc_child_parents` VALUES ('27', '30', '513', '1');
INSERT INTO `t_fbc_child_parents` VALUES ('28', '31', '513', '1');
INSERT INTO `t_fbc_child_parents` VALUES ('31', '35', '512', '1');
INSERT INTO `t_fbc_child_parents` VALUES ('35', '39', '521', '1');
INSERT INTO `t_fbc_child_parents` VALUES ('36', '40', '521', '1');

-- ----------------------------
-- Table structure for t_fbc_class
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_class`;
CREATE TABLE `t_fbc_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部名称',
  `name` varchar(50) DEFAULT NULL COMMENT '班级名称',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `number` int(11) DEFAULT '0' COMMENT '班级人数',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  `author` varchar(50) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='班级';

-- ----------------------------
-- Records of t_fbc_class
-- ----------------------------
INSERT INTO `t_fbc_class` VALUES ('19', '1', 'U8普通（连松）', '56', '6', '2020-08-13 18:00:36', '2020-08-26 13:56:12', null);
INSERT INTO `t_fbc_class` VALUES ('23', '1', 'U8精英队', '55', '2', '2020-08-13 18:02:15', '2020-08-26 14:50:29', null);
INSERT INTO `t_fbc_class` VALUES ('28', '1', 'U11精英队', '90', '0', '2020-08-13 18:03:12', '2020-08-26 10:57:35', null);
INSERT INTO `t_fbc_class` VALUES ('29', '1', 'U9（男队）', '50', '0', '2020-08-13 18:04:40', '2020-08-26 10:27:47', null);
INSERT INTO `t_fbc_class` VALUES ('33', '1', 'U10精英队', '54', '0', '2020-08-13 18:34:28', '2020-08-13 18:38:51', null);
INSERT INTO `t_fbc_class` VALUES ('34', '1', 'U6一队、二队', '49', '0', '2020-08-13 18:34:41', '2020-08-13 18:34:41', null);
INSERT INTO `t_fbc_class` VALUES ('45', '1', 'U11、U10、U9混 女队', '60', '0', '2020-08-14 18:27:29', '2020-08-14 18:27:29', null);
INSERT INTO `t_fbc_class` VALUES ('51', '1', 'U10/U9混男子队', '52', '1', '2020-08-14 18:27:28', '2020-08-14 18:27:28', null);
INSERT INTO `t_fbc_class` VALUES ('52', '1', 'U10/U9混女子队', '63', '0', '2020-08-14 18:27:28', '2020-08-14 18:27:28', null);
INSERT INTO `t_fbc_class` VALUES ('53', '1', 'U11、U10、U9混', '115', '0', '2020-08-14 18:27:28', '2020-08-14 18:27:28', null);
INSERT INTO `t_fbc_class` VALUES ('54', '1', 'U13以上（男队）', '114', '0', '2020-08-14 18:27:28', '2020-08-14 18:27:28', null);
INSERT INTO `t_fbc_class` VALUES ('55', '1', 'U13以上（女队）', '113', '0', '2020-08-14 18:27:28', '2020-08-14 18:27:28', null);
INSERT INTO `t_fbc_class` VALUES ('56', '1', 'U7（2013级）', '118', '0', '2020-08-14 18:27:28', '2020-08-14 18:27:28', null);
INSERT INTO `t_fbc_class` VALUES ('57', '1', 'U7、U8混女子队（2013级）', '119', '0', '2020-08-14 18:27:28', '2020-08-14 18:27:28', null);
INSERT INTO `t_fbc_class` VALUES ('58', '1', 'U7、U8混（2012级）', '120', '0', '2020-08-14 18:27:28', '2020-08-14 18:27:28', null);
INSERT INTO `t_fbc_class` VALUES ('59', '1', 'U7、U8混男女队（2013级）', '121', '0', '2020-08-14 18:27:28', '2020-08-14 18:27:28', null);
INSERT INTO `t_fbc_class` VALUES ('60', '1', 'U9/U10梯队', '122', '0', '2020-08-14 18:27:28', '2020-08-25 17:34:32', null);
INSERT INTO `t_fbc_class` VALUES ('61', '1', 'U7精英队', '123', '0', '2020-08-26 09:59:06', '2020-08-26 11:40:29', null);
INSERT INTO `t_fbc_class` VALUES ('62', '1', 'U8、U9混', '124', '0', '2020-08-26 10:04:42', '2020-08-26 10:04:42', null);
INSERT INTO `t_fbc_class` VALUES ('63', '1', 'U8普通（周浩）', '125', '0', '2020-08-26 10:25:59', '2020-08-26 14:02:21', null);
INSERT INTO `t_fbc_class` VALUES ('64', '1', 'U9（女队）', '126', '0', '2020-08-26 10:27:59', '2020-08-26 10:27:59', null);
INSERT INTO `t_fbc_class` VALUES ('88', '1', '测试梯队123', '127', '0', '2020-12-15 13:50:32', '2020-12-15 13:55:28', null);
INSERT INTO `t_fbc_class` VALUES ('91', '1', '测试', '128', '0', '2021-01-04 16:07:54', '2021-01-04 16:07:54', 'zy');
INSERT INTO `t_fbc_class` VALUES ('92', '1', '精英队', '129', '0', '2021-02-01 14:35:02', '2021-02-01 14:35:02', 'zy');
INSERT INTO `t_fbc_class` VALUES ('94', '0', '11', '130', '0', '2021-04-22 16:55:48', '2021-04-22 16:55:48', '后台管理');

-- ----------------------------
-- Table structure for t_fbc_club
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_club`;
CREATE TABLE `t_fbc_club` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `jgid` int(11) DEFAULT NULL COMMENT '机构ID',
  `name` varchar(100) DEFAULT NULL COMMENT '俱乐部名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='俱乐部';

-- ----------------------------
-- Records of t_fbc_club
-- ----------------------------
INSERT INTO `t_fbc_club` VALUES ('1', '1', '众焱足球');

-- ----------------------------
-- Table structure for t_fbc_club_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_club_menu`;
CREATE TABLE `t_fbc_club_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `menu_id` int(11) DEFAULT NULL COMMENT '菜单id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=276 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_fbc_club_menu
-- ----------------------------
INSERT INTO `t_fbc_club_menu` VALUES ('1', '0', '1100');
INSERT INTO `t_fbc_club_menu` VALUES ('2', '0', '1101');
INSERT INTO `t_fbc_club_menu` VALUES ('3', '0', '1102');
INSERT INTO `t_fbc_club_menu` VALUES ('4', '0', '1103');
INSERT INTO `t_fbc_club_menu` VALUES ('5', '0', '2200');
INSERT INTO `t_fbc_club_menu` VALUES ('6', '0', '2201');
INSERT INTO `t_fbc_club_menu` VALUES ('7', '0', '2202');
INSERT INTO `t_fbc_club_menu` VALUES ('8', '0', '2203');
INSERT INTO `t_fbc_club_menu` VALUES ('9', '0', '3300');
INSERT INTO `t_fbc_club_menu` VALUES ('10', '0', '3301');
INSERT INTO `t_fbc_club_menu` VALUES ('11', '0', '3302');
INSERT INTO `t_fbc_club_menu` VALUES ('12', '0', '4400');
INSERT INTO `t_fbc_club_menu` VALUES ('13', '0', '4401');
INSERT INTO `t_fbc_club_menu` VALUES ('14', '0', '4402');
INSERT INTO `t_fbc_club_menu` VALUES ('15', '0', '5500');
INSERT INTO `t_fbc_club_menu` VALUES ('16', '0', '5501');
INSERT INTO `t_fbc_club_menu` VALUES ('17', '0', '5502');
INSERT INTO `t_fbc_club_menu` VALUES ('18', '0', '5503');
INSERT INTO `t_fbc_club_menu` VALUES ('19', '0', '5504');
INSERT INTO `t_fbc_club_menu` VALUES ('20', '0', '5505');
INSERT INTO `t_fbc_club_menu` VALUES ('21', '0', '5506');
INSERT INTO `t_fbc_club_menu` VALUES ('22', '0', '6600');
INSERT INTO `t_fbc_club_menu` VALUES ('23', '0', '6601');
INSERT INTO `t_fbc_club_menu` VALUES ('24', '0', '6602');
INSERT INTO `t_fbc_club_menu` VALUES ('25', '0', '6603');
INSERT INTO `t_fbc_club_menu` VALUES ('26', '0', '6604');
INSERT INTO `t_fbc_club_menu` VALUES ('27', '0', '6605');
INSERT INTO `t_fbc_club_menu` VALUES ('28', '0', '6606');
INSERT INTO `t_fbc_club_menu` VALUES ('249', '1', '1100');
INSERT INTO `t_fbc_club_menu` VALUES ('250', '1', '1101');
INSERT INTO `t_fbc_club_menu` VALUES ('251', '1', '1102');
INSERT INTO `t_fbc_club_menu` VALUES ('252', '1', '1103');
INSERT INTO `t_fbc_club_menu` VALUES ('253', '1', '2200');
INSERT INTO `t_fbc_club_menu` VALUES ('254', '1', '2201');
INSERT INTO `t_fbc_club_menu` VALUES ('255', '1', '2202');
INSERT INTO `t_fbc_club_menu` VALUES ('256', '1', '2203');
INSERT INTO `t_fbc_club_menu` VALUES ('257', '1', '3300');
INSERT INTO `t_fbc_club_menu` VALUES ('258', '1', '3301');
INSERT INTO `t_fbc_club_menu` VALUES ('259', '1', '3302');
INSERT INTO `t_fbc_club_menu` VALUES ('260', '1', '4400');
INSERT INTO `t_fbc_club_menu` VALUES ('261', '1', '4401');
INSERT INTO `t_fbc_club_menu` VALUES ('262', '1', '4402');
INSERT INTO `t_fbc_club_menu` VALUES ('263', '1', '5500');
INSERT INTO `t_fbc_club_menu` VALUES ('264', '1', '5501');
INSERT INTO `t_fbc_club_menu` VALUES ('265', '1', '5502');
INSERT INTO `t_fbc_club_menu` VALUES ('266', '1', '5503');
INSERT INTO `t_fbc_club_menu` VALUES ('267', '1', '5504');
INSERT INTO `t_fbc_club_menu` VALUES ('268', '1', '5505');
INSERT INTO `t_fbc_club_menu` VALUES ('269', '1', '5506');
INSERT INTO `t_fbc_club_menu` VALUES ('270', '1', '6602');
INSERT INTO `t_fbc_club_menu` VALUES ('271', '1', '6603');
INSERT INTO `t_fbc_club_menu` VALUES ('272', '1', '6604');
INSERT INTO `t_fbc_club_menu` VALUES ('273', '1', '6605');
INSERT INTO `t_fbc_club_menu` VALUES ('274', '1', '6606');
INSERT INTO `t_fbc_club_menu` VALUES ('275', '1', '6600');

-- ----------------------------
-- Table structure for t_fbc_coach
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_coach`;
CREATE TABLE `t_fbc_coach` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部ID',
  `name` varchar(20) DEFAULT NULL COMMENT '教练员名称',
  `sex` char(2) DEFAULT NULL COMMENT '性别（男，女）',
  `title` varchar(20) DEFAULT NULL COMMENT '职证',
  `phone` varchar(255) DEFAULT NULL COMMENT '联系方式',
  `picture` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '球员照片',
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='学员/小孩';

-- ----------------------------
-- Records of t_fbc_coach
-- ----------------------------
INSERT INTO `t_fbc_coach` VALUES ('51', '1', '刘小帅', '男', '高级教练员', '17681099240', null, '142');
INSERT INTO `t_fbc_coach` VALUES ('54', '1', '唐教练', '男', '', '13856951396', '', '154');
INSERT INTO `t_fbc_coach` VALUES ('55', '1', '李教练', '男', '', '13156899876', '', '0');
INSERT INTO `t_fbc_coach` VALUES ('56', '1', '张教练', '男', '', '15805608840', '', '146');
INSERT INTO `t_fbc_coach` VALUES ('57', '1', '唐风教练', '男', '', '13621318846', '', '0');

-- ----------------------------
-- Table structure for t_fbc_coach_class
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_coach_class`;
CREATE TABLE `t_fbc_coach_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `coach_id` int(11) DEFAULT NULL COMMENT '用户id',
  `class_id` int(11) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_fbc_coach_class
-- ----------------------------
INSERT INTO `t_fbc_coach_class` VALUES ('50', '1', '52', '19');
INSERT INTO `t_fbc_coach_class` VALUES ('51', '1', '53', '19');
INSERT INTO `t_fbc_coach_class` VALUES ('52', '1', '51', '23');
INSERT INTO `t_fbc_coach_class` VALUES ('53', '1', '51', '19');
INSERT INTO `t_fbc_coach_class` VALUES ('54', '1', '54', '19');
INSERT INTO `t_fbc_coach_class` VALUES ('55', '1', '55', '19');
INSERT INTO `t_fbc_coach_class` VALUES ('57', '1', '56', '34');
INSERT INTO `t_fbc_coach_class` VALUES ('58', '1', '57', '29');

-- ----------------------------
-- Table structure for t_fbc_cs_club_user
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_cs_club_user`;
CREATE TABLE `t_fbc_cs_club_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) DEFAULT NULL COMMENT 'cs_user表id',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部ID',
  `type` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '用户类型: 1(普通用户)  2(管理员)',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_fbc_cs_club_user
-- ----------------------------
INSERT INTO `t_fbc_cs_club_user` VALUES ('146', '142', '1', '1,2', '2020-12-11 10:08:17');
INSERT INTO `t_fbc_cs_club_user` VALUES ('147', '143', '1', '1,2', '2020-12-11 10:08:17');
INSERT INTO `t_fbc_cs_club_user` VALUES ('149', '145', '1', '1', '2020-12-14 14:10:04');
INSERT INTO `t_fbc_cs_club_user` VALUES ('150', '146', '1', '1', '2020-12-14 16:16:23');
INSERT INTO `t_fbc_cs_club_user` VALUES ('151', '147', '1', '1', '2020-12-15 14:37:58');
INSERT INTO `t_fbc_cs_club_user` VALUES ('152', '148', '1', '1', '2021-01-07 17:04:06');
INSERT INTO `t_fbc_cs_club_user` VALUES ('158', '154', '1', '1', '2021-02-02 10:37:18');
INSERT INTO `t_fbc_cs_club_user` VALUES ('159', '155', '1', '1', '2021-03-01 15:25:38');
INSERT INTO `t_fbc_cs_club_user` VALUES ('160', '156', '1', '1', '2021-07-05 20:33:08');
INSERT INTO `t_fbc_cs_club_user` VALUES ('161', '157', '1', '1', '2021-07-19 21:26:14');
INSERT INTO `t_fbc_cs_club_user` VALUES ('162', '158', '1', '1', '2021-07-29 15:51:13');
INSERT INTO `t_fbc_cs_club_user` VALUES ('163', '159', '1', '1', '2021-09-26 22:04:52');

-- ----------------------------
-- Table structure for t_fbc_cs_user
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_cs_user`;
CREATE TABLE `t_fbc_cs_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '姓名',
  `sex` tinyint(1) DEFAULT NULL COMMENT '性别  1-男、2-女',
  `phone` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '手机号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_fbc_cs_user
-- ----------------------------
INSERT INTO `t_fbc_cs_user` VALUES ('142', '刘小帅', '1', '17681099240');
INSERT INTO `t_fbc_cs_user` VALUES ('143', '刘丽丽', '2', '17681104171');
INSERT INTO `t_fbc_cs_user` VALUES ('145', '王环', '2', '15375490360');
INSERT INTO `t_fbc_cs_user` VALUES ('146', '朱靖靖', '2', '15805608840');
INSERT INTO `t_fbc_cs_user` VALUES ('147', '单世峰', '1', '18656131177');
INSERT INTO `t_fbc_cs_user` VALUES ('148', '安咯', '1', '18815597841');
INSERT INTO `t_fbc_cs_user` VALUES ('154', '唐寅', '2', '13856951396');
INSERT INTO `t_fbc_cs_user` VALUES ('155', '王依依', '2', '18269755531');
INSERT INTO `t_fbc_cs_user` VALUES ('156', '陈宝光', '2', '18933787697');
INSERT INTO `t_fbc_cs_user` VALUES ('157', '岑杰翔', '1', '18640433798');
INSERT INTO `t_fbc_cs_user` VALUES ('158', '霍先生', '1', '18155106060');
INSERT INTO `t_fbc_cs_user` VALUES ('159', '郑浩贤', '1', '13360557156');

-- ----------------------------
-- Table structure for t_fbc_game
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_game`;
CREATE TABLE `t_fbc_game` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `name` varchar(255) DEFAULT NULL COMMENT '比赛名称',
  `contact` varchar(100) DEFAULT NULL COMMENT '联系人',
  `phone` varchar(20) DEFAULT NULL COMMENT '联系方式',
  `start` datetime DEFAULT NULL COMMENT '开始时间',
  `end` datetime DEFAULT NULL COMMENT '结束时间',
  `mode` int(11) DEFAULT NULL COMMENT '缴费方式，1免费，2固定费用，3按人计算',
  `total` decimal(10,2) DEFAULT NULL COMMENT '固定费用金额',
  `cap_money` decimal(10,2) DEFAULT NULL COMMENT '领队每人费用',
  `coa_money` decimal(10,2) DEFAULT NULL COMMENT '教练员每人费用',
  `stu_money` decimal(10,2) DEFAULT NULL COMMENT '球员每人费用',
  `status` int(1) DEFAULT '0' COMMENT '赛程状态：0, 未开始;1,进行中;2已结束',
  `introduction` text COMMENT '赛程简介',
  `location` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '比赛地址',
  `match_start_time` datetime DEFAULT NULL COMMENT '比赛开始时间',
  `match_end_time` datetime DEFAULT NULL COMMENT '比赛结束时间',
  `lastoptime` datetime DEFAULT NULL COMMENT '最后操作时间',
  `picture` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '图片url',
  `expire` int(11) DEFAULT NULL COMMENT '删除标识 1未删除 2已删除',
  `mini_number` int(11) DEFAULT NULL COMMENT '参加比赛最少人数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='赛程表';

-- ----------------------------
-- Records of t_fbc_game
-- ----------------------------
INSERT INTO `t_fbc_game` VALUES ('51', '1', '2020年度U8精英队冬季赛', '刘小帅', '17681099240', '2020-12-11 10:17:00', '2020-12-17 11:01:00', '3', null, '10.00', '10.00', '10.00', '2', '<p>欢迎大家踊跃报名~</p>', '合肥体育中心', '2020-12-11 00:00:00', '2020-12-30 00:00:00', null, 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607653078_19.jpg', '1', null);
INSERT INTO `t_fbc_game` VALUES ('52', '1', '2020百事可乐杯踢嗨小将全国青少年足球精英邀请赛U8组', '李宁', '13605513807', '2020-12-10 13:11:00', '2020-12-14 14:27:00', '1', null, null, null, null, '2', '<p><img src=\"https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20201214095657.jpg?Expires=1923271022&amp;OSSAccessKeyId=LTAI4FsUjvLaQdvxhAp6Exsv&amp;Signature=mdOk5LlJMqmc1oYsMAEqKxSnPA0%3D\" alt=\"\" width=\"1080\" height=\"1913\" /></p>', '合肥名流足球公园', '2020-12-15 13:00:00', '2020-12-18 14:11:00', null, 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1608001594_比赛-more11.jpg', '1', null);
INSERT INTO `t_fbc_game` VALUES ('57', '1', '冬日足球嘉年华', '唐队', '13856951396', '2021-01-19 11:12:00', '2021-01-28 10:21:00', '3', '0.00', '20.00', '20.00', '20.00', '2', '<p>快来加入吧</p>', null, '2020-12-26 09:00:00', '2021-01-30 15:00:00', null, 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1608007563_4832', '1', null);
INSERT INTO `t_fbc_game` VALUES ('58', '1', '20201228', '王', '15375490360', '2020-12-28 10:43:00', '2020-12-29 00:00:00', '1', null, null, null, null, '2', '<p>11</p>', null, '2020-12-29 00:00:00', '2020-12-30 00:00:00', '2020-12-28 14:51:28', '', '2', null);
INSERT INTO `t_fbc_game` VALUES ('59', '1', '2021春季赛', '唐教练', '13856951396', '2021-02-03 15:15:00', '2021-02-06 16:00:00', '3', '0.00', '50.00', '60.00', '30.00', '2', '<p>快来参赛吧！</p>', null, '2021-02-10 08:00:00', '2021-02-15 17:00:00', null, 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1612336661_4313', '1', null);
INSERT INTO `t_fbc_game` VALUES ('60', '1', '[复制]  2021春季赛', '唐教练', '13856951396', '2021-02-03 15:15:00', '2021-02-06 16:00:00', '3', '0.00', '50.00', '60.00', '30.00', '2', '<p>快来参赛吧！</p>', null, '2021-02-10 08:00:00', '2021-02-15 17:00:00', '2021-02-03 16:23:29', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1612336661_4313', '2', null);
INSERT INTO `t_fbc_game` VALUES ('61', '1', '[复制]  2021春季赛', '唐教练', '13856951396', '2021-02-03 15:15:00', '2021-02-06 16:00:00', '3', '0.00', '50.00', '60.00', '30.00', '2', '<p>快来参赛吧！</p>', null, '2021-02-10 08:00:00', '2021-02-15 17:00:00', null, 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1612336661_4313', '1', null);

-- ----------------------------
-- Table structure for t_fbc_game_activity
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_game_activity`;
CREATE TABLE `t_fbc_game_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `game_id` int(10) unsigned DEFAULT NULL COMMENT '赛事id',
  `title` varchar(100) DEFAULT NULL COMMENT '活动标题',
  `desc` text COMMENT '活动简介',
  `status` int(11) DEFAULT '0' COMMENT '活动状态，0未开始，1进行中，2已结束',
  `start_time` datetime DEFAULT NULL COMMENT '活动开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '活动结束时间',
  `show_status` int(11) DEFAULT '0' COMMENT '活动显示状态，0不显示，1显示',
  `picture` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '活动照片',
  `ad_picture` varchar(255) DEFAULT NULL COMMENT '广告位图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='赛事活动表';

-- ----------------------------
-- Records of t_fbc_game_activity
-- ----------------------------
INSERT INTO `t_fbc_game_activity` VALUES ('13', '1', '52', '最佳人气王足球小将投票', '<p>正在更新，请稍后关注...</p>', '2', '2020-12-15 10:53:00', '2020-12-31 00:00:00', '1', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1608000908_投票宣传页.jpg', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1608001563_投票.jpg');
INSERT INTO `t_fbc_game_activity` VALUES ('15', '1', '57', '最佳人气王足球小将', '<p>赶快来投票吧</p>', '2', '2021-01-28 09:14:00', '2021-01-29 00:00:00', '1', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1611796493_投票(1).jpg', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1611796548_投票(1).jpg');
INSERT INTO `t_fbc_game_activity` VALUES ('16', '1', '59', '最佳球员', '<p>快来投票吧！</p>', '2', '2021-02-10 13:00:00', '2021-02-14 16:00:00', '0', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1612401747_赛事图片.jpg', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1612401758_投票(1).jpg');

-- ----------------------------
-- Table structure for t_fbc_game_against
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_game_against`;
CREATE TABLE `t_fbc_game_against` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `game_id` int(11) DEFAULT NULL COMMENT '赛事id',
  `round` varchar(255) DEFAULT NULL COMMENT '回合',
  `session` int(11) DEFAULT NULL COMMENT '场次',
  `start_date` date DEFAULT NULL COMMENT '开始日期',
  `start_time` varchar(100) DEFAULT NULL COMMENT '比赛开始时间',
  `end_time` varchar(100) DEFAULT NULL COMMENT '比赛结束时间',
  `location` varchar(255) DEFAULT NULL COMMENT '比赛地点',
  `team_left_id` int(11) DEFAULT NULL COMMENT '左队id',
  `team_right_id` int(11) DEFAULT NULL COMMENT '右队id',
  `score_left` int(11) DEFAULT '0' COMMENT '左队得分',
  `score_right` int(11) DEFAULT '0' COMMENT '右队得分',
  `release_time` datetime DEFAULT NULL COMMENT '该轮比赛发布时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=267 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='赛事对阵表';

-- ----------------------------
-- Records of t_fbc_game_against
-- ----------------------------
INSERT INTO `t_fbc_game_against` VALUES ('179', '1', '51', '第一轮', '1', '2020-12-22', '09:00', '11:30', '合肥体育中心', '136', '137', '2', '1', '2020-12-11 10:52:09');
INSERT INTO `t_fbc_game_against` VALUES ('240', '1', '52', '第1轮', '1', '2020-12-15', '08:00', '09:00', '合肥名流足球公园', '164', '166', '0', '0', '2020-12-14 15:59:58');
INSERT INTO `t_fbc_game_against` VALUES ('241', '1', '52', '第1轮', '2', '2020-12-15', '09:30', '10:30', '合肥名流足球公园', '167', '165', '0', '0', '2020-12-14 15:59:58');
INSERT INTO `t_fbc_game_against` VALUES ('242', '1', '52', '第2轮', '1', '2020-12-15', '14:00', '15:00', '合肥名流足球公园', '168', '166', '0', '0', '2020-12-14 15:59:58');
INSERT INTO `t_fbc_game_against` VALUES ('243', '1', '52', '第2轮', '2', '2020-12-15', '15:30', '16:30', '合肥名流足球公园', '164', '167', '0', '0', '2020-12-14 15:59:58');
INSERT INTO `t_fbc_game_against` VALUES ('244', '1', '52', '第3轮', '1', '2020-12-15', '18:30', '17:30', '合肥名流足球公园', '168', '165', '0', '0', '2020-12-14 15:59:58');
INSERT INTO `t_fbc_game_against` VALUES ('245', '1', '52', '第3轮', '2', '2020-12-15', '20:00', '21:00', '合肥名流足球公园', '166', '167', '0', '0', '2020-12-14 15:59:58');
INSERT INTO `t_fbc_game_against` VALUES ('246', '1', '52', '第4轮', '1', '2020-12-16', '08:00', '09:00', '合肥名流足球公园', '168', '167', '0', '0', '2020-12-14 15:59:58');
INSERT INTO `t_fbc_game_against` VALUES ('247', '1', '52', '第4轮', '2', '2020-12-16', '09:30', '10:30', '合肥名流足球公园', '165', '164', '0', '0', '2020-12-14 15:59:58');
INSERT INTO `t_fbc_game_against` VALUES ('248', '1', '52', '第5轮', '1', '2020-12-16', '13:00', '14:00', '合肥名流足球公园', '168', '164', '0', '0', '2020-12-14 15:59:58');
INSERT INTO `t_fbc_game_against` VALUES ('249', '1', '52', '第5轮', '2', '2020-12-16', '14:30', '15:30', '合肥名流足球公园', '165', '166', '0', '0', '2020-12-14 15:59:58');
INSERT INTO `t_fbc_game_against` VALUES ('250', '1', '52', '第6轮', '1', '2020-12-16', '17:00', '18:00', '合肥名流足球公园', '166', '164', '0', '0', '2020-12-14 15:59:58');
INSERT INTO `t_fbc_game_against` VALUES ('251', '1', '52', '第6轮', '2', '2020-12-16', '18:30', '19:30', '合肥名流足球公园', '165', '167', '0', '0', '2020-12-14 15:59:58');
INSERT INTO `t_fbc_game_against` VALUES ('252', '1', '52', '第7轮', '1', '2020-12-17', '08:00', '09:00', '合肥名流足球公园', '166', '168', '0', '0', '2020-12-14 15:59:59');
INSERT INTO `t_fbc_game_against` VALUES ('253', '1', '52', '第7轮', '2', '2020-12-17', '09:30', '10:30', '合肥名流足球公园', '167', '164', '0', '0', '2020-12-14 15:59:59');
INSERT INTO `t_fbc_game_against` VALUES ('254', '1', '52', '第8轮', '1', '2020-12-17', '14:00', '15:00', '合肥名流足球公园', '165', '168', '0', '0', '2020-12-14 15:59:59');
INSERT INTO `t_fbc_game_against` VALUES ('255', '1', '52', '第8轮', '2', '2020-12-17', '15:30', '16:30', '合肥名流足球公园', '167', '166', '0', '0', '2020-12-14 15:59:59');
INSERT INTO `t_fbc_game_against` VALUES ('256', '1', '52', '第9轮', '1', '2020-12-17', '18:30', '17:30', '合肥名流足球公园', '164', '165', '0', '0', '2020-12-14 15:59:59');
INSERT INTO `t_fbc_game_against` VALUES ('257', '1', '52', '第9轮', '2', '2020-12-17', '20:00', '21:00', '合肥名流足球公园', '167', '168', '0', '0', '2020-12-14 15:59:59');
INSERT INTO `t_fbc_game_against` VALUES ('258', '1', '52', '第10轮', '3', '2020-12-18', '07:30', '08:30', '合肥名流足球公园', '166', '165', '0', '0', '2020-12-14 15:59:59');
INSERT INTO `t_fbc_game_against` VALUES ('259', '1', '52', '第10轮', '4', '2020-12-18', '07:30', '08:30', '合肥名流足球公园', '164', '168', '0', '0', '2020-12-14 15:59:59');
INSERT INTO `t_fbc_game_against` VALUES ('264', '1', '57', '1', '1', '2020-12-22', '08:30', '11:06', '合肥', '177', '180', '1', '0', '2020-12-16 11:06:43');
INSERT INTO `t_fbc_game_against` VALUES ('265', '1', '57', '1', '2', '2021-01-28', '08:47', '10:47', '合肥', '180', '185', '0', '1', '2021-01-21 09:46:56');
INSERT INTO `t_fbc_game_against` VALUES ('266', '1', '59', '1', '1', '2021-02-10', '09:50', '11:30', '合肥', '186', '187', '0', '0', '2021-02-03 16:59:23');

-- ----------------------------
-- Table structure for t_fbc_game_against_referee
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_game_against_referee`;
CREATE TABLE `t_fbc_game_against_referee` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `against_id` int(11) DEFAULT NULL COMMENT '对阵表id',
  `referee_id` int(11) DEFAULT NULL COMMENT '裁判id',
  `type` int(11) DEFAULT '1' COMMENT '类型：1，助理裁判；2，主裁判；3，第四官员',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='赛事对阵表';

-- ----------------------------
-- Records of t_fbc_game_against_referee
-- ----------------------------
INSERT INTO `t_fbc_game_against_referee` VALUES ('198', '180', null, '2');
INSERT INTO `t_fbc_game_against_referee` VALUES ('199', '180', null, '3');
INSERT INTO `t_fbc_game_against_referee` VALUES ('200', '220', null, '2');
INSERT INTO `t_fbc_game_against_referee` VALUES ('201', '220', '9', '3');
INSERT INTO `t_fbc_game_against_referee` VALUES ('204', '221', null, '2');
INSERT INTO `t_fbc_game_against_referee` VALUES ('205', '221', null, '3');
INSERT INTO `t_fbc_game_against_referee` VALUES ('212', '226', null, '2');
INSERT INTO `t_fbc_game_against_referee` VALUES ('213', '226', null, '3');
INSERT INTO `t_fbc_game_against_referee` VALUES ('214', '227', null, '2');
INSERT INTO `t_fbc_game_against_referee` VALUES ('215', '227', null, '3');
INSERT INTO `t_fbc_game_against_referee` VALUES ('216', '233', null, '2');
INSERT INTO `t_fbc_game_against_referee` VALUES ('217', '233', null, '3');
INSERT INTO `t_fbc_game_against_referee` VALUES ('218', '238', null, '2');
INSERT INTO `t_fbc_game_against_referee` VALUES ('219', '238', null, '3');
INSERT INTO `t_fbc_game_against_referee` VALUES ('220', '239', null, '2');
INSERT INTO `t_fbc_game_against_referee` VALUES ('221', '239', null, '3');
INSERT INTO `t_fbc_game_against_referee` VALUES ('222', '232', null, '2');
INSERT INTO `t_fbc_game_against_referee` VALUES ('223', '232', null, '3');
INSERT INTO `t_fbc_game_against_referee` VALUES ('224', '260', null, '2');
INSERT INTO `t_fbc_game_against_referee` VALUES ('225', '260', '9', '3');
INSERT INTO `t_fbc_game_against_referee` VALUES ('232', '262', null, '2');
INSERT INTO `t_fbc_game_against_referee` VALUES ('233', '262', '9', '3');
INSERT INTO `t_fbc_game_against_referee` VALUES ('234', '263', null, '2');
INSERT INTO `t_fbc_game_against_referee` VALUES ('235', '263', '10', '3');
INSERT INTO `t_fbc_game_against_referee` VALUES ('238', '179', null, '2');
INSERT INTO `t_fbc_game_against_referee` VALUES ('239', '179', '9', '3');
INSERT INTO `t_fbc_game_against_referee` VALUES ('242', '264', null, '2');
INSERT INTO `t_fbc_game_against_referee` VALUES ('243', '264', '10', '3');
INSERT INTO `t_fbc_game_against_referee` VALUES ('250', '265', '11', '2');
INSERT INTO `t_fbc_game_against_referee` VALUES ('251', '265', '11', '3');
INSERT INTO `t_fbc_game_against_referee` VALUES ('252', '265', '11', '1');
INSERT INTO `t_fbc_game_against_referee` VALUES ('253', '266', '11', '2');
INSERT INTO `t_fbc_game_against_referee` VALUES ('254', '266', '11', '3');
INSERT INTO `t_fbc_game_against_referee` VALUES ('255', '266', '11', '1');

-- ----------------------------
-- Table structure for t_fbc_game_footballer
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_game_footballer`;
CREATE TABLE `t_fbc_game_footballer` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户中心表id',
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '球员名字',
  `sex` varchar(2) CHARACTER SET utf8 DEFAULT NULL COMMENT '性别',
  `height` double(20,2) DEFAULT NULL COMMENT '身高',
  `weight` double(20,2) DEFAULT NULL COMMENT '体重',
  `phone` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '手机',
  `birth_date` date DEFAULT NULL,
  `id_card` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '身份证',
  `picture` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '球员照片',
  `type` int(11) DEFAULT NULL COMMENT '证件类型 1身份证 2护照',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=693 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_fbc_game_footballer
-- ----------------------------
INSERT INTO `t_fbc_game_footballer` VALUES ('467', '1', '142', '刘帅帅', '男', '170.00', '50.00', '17681099240', '1998-08-25', '341226199808255123', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1608001344_9.jpg', '1');
INSERT INTO `t_fbc_game_footballer` VALUES ('468', '1', '143', '刘丽丽', '女', '165.00', '50.00', '17681104171', '1998-08-25', '341226199808255124', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607654857_7.jpg', '1');
INSERT INTO `t_fbc_game_footballer` VALUES ('610', '1', '145', '吴孟泽', '男', '160.00', '50.00', '15375490360', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('611', '1', '0', '刘博瑞', '男', '160.00', '50.00', '15375490361', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('612', '1', '0', '叶志远', '男', '160.00', '50.00', '15375490362', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('613', '1', '0', '朱李建成', '男', '160.00', '50.00', '15375490363', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('614', '1', '0', '胡曦元', '男', '160.00', '50.00', '15375490364', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('615', '1', '0', '李湖', '男', '160.00', '50.00', '15375490365', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('616', '1', '0', '连鹏吉', '男', '160.00', '50.00', '15375490366', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('617', '1', '0', '马陈哲', '男', '160.00', '50.00', '15375490367', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('618', '1', '0', '金铭凯', '男', '160.00', '50.00', '15375490368', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('619', '1', '0', '王金洋', '男', '160.00', '50.00', '15375490369', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('620', '1', '0', '朱嘉瑞', '男', '160.00', '50.00', '15375490370', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('621', '1', '0', '葛皓天', '男', '160.00', '50.00', '15375490371', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('622', '1', '0', '方明宇', '男', '160.00', '50.00', '15375490372', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('623', '1', '0', '崔智博', '男', '160.00', '50.00', '15375490373', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('624', '1', '0', '苏麒霖', '男', '160.00', '50.00', '15375490374', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('625', '1', '0', '刘戴铖', '男', '160.00', '50.00', '15375490375', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('626', '1', '0', '孙瑞阳', '男', '160.00', '50.00', '15375490376', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('627', '1', '0', '郭弈泽', '男', '160.00', '50.00', '15375490377', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('628', '1', '0', '许梓睿', '男', '160.00', '50.00', '15375490378', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('629', '1', '0', '董子铭', '男', '160.00', '50.00', '15375490379', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('630', '1', '0', '丁一龙', '男', '160.00', '50.00', '15375490380', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('631', '1', '0', '高梓宸', '男', '160.00', '50.00', '15375490381', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('632', '1', '0', '王逍羽', '男', '160.00', '50.00', '15375490382', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('633', '1', '0', '姜弈帆', '男', '160.00', '50.00', '15375490383', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('634', '1', '0', '张雨腾', '男', '160.00', '50.00', '15375490384', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('635', '1', '0', '王弈辰', '男', '160.00', '50.00', '15375490385', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('636', '1', '0', '王浩然', '男', '160.00', '50.00', '15375490386', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('637', '1', '0', '辛梓扬', '男', '160.00', '50.00', '15375490387', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('638', '1', '0', '刘鑫宇', '男', '160.00', '50.00', '15375490388', '1998-08-25', '341226199808255123', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607932901_1.jpg', '1');
INSERT INTO `t_fbc_game_footballer` VALUES ('639', '1', '0', '王迦诺', '男', '160.00', '50.00', '15375490389', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('640', '1', '0', '夏浩然', '男', '160.00', '50.00', '15375490390', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('641', '1', '0', '孟航宇', '男', '160.00', '50.00', '15375490391', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('642', '1', '0', '余中昊', '男', '160.00', '50.00', '15375490392', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('643', '1', '0', '茹景颢', '男', '160.00', '50.00', '15375490393', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('644', '1', '0', '刘修泽', '男', '160.00', '50.00', '15375490394', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('645', '1', '0', '吴天奥', '男', '160.00', '50.00', '15375490395', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('646', '1', '0', '付炜航', '男', '160.00', '50.00', '15375490396', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('647', '1', '0', '谢振华', '男', '160.00', '50.00', '15375490397', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('648', '1', '0', '孙乐天', '男', '160.00', '50.00', '15375490398', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('649', '1', '0', '赵浩钦', '男', '160.00', '50.00', '15375490399', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('650', '1', '0', '吴赵隽皓', '男', '160.00', '50.00', '15375490400', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('651', '1', '0', '闫欣泽', '男', '160.00', '50.00', '15375490401', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('652', '1', '0', '李唯赫', '男', '160.00', '50.00', '15375490402', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('653', '1', '0', '周恒立', '男', '160.00', '50.00', '15375490403', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('654', '1', '0', '韩梓旭', '男', '160.00', '50.00', '15375490404', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('655', '1', '0', '张旭', '男', '160.00', '50.00', '15375490405', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('656', '1', '0', '蒋鸣', '男', '160.00', '50.00', '15375490406', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('657', '1', '0', '郑思汉', '男', '160.00', '50.00', '15375490407', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('658', '1', '0', '朱晨铭', '男', '160.00', '50.00', '15375490408', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('659', '1', '0', '王文昊', '男', '160.00', '50.00', '15375490409', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('660', '1', '0', '张嘉逸', '男', '160.00', '50.00', '15375490410', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('661', '1', '0', '郑博宇', '男', '160.00', '50.00', '15375490411', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('662', '1', '0', '张富豪', '男', '160.00', '50.00', '15375490412', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('663', '1', '0', '朱浩然', '男', '160.00', '50.00', '15375490413', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('664', '1', '0', '邵诗彤', '女', '160.00', '50.00', '15375490414', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('665', '1', '0', '崔雅玥', '女', '160.00', '50.00', '15375490415', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('666', '1', '0', '吕欣怡', '女', '160.00', '50.00', '15375490416', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('667', '1', '0', '顾昕玥', '女', '160.00', '50.00', '15375490417', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('668', '1', '0', '付芷伊', '女', '160.00', '50.00', '15375490418', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('669', '1', '0', '邹帆', '女', '160.00', '50.00', '15375490419', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('670', '1', '0', '梁艺芯', '女', '160.00', '50.00', '15375490420', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('671', '1', '0', '钱雅琳', '女', '160.00', '50.00', '15375490421', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('672', '1', '0', '刘文曦', '女', '160.00', '50.00', '15375490422', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('673', '1', '0', '王柔瑾', '女', '160.00', '50.00', '15375490423', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('674', '1', '0', '夏书韵', '女', '160.00', '50.00', '15375490424', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('675', '1', '0', '张心然', '女', '160.00', '50.00', '15375490425', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('676', '1', '0', '王柔瑜', '女', '160.00', '50.00', '15375490426', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('677', '1', '0', '王佳欣', '女', '160.00', '50.00', '15375490427', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('678', '1', '0', '王艺菲', '女', '160.00', '50.00', '15375490428', '1993-06-05', '341125199306053652', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607930349_2.jpg', '1');
INSERT INTO `t_fbc_game_footballer` VALUES ('679', '1', '0', '王思彤', '女', '160.00', '50.00', '15375490429', '1993-06-05', '341125199306053652', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607930300_2.jpg', '1');
INSERT INTO `t_fbc_game_footballer` VALUES ('684', '1', '0', '王思2', '女', '160.00', '50.00', '15375490430', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('685', '1', '0', '王思3', '女', '161.00', '51.00', '15375490431', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('686', '1', '0', '王思4', '女', '162.00', '52.00', '15375490432', null, null, null, null);
INSERT INTO `t_fbc_game_footballer` VALUES ('687', '1', '0', '王思5', '女', '163.00', '52.00', '15375490433', '1998-08-23', '341226199808235123', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607933030_7.jpg', '1');
INSERT INTO `t_fbc_game_footballer` VALUES ('690', '1', '154', '唐鑫', '男', null, null, '13856951396', '2010-02-05', '252410201002053366', '', '1');
INSERT INTO `t_fbc_game_footballer` VALUES ('691', '1', '157', '岑杰翔', '男', '181.00', '66.00', '18640433798', '2004-10-08', '440785200410080016', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/tmp_3c3086e5e474241548fdb7422bd3f136.jpg', '1');
INSERT INTO `t_fbc_game_footballer` VALUES ('692', '1', '159', '郑浩贤', '男', '171.00', '48.00', '13360557156', '2003-09-11', '440107200309112210', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/tmp_8f5086c1f9049983de716223ceb0101e.jpg', '1');

-- ----------------------------
-- Table structure for t_fbc_game_footballer_team
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_game_footballer_team`;
CREATE TABLE `t_fbc_game_footballer_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ftb_id` int(11) DEFAULT NULL COMMENT '球员id',
  `team_id` int(11) DEFAULT NULL COMMENT '队伍id',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `status` int(11) DEFAULT NULL COMMENT '审核状态 0未审核 1审核通过 2审核未通过',
  `remark` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注 未通过原因',
  `number` int(11) DEFAULT NULL,
  `location` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '球员在球队中位置',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=747 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='球员领队关联表';

-- ----------------------------
-- Records of t_fbc_game_footballer_team
-- ----------------------------
INSERT INTO `t_fbc_game_footballer_team` VALUES ('520', '468', '58', '1', '1', null, '2', '守门员');
INSERT INTO `t_fbc_game_footballer_team` VALUES ('522', '467', '57', '1', '1', null, '0', '先锋');
INSERT INTO `t_fbc_game_footballer_team` VALUES ('523', '469', '59', '1', '1', null, '18', '中锋');
INSERT INTO `t_fbc_game_footballer_team` VALUES ('664', '467', '66', '1', '1', null, '1', '先锋');
INSERT INTO `t_fbc_game_footballer_team` VALUES ('665', '469', '66', '1', '1', null, '2', '中锋');
INSERT INTO `t_fbc_game_footballer_team` VALUES ('666', '610', '72', '1', '1', null, '1', '前锋');
INSERT INTO `t_fbc_game_footballer_team` VALUES ('667', '611', '72', '1', '1', null, '0', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('668', '612', '72', '1', '1', null, '0', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('669', '613', '72', '1', '1', null, '1', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('670', '614', '72', '1', '1', null, '4', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('671', '615', '72', '1', '1', null, '6', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('672', '616', '72', '1', '1', null, '7', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('673', '617', '72', '1', '1', null, '9', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('674', '618', '72', '1', '1', null, '11', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('675', '619', '72', '1', '1', null, '18', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('676', '620', '72', '1', '1', null, '20', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('677', '621', '72', '1', '1', null, '21', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('678', '622', '72', '1', '1', null, '26', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('679', '623', '72', '1', '1', null, '29', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('680', '624', '73', '1', '1', null, '2', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('681', '625', '73', '1', '1', null, '3', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('682', '626', '73', '1', '1', null, '6', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('683', '627', '73', '1', '1', null, '7', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('684', '628', '73', '1', '1', null, '8', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('685', '629', '73', '1', '1', null, '9', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('686', '630', '73', '1', '1', null, '10', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('687', '631', '73', '1', '1', null, '12', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('688', '632', '73', '1', '1', null, '15', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('689', '633', '73', '1', '1', null, '18', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('690', '634', '73', '1', '1', null, '19', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('691', '635', '73', '1', '1', null, '21', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('692', '636', '73', '1', '1', null, '26', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('693', '637', '73', '1', '1', null, '27', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('694', '638', '73', '1', '1', null, '31', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('695', '639', '73', '1', '1', null, '35', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('696', '640', '74', '1', '1', null, '0', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('697', '641', '74', '1', '1', null, '0', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('698', '642', '74', '1', '1', null, '0', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('699', '643', '74', '1', '1', null, '0', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('700', '644', '74', '1', '1', null, '0', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('701', '645', '74', '1', '1', null, '0', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('702', '646', '74', '1', '1', null, '0', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('703', '647', '74', '1', '1', null, '0', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('704', '648', '74', '1', '1', null, '1', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('705', '649', '74', '1', '1', null, '24', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('706', '650', '74', '1', '1', null, '40', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('707', '651', '75', '1', '1', null, '1', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('708', '652', '75', '1', '1', null, '2', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('709', '653', '75', '1', '1', null, '5', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('710', '654', '75', '1', '1', null, '6', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('711', '655', '75', '1', '1', null, '7', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('712', '656', '75', '1', '1', null, '8', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('713', '657', '75', '1', '1', null, '9', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('714', '658', '75', '1', '1', null, '11', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('715', '659', '75', '1', '1', null, '12', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('716', '660', '75', '1', '1', null, '15', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('717', '661', '75', '1', '1', null, '16', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('718', '662', '75', '1', '1', null, '33', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('719', '663', '75', '1', '1', null, '53', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('720', '664', '76', '1', '1', null, '6', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('721', '665', '76', '1', '1', null, '6', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('722', '666', '76', '1', '1', null, '7', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('723', '667', '76', '1', '1', null, '11', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('724', '668', '76', '1', '1', null, '12', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('725', '669', '76', '1', '1', null, '15', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('726', '670', '76', '1', '1', null, '16', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('727', '671', '76', '1', '1', null, '19', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('728', '672', '76', '1', '1', null, '20', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('729', '673', '76', '1', '1', null, '21', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('730', '674', '76', '1', '1', null, '23', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('731', '675', '76', '1', '1', null, '25', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('732', '676', '76', '1', '1', null, '26', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('733', '677', '76', '1', '1', null, '28', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('734', '678', '76', '1', '1', null, '35', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('735', '679', '76', '1', '1', null, '37', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('741', '684', '82', '1', '1', null, '37', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('742', '685', '83', '1', '1', null, '38', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('743', '686', '84', '1', '1', null, '39', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('744', '687', '85', '1', '1', null, '40', null);
INSERT INTO `t_fbc_game_footballer_team` VALUES ('745', '467', '58', '1', '1', null, '1', '前卫');
INSERT INTO `t_fbc_game_footballer_team` VALUES ('746', '610', '66', '1', '1', null, '1', '前锋');

-- ----------------------------
-- Table structure for t_fbc_game_referee
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_game_referee`;
CREATE TABLE `t_fbc_game_referee` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户中心表id',
  `name` varchar(100) DEFAULT NULL COMMENT '裁判姓名',
  `sex` char(2) DEFAULT NULL COMMENT '性别，男，女',
  `phone` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `status` int(11) DEFAULT NULL COMMENT '审核状态 0 未审核 1审核通过 2审核未通过',
  `remark` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注',
  `id_card` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '身份证号码',
  `picture` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '裁判照片',
  `type` varchar(50) DEFAULT NULL COMMENT '裁判类型，1助理裁判，2主裁判，3第四官员',
  `lastoptime` datetime DEFAULT NULL COMMENT '最后操作时间',
  `expire` int(11) DEFAULT NULL COMMENT '删除标识 1未删除 2已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='裁判表';

-- ----------------------------
-- Records of t_fbc_game_referee
-- ----------------------------
INSERT INTO `t_fbc_game_referee` VALUES ('9', '1', '142', '刘小帅', '男', '17681099240', '1', null, '341226199808255123', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607652834_1.jpg', '1,2,3', null, null);
INSERT INTO `t_fbc_game_referee` VALUES ('11', '1', '154', '赵六', '男', '13856951396', '1', null, null, 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/unlogin.jpg', '1,2,3', null, null);

-- ----------------------------
-- Table structure for t_fbc_game_team
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_game_team`;
CREATE TABLE `t_fbc_game_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '队伍名称',
  `cap_id` int(11) DEFAULT NULL COMMENT '领队id',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `picture` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '队伍照片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='队伍表';

-- ----------------------------
-- Records of t_fbc_game_team
-- ----------------------------
INSERT INTO `t_fbc_game_team` VALUES ('57', '雷霆球队', '15', '1', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607654813_97.jpg');
INSERT INTO `t_fbc_game_team` VALUES ('58', '刀锋球队', '15', '1', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607654820_99.jpg');
INSERT INTO `t_fbc_game_team` VALUES ('66', '合肥众鑫', '18', '1', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607912928_u=1871560953,315592401&fm=26&gp=0.jpg');
INSERT INTO `t_fbc_game_team` VALUES ('72', '合肥芙水源FC', '20', '1', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607916201_合肥芙水源FC.jpg');
INSERT INTO `t_fbc_game_team` VALUES ('73', '青岛鲲鹏', '20', '1', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607916133_青岛鲲鹏B.jpg');
INSERT INTO `t_fbc_game_team` VALUES ('74', '合肥四方', '20', '1', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607916163_合肥四方.jpg');
INSERT INTO `t_fbc_game_team` VALUES ('75', '淮北派菲特', '20', '1', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607916174_淮北派菲特.jpg');
INSERT INTO `t_fbc_game_team` VALUES ('76', '合肥师范附二小', '20', '1', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607916185_合肥师范附二小.jpg');
INSERT INTO `t_fbc_game_team` VALUES ('82', '合肥师范B', '20', '1', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607999181_1607916185_合肥师范附二小.jpg');
INSERT INTO `t_fbc_game_team` VALUES ('83', '合肥师范C', '21', '1', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1607999193_1607916185_合肥师范附二小.jpg');
INSERT INTO `t_fbc_game_team` VALUES ('84', '合肥师范5', '22', '1', null);
INSERT INTO `t_fbc_game_team` VALUES ('85', '合肥师范6', '23', '1', null);
INSERT INTO `t_fbc_game_team` VALUES ('86', '剑锋球队', '15', '1', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1608022336_39.png');
INSERT INTO `t_fbc_game_team` VALUES ('88', '飞流队', '25', '1', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/tmp_e645da143d9d22a1d76b93a371a3737e.jpg');
INSERT INTO `t_fbc_game_team` VALUES ('89', '飞流队2', '25', '1', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/tmp_4957b43c2de57189127a275eee5b545f.jpg');
INSERT INTO `t_fbc_game_team` VALUES ('90', '合云队', '26', '1', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/tmp_a3038d9e7ef305dd45256734f5f09fa0.jpg');
INSERT INTO `t_fbc_game_team` VALUES ('91', '飞云队', '26', '1', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1612422825_球队.jpg');
INSERT INTO `t_fbc_game_team` VALUES ('92', '雅宠科技', '27', '1', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/tmp_608173694c49e0f718d1a32d14919464633bddb8ca28798d.jpg');
INSERT INTO `t_fbc_game_team` VALUES ('93', 'ONE   PUNCH', '28', '1', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/tmp_9df12e88a9761872fddd3be8ae788e2e.jpg');

-- ----------------------------
-- Table structure for t_fbc_game_vote
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_game_vote`;
CREATE TABLE `t_fbc_game_vote` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `activity_id` int(11) DEFAULT NULL COMMENT '活动id',
  `type` int(11) DEFAULT '1' COMMENT '投票类型，1球队，2球员',
  `rule` int(11) DEFAULT '1' COMMENT '投票方式，1每人/天参与次数，2每人可以参与次数',
  `num` int(11) DEFAULT '1' COMMENT '投票次数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='赛事活动投票表';

-- ----------------------------
-- Records of t_fbc_game_vote
-- ----------------------------
INSERT INTO `t_fbc_game_vote` VALUES ('11', '13', '2', '1', '3');
INSERT INTO `t_fbc_game_vote` VALUES ('13', '15', '1', '1', '3');
INSERT INTO `t_fbc_game_vote` VALUES ('14', '16', '2', '1', '5');

-- ----------------------------
-- Table structure for t_fbc_game_vote_detail
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_game_vote_detail`;
CREATE TABLE `t_fbc_game_vote_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `vote_id` int(11) DEFAULT NULL COMMENT '投票id',
  `user_id` int(11) DEFAULT NULL COMMENT '球员 / 球队id',
  `team_id` int(11) DEFAULT NULL COMMENT '球队id（确定球员的球队关系）',
  `number` int(11) DEFAULT '0' COMMENT '得票数',
  `vote_time` datetime DEFAULT NULL COMMENT '最后投票时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1314 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='投票详情表';

-- ----------------------------
-- Records of t_fbc_game_vote_detail
-- ----------------------------
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1231', '11', '1029', '164', '5', '2020-12-29 11:00:25');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1232', '11', '1030', '164', '3', '2020-12-15 11:09:12');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1233', '11', '1031', '164', '1', '2020-12-15 11:09:14');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1234', '11', '1032', '164', '1', '2020-12-15 11:09:13');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1235', '11', '1033', '164', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1236', '11', '1034', '164', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1237', '11', '1035', '164', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1238', '11', '1036', '164', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1239', '11', '1037', '164', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1240', '11', '1038', '164', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1241', '11', '1039', '164', '2', '2020-12-21 09:55:22');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1242', '11', '1040', '164', '1', '2020-12-21 09:55:19');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1243', '11', '1041', '164', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1244', '11', '1042', '164', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1245', '11', '1043', '165', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1246', '11', '1044', '165', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1247', '11', '1045', '165', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1248', '11', '1046', '165', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1249', '11', '1047', '165', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1250', '11', '1048', '165', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1251', '11', '1049', '165', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1252', '11', '1050', '165', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1253', '11', '1051', '165', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1254', '11', '1052', '165', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1255', '11', '1053', '165', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1256', '11', '1054', '165', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1257', '11', '1055', '165', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1258', '11', '1056', '165', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1259', '11', '1057', '165', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1260', '11', '1058', '165', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1261', '11', '1059', '166', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1262', '11', '1060', '166', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1263', '11', '1061', '166', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1264', '11', '1062', '166', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1265', '11', '1063', '166', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1266', '11', '1064', '166', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1267', '11', '1065', '166', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1268', '11', '1066', '166', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1269', '11', '1067', '166', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1270', '11', '1068', '166', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1271', '11', '1069', '166', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1272', '11', '1070', '167', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1273', '11', '1071', '167', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1274', '11', '1072', '167', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1275', '11', '1073', '167', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1276', '11', '1074', '167', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1277', '11', '1075', '167', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1278', '11', '1076', '167', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1279', '11', '1077', '167', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1280', '11', '1078', '167', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1281', '11', '1079', '167', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1282', '11', '1080', '167', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1283', '11', '1081', '167', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1284', '11', '1082', '167', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1285', '11', '1083', '168', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1286', '11', '1084', '168', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1287', '11', '1085', '168', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1288', '11', '1086', '168', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1289', '11', '1087', '168', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1290', '11', '1088', '168', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1291', '11', '1089', '168', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1292', '11', '1090', '168', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1293', '11', '1091', '168', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1294', '11', '1092', '168', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1295', '11', '1093', '168', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1296', '11', '1094', '168', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1297', '11', '1095', '168', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1298', '11', '1096', '168', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1299', '11', '1097', '168', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1300', '11', '1098', '168', '1', '2020-12-15 10:56:37');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1301', '11', '1099', '169', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1302', '11', '1100', '170', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1303', '11', '1101', '171', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1304', '11', '1102', '172', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1305', '11', '1105', '174', '0', '2020-12-15 10:56:05');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1306', '13', '177', '177', '0', '2021-01-28 09:15:03');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1307', '13', '180', '180', '0', '2021-01-28 09:15:03');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1308', '13', '185', '185', '0', '2021-01-28 09:15:03');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1311', '14', '1123', '186', '0', '2021-02-04 10:21:43');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1312', '14', '1124', '187', '0', '2021-02-04 10:21:43');
INSERT INTO `t_fbc_game_vote_detail` VALUES ('1313', '14', '1125', '187', '0', '2021-02-04 10:21:43');

-- ----------------------------
-- Table structure for t_fbc_game_vote_record
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_game_vote_record`;
CREATE TABLE `t_fbc_game_vote_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `wx_id` int(11) DEFAULT NULL COMMENT '微信用户id',
  `vote_id` int(11) DEFAULT NULL COMMENT '投票表id',
  `vote_time` datetime DEFAULT NULL COMMENT '投票时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=350 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户投票记录表';

-- ----------------------------
-- Records of t_fbc_game_vote_record
-- ----------------------------
INSERT INTO `t_fbc_game_vote_record` VALUES ('336', '1110', '11', '2020-12-15 10:56:37');
INSERT INTO `t_fbc_game_vote_record` VALUES ('337', '1110', '11', '2020-12-15 11:07:36');
INSERT INTO `t_fbc_game_vote_record` VALUES ('338', '1110', '11', '2020-12-15 11:07:37');
INSERT INTO `t_fbc_game_vote_record` VALUES ('339', '1108', '11', '2020-12-15 11:09:12');
INSERT INTO `t_fbc_game_vote_record` VALUES ('340', '1108', '11', '2020-12-15 11:09:13');
INSERT INTO `t_fbc_game_vote_record` VALUES ('341', '1108', '11', '2020-12-15 11:09:14');
INSERT INTO `t_fbc_game_vote_record` VALUES ('342', '1144', '11', '2020-12-16 08:45:48');
INSERT INTO `t_fbc_game_vote_record` VALUES ('343', '1108', '11', '2020-12-17 09:43:19');
INSERT INTO `t_fbc_game_vote_record` VALUES ('344', '1108', '11', '2020-12-17 09:43:21');
INSERT INTO `t_fbc_game_vote_record` VALUES ('345', '1108', '11', '2020-12-17 09:43:21');
INSERT INTO `t_fbc_game_vote_record` VALUES ('346', '1110', '11', '2020-12-21 09:55:19');
INSERT INTO `t_fbc_game_vote_record` VALUES ('347', '1110', '11', '2020-12-21 09:55:21');
INSERT INTO `t_fbc_game_vote_record` VALUES ('348', '1110', '11', '2020-12-21 09:55:22');
INSERT INTO `t_fbc_game_vote_record` VALUES ('349', '1155', '11', '2020-12-29 11:00:25');

-- ----------------------------
-- Table structure for t_fbc_ip
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_ip`;
CREATE TABLE `t_fbc_ip` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `ip` varchar(255) DEFAULT NULL COMMENT 'ip',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_fbc_ip
-- ----------------------------
INSERT INTO `t_fbc_ip` VALUES ('1', '127.0.0.1', 'Fri Sep 18 10:07:52 CST 2020');
INSERT INTO `t_fbc_ip` VALUES ('2', '127.0.0.1', 'Fri Sep 18 10:09:25 CST 2020');
INSERT INTO `t_fbc_ip` VALUES ('3', '127.0.0.1', 'Fri Sep 18 10:10:24 CST 2020');
INSERT INTO `t_fbc_ip` VALUES ('4', '127.0.0.1', 'Fri Sep 18 10:10:53 CST 2020');
INSERT INTO `t_fbc_ip` VALUES ('5', '127.0.0.1', 'Fri Sep 18 10:11:55 CST 2020');
INSERT INTO `t_fbc_ip` VALUES ('6', '117.64.248.31', 'Fri Sep 18 10:18:50 CST 2020');
INSERT INTO `t_fbc_ip` VALUES ('7', '39.144.34.106', 'Fri Sep 18 10:21:56 CST 2020');
INSERT INTO `t_fbc_ip` VALUES ('8', '117.64.248.31', 'Fri Sep 18 16:45:32 CST 2020');
INSERT INTO `t_fbc_ip` VALUES ('9', '117.64.248.31', 'Fri Sep 18 16:47:21 CST 2020');
INSERT INTO `t_fbc_ip` VALUES ('10', '117.64.248.31', 'Fri Sep 18 16:47:28 CST 2020');
INSERT INTO `t_fbc_ip` VALUES ('11', '117.64.249.141', 'Mon Sep 21 11:06:03 CST 2020');
INSERT INTO `t_fbc_ip` VALUES ('12', '117.64.249.141', 'Mon Sep 21 11:06:41 CST 2020');
INSERT INTO `t_fbc_ip` VALUES ('13', '117.64.249.141', 'Mon Sep 21 14:31:21 CST 2020');
INSERT INTO `t_fbc_ip` VALUES ('14', '117.64.249.141', 'Mon Sep 21 14:37:50 CST 2020');

-- ----------------------------
-- Table structure for t_fbc_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_menu`;
CREATE TABLE `t_fbc_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '菜单名称',
  `path` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '路径',
  `component` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '组件',
  `redirect` varchar(200) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '重定向',
  `always_show` tinyint(1) DEFAULT '0' COMMENT '仅有一个子节点时是否显示根目录（1:是0：否）',
  `pid` int(11) DEFAULT '0' COMMENT '父ID',
  `meta_roles` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '权限',
  `meta_title` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT 'meta标题',
  `meta_icon` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT 'meta icon',
  `meta_nocache` tinyint(1) DEFAULT '0' COMMENT '是否缓存（1:是 0:否）',
  `meta_affix` tinyint(1) DEFAULT '0' COMMENT '是否加固（1:是0：否）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6607 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='路由表';

-- ----------------------------
-- Records of t_fbc_menu
-- ----------------------------
INSERT INTO `t_fbc_menu` VALUES ('1100', 'Dictionary', '/root', 'Layout', '/root/class', '1', '0', null, '数据字典', 'table', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('1101', 'Class', 'class', '/class/index', '', '0', '1100', null, '梯队管理', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('1102', 'School', 'school', '/school/index', '', '0', '1100', null, '学校管理', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('1103', 'Grade', 'grade', '/grade/index', '', '0', '1100', null, '年级管理', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('2200', 'Peoples', '/people', 'Layout', '/people/student', '0', '0', null, '人员管理', 'peoples', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('2201', 'Student', 'student', '/peoples/student', '', '0', '2200', null, '球员管理', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('2202', 'Parent', 'parent', '/peoples/parent', '', '0', '2200', null, '家长管理', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('2203', 'Coach', 'coach', '/peoples/coach', '', '0', '2200', null, '教练员管理', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('3300', 'PayManagement', '/pay', 'Layout', '/pay/index', '0', '0', null, '缴费管理', 'money', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('3301', 'Pay', 'index', '/pay/index', '', '0', '3300', null, '缴费管理', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('3302', 'Statistics', 'statistics', '/pay/statistics', '', '0', '3300', null, '缴费统计报表', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('4400', 'Attendance', '/attend', 'Layout', '/attend/coach', '0', '0', null, '考勤管理', 'attend', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('4401', 'CoachAtt', 'coach', '/attend/coach', '', '0', '4400', null, '教练员考勤', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('4402', 'StudentAtt', 'student', '/attend/student', '', '0', '4400', null, '球员考勤', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('5500', 'GameSetting', '/game', 'Layout', '/game/index', '0', '0', null, '赛事管理', 'game', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('5501', 'Game', 'index', '/game/index', '', '0', '5500', null, '赛事管理', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('5502', 'Activity', 'activity', '/game/activity', '', '0', '5500', null, '赛事活动', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('5503', 'Referee', 'referee', '/game/referee', '', '0', '5500', null, '裁判管理', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('5504', 'Captain', 'captain', '/game/captain', '', '0', '5500', null, '领队管理', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('5505', 'Player', 'player', '/game/player', '', '0', '5500', null, '球员管理', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('5506', 'Team', 'team', '/game/team', '', '0', '5500', null, '球队管理', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('6600', 'SystemManage', '/permission', 'Layout', '/permission/user', '0', '0', null, '系统管理', 'setting', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('6601', 'ClubSetting', 'club', '/system/club', '', '0', '6600', null, '俱乐部管理', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('6602', 'UserSetting', 'user', '/system/user', '', '0', '6600', null, '用户管理', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('6603', 'WxUser', 'wx', '/system/wxmanage', '', '0', '6600', null, '微信管理', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('6604', 'Permission', 'role', '/system/role', '', '0', '6600', null, '角色管理', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('6605', 'ClubUser', 'clubuser', '/system/clubuser', '', '0', '6600', null, '已绑定用户管理', '', '0', '0');
INSERT INTO `t_fbc_menu` VALUES ('6606', 'Advertising', 'advertising', '/system/advertising', '', '0', '6600', null, '首页广告位管理', '', '0', '0');

-- ----------------------------
-- Table structure for t_fbc_menu_role
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_menu_role`;
CREATE TABLE `t_fbc_menu_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `menu_id` int(11) DEFAULT NULL COMMENT '用户id',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_fbc_menu_role
-- ----------------------------
INSERT INTO `t_fbc_menu_role` VALUES ('27', '0', '1100', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('28', '0', '1101', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('29', '0', '1102', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('30', '0', '1103', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('31', '0', '2200', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('32', '0', '2201', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('33', '0', '2202', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('34', '0', '2203', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('35', '0', '3300', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('36', '0', '3301', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('37', '0', '3302', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('38', '0', '4400', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('39', '0', '4401', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('40', '0', '4402', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('41', '0', '5500', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('42', '0', '5501', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('43', '0', '5502', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('44', '0', '5503', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('45', '0', '5504', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('46', '0', '5505', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('47', '0', '5506', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('48', '0', '6600', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('49', '0', '6601', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('50', '0', '6602', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('51', '0', '6603', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('52', '0', '6604', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('53', '0', '6605', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('54', '0', '6606', '4');
INSERT INTO `t_fbc_menu_role` VALUES ('58', '1', '1100', '27');
INSERT INTO `t_fbc_menu_role` VALUES ('118', '4', '1100', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('119', '4', '1101', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('120', '4', '1102', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('121', '4', '1103', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('122', '4', '2200', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('123', '4', '2201', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('124', '4', '2202', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('125', '4', '2203', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('126', '4', '3300', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('127', '4', '3301', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('128', '4', '3302', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('129', '4', '4400', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('130', '4', '4401', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('131', '4', '4402', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('132', '4', '5500', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('133', '4', '5501', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('134', '4', '5502', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('135', '4', '5503', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('136', '4', '5504', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('137', '4', '5505', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('138', '4', '5506', '33');
INSERT INTO `t_fbc_menu_role` VALUES ('139', '1', '2200', '34');
INSERT INTO `t_fbc_menu_role` VALUES ('140', '1', '2201', '34');
INSERT INTO `t_fbc_menu_role` VALUES ('141', '1', '2203', '34');
INSERT INTO `t_fbc_menu_role` VALUES ('142', '1', '4400', '34');
INSERT INTO `t_fbc_menu_role` VALUES ('143', '1', '4401', '34');
INSERT INTO `t_fbc_menu_role` VALUES ('144', '1', '4402', '34');
INSERT INTO `t_fbc_menu_role` VALUES ('145', '1', '1100', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('146', '1', '1101', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('147', '1', '2200', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('148', '1', '2201', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('149', '1', '2202', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('150', '1', '2203', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('151', '1', '3300', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('152', '1', '3302', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('153', '1', '4400', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('154', '1', '4401', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('155', '1', '4402', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('156', '1', '5500', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('157', '1', '5501', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('158', '1', '5502', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('159', '1', '5503', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('160', '1', '5504', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('161', '1', '5505', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('162', '1', '5506', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('163', '1', '6600', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('164', '1', '6602', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('165', '1', '6603', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('166', '1', '6604', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('167', '1', '6605', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('168', '1', '6606', '1');
INSERT INTO `t_fbc_menu_role` VALUES ('169', '1', '3300', '35');
INSERT INTO `t_fbc_menu_role` VALUES ('170', '1', '3301', '35');
INSERT INTO `t_fbc_menu_role` VALUES ('171', '1', '3302', '35');
INSERT INTO `t_fbc_menu_role` VALUES ('172', '1', '4400', '35');
INSERT INTO `t_fbc_menu_role` VALUES ('173', '1', '4401', '35');
INSERT INTO `t_fbc_menu_role` VALUES ('174', '1', '4402', '35');

-- ----------------------------
-- Table structure for t_fbc_parents
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_parents`;
CREATE TABLE `t_fbc_parents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `user_id` int(11) DEFAULT NULL COMMENT 'cs_user表id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=539 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_fbc_parents
-- ----------------------------
INSERT INTO `t_fbc_parents` VALUES ('512', '刘小帅', '17681099240', '1', '142');
INSERT INTO `t_fbc_parents` VALUES ('515', '王环', '15375490360', '1', '145');
INSERT INTO `t_fbc_parents` VALUES ('516', '朱靖靖', '15805608840', '1', '146');
INSERT INTO `t_fbc_parents` VALUES ('517', '单世峰', '18656131177', '1', '147');
INSERT INTO `t_fbc_parents` VALUES ('521', '唐寅', '13856951396', '1', '154');
INSERT INTO `t_fbc_parents` VALUES ('522', '魏正美', '13355605562', '1', '0');
INSERT INTO `t_fbc_parents` VALUES ('523', '刘奕含', '15305518609', '1', '0');
INSERT INTO `t_fbc_parents` VALUES ('524', '徐辉', '17775396931', '1', '0');
INSERT INTO `t_fbc_parents` VALUES ('525', '李登科', '18605515180', '1', '0');
INSERT INTO `t_fbc_parents` VALUES ('526', '谷凤', '15056051371', '1', '0');
INSERT INTO `t_fbc_parents` VALUES ('527', '赵治华', '13355514688', '1', '0');
INSERT INTO `t_fbc_parents` VALUES ('528', '冯雪松', '15375395017', '1', '0');
INSERT INTO `t_fbc_parents` VALUES ('529', '刘锐', '13856009960', '1', '0');
INSERT INTO `t_fbc_parents` VALUES ('530', '汤大定', '13966669145', '1', '0');
INSERT INTO `t_fbc_parents` VALUES ('531', '宇健', '18756091236', '1', '0');
INSERT INTO `t_fbc_parents` VALUES ('532', '高阳', '13721059418', '1', '0');
INSERT INTO `t_fbc_parents` VALUES ('533', '王俊', '13625517900', '1', '0');
INSERT INTO `t_fbc_parents` VALUES ('534', '常中伟', '13695658531', '1', '0');
INSERT INTO `t_fbc_parents` VALUES ('535', '张四平', '13866799249', '1', '0');
INSERT INTO `t_fbc_parents` VALUES ('536', '刘娟娟', '18755139576', '1', '0');
INSERT INTO `t_fbc_parents` VALUES ('537', '孔凡岩', '15375337688', '1', '0');
INSERT INTO `t_fbc_parents` VALUES ('538', '宋风', '13856949571', '1', '0');

-- ----------------------------
-- Table structure for t_fbc_pay
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_pay`;
CREATE TABLE `t_fbc_pay` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `pay_name` varchar(200) DEFAULT NULL COMMENT '缴费项目名称',
  `amount` decimal(10,2) DEFAULT NULL COMMENT '缴费金额',
  `content` varchar(255) DEFAULT NULL COMMENT '缴费内容介绍',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `class_id` varchar(255) DEFAULT NULL COMMENT '梯队id',
  `status` int(11) DEFAULT '1' COMMENT '缴费项目的状态 1：进行中，2 ：已停止',
  `end_time` date DEFAULT NULL COMMENT '截止日期',
  `start` int(11) DEFAULT NULL COMMENT ' 是否立即启动（1：是，2否，默认1）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='在线缴费';

-- ----------------------------
-- Records of t_fbc_pay
-- ----------------------------
INSERT INTO `t_fbc_pay` VALUES ('4', '青训第一季度费用', '0.01', '请尽快缴费', '1', '19,23', '2', '2020-12-17', '1');
INSERT INTO `t_fbc_pay` VALUES ('5', '测试缴费', '20.00', '', '1', '23', '2', '2020-12-17', '1');
INSERT INTO `t_fbc_pay` VALUES ('6', '青训第二季度缴费', '0.01', '请立即缴费', '1', '51', '2', '2021-01-18', '1');
INSERT INTO `t_fbc_pay` VALUES ('7', '青训缴费', '120.00', '请立即缴费', '1', '19,23', '2', '2021-02-02', '1');

-- ----------------------------
-- Table structure for t_fbc_pay_config
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_pay_config`;
CREATE TABLE `t_fbc_pay_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部ID',
  `appid` varchar(200) DEFAULT NULL COMMENT '支付接口参数 - 字段自行扩展',
  `mchid` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '商户号',
  `sub_appid` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '小程序id',
  `sub_mchid` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '子商户id',
  `apikey` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '服务商户号密钥',
  `secret` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '小程序号密钥---与sub_appid成对',
  `type` varchar(50) DEFAULT NULL COMMENT '接口类型（WX,WXAPP,ALIPAY...）',
  `notify_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '支付结果通知地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='支付接口信息';

-- ----------------------------
-- Records of t_fbc_pay_config
-- ----------------------------
INSERT INTO `t_fbc_pay_config` VALUES ('1', '1', 'wxdcc4259f5eb3eb8d', '1600832212', 'wxcb53c5d3e83c6b82', '1602811853', 'ahzy3445810344581034458103445810', 'f7c6b7568dec1f5d249652d56e689c9e', '1', 'https://test.ahzysoft.com.cn/wx/notify');

-- ----------------------------
-- Table structure for t_fbc_pay_log
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_pay_log`;
CREATE TABLE `t_fbc_pay_log` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `pay_id` varchar(200) DEFAULT NULL COMMENT '缴费项目名称',
  `amount` decimal(10,2) DEFAULT NULL COMMENT '缴费金额',
  `pay_order_id` varchar(200) DEFAULT NULL COMMENT '业务订单号',
  `api_order_id` varchar(200) DEFAULT NULL COMMENT '支付接口流水号',
  `request` varchar(1000) DEFAULT NULL COMMENT '接口发起数据',
  `request_time` datetime DEFAULT NULL COMMENT '接口发起时间',
  `response` varchar(1000) DEFAULT NULL COMMENT '接口返回数据',
  `response_time` datetime DEFAULT NULL COMMENT '接口返回时间',
  `status` int(255) DEFAULT NULL COMMENT '支付接口状态- 1支付中 2已支付 3支付异常',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='支付日志';

-- ----------------------------
-- Records of t_fbc_pay_log
-- ----------------------------
INSERT INTO `t_fbc_pay_log` VALUES ('43', '4', '0.01', '2020121114303600000', null, 'amount:0.01,body:众焱科技-微信支付,detail:汤子晨-青训第一季度费用缴费-0.01元;,mch_id:1600832212,out_trade_no:2020121114303600000,payIds:4,payRecordIds:33,sub_appid:wxcb53c5d3e83c6b82,sub_mch_id:1602811853,trade_type:JSAPI', '2020-12-11 14:30:37', 'appId:wxcb53c5d3e83c6b82,key:ahzy3445810344581034458103445810,nonceStr:sC2UfnPlrhbx8qCM2ceHoif9nMHlrjim,package:prepay_id=wx111430373767179d7f2584bec2ed100000,paySign:CCE1EC59696D531F4A9F384CEC4639B5662F10E2FF1DF50CDBE3552C58B22E1F,signType:HMAC-SHA256,timeStamp:1607668237', '2020-12-11 14:30:43', '2');
INSERT INTO `t_fbc_pay_log` VALUES ('44', null, '110.00', '2020121415093800000', null, 'amount:110,body:众焱科技-微信支付,detail:比赛项目-冬日足球嘉年华费用-110,mch_id:1600832212,out_trade_no:2020121415093800000,sub_appid:wxcb53c5d3e83c6b82,sub_mch_id:1602811853,trade_type:JSAPI', '2020-12-14 15:09:39', 'appId:wxcb53c5d3e83c6b82,key:ahzy3445810344581034458103445810,nonceStr:DRHkeHElpp5uawBMaEiZLJUquQmEYL7R,package:prepay_id=wx141509393721897eeaa4d20b9d7e2c0000,paySign:7D8603CFB283A1EB1520A07DEC3A20F8AF928DB9B036E37A4E3BCEDFB78570C8,signType:HMAC-SHA256,timeStamp:1607929779', '2020-12-14 15:09:42', '3');
INSERT INTO `t_fbc_pay_log` VALUES ('45', '6', '0.01', '2021011516463600000', null, 'amount:0.01,body:众焱科技-微信支付,detail:张三-青训第二季度缴费缴费-0.01元;,mch_id:1600832212,out_trade_no:2021011516463600000,payIds:6,payRecordIds:39,sub_appid:wxcb53c5d3e83c6b82,sub_mch_id:1602811853,trade_type:JSAPI', '2021-01-15 16:46:37', 'appId:wxcb53c5d3e83c6b82,key:ahzy3445810344581034458103445810,nonceStr:DeXMZdkBPN2e3TkN1ZyCoxJkxgv0V0nP,package:prepay_id=wx1516463689992558c8c77d876b9f8e0000,paySign:0C702DB1246CF226AF7C3D111CF7E6E1E973D393B161B92711D769B87D6D21AA,signType:HMAC-SHA256,timeStamp:1610700396', '2021-01-15 16:46:43', '2');
INSERT INTO `t_fbc_pay_log` VALUES ('46', null, '60.00', '2021011914154100001', null, 'amount:60,body:众焱科技-微信支付,detail:比赛项目-冬日足球嘉年华费用-60,mch_id:1600832212,out_trade_no:2021011914154100001,sub_appid:wxcb53c5d3e83c6b82,sub_mch_id:1602811853,trade_type:JSAPI', '2021-01-19 14:15:42', 'appId:wxcb53c5d3e83c6b82,key:ahzy3445810344581034458103445810,nonceStr:Ui0Xsb1rDD13GDphP3rQsIc446Zw5H70,package:prepay_id=wx19141541882723b112afc9bd169a5d0000,paySign:8B25304CE66646A47647D9912D07E727388C1868A324C31C283A30E469EE3183,signType:HMAC-SHA256,timeStamp:1611036941', '2021-01-19 14:15:50', '3');
INSERT INTO `t_fbc_pay_log` VALUES ('47', null, '60.00', '2021011914185200002', null, 'amount:60,body:众焱科技-微信支付,detail:比赛项目-冬日足球嘉年华费用-60,mch_id:1600832212,out_trade_no:2021011914185200002,sub_appid:wxcb53c5d3e83c6b82,sub_mch_id:1602811853,trade_type:JSAPI', '2021-01-19 14:18:52', 'appId:wxcb53c5d3e83c6b82,key:ahzy3445810344581034458103445810,nonceStr:MbY5Hgk41PsKI6Vi7X01HDQE4gGK505O,package:prepay_id=wx191418523655801205fadd7cecd0af0000,paySign:EE033E3E25BD33BCE81ABAD5C2115C5A587CB5303DEFB38D24630416032A2593,signType:HMAC-SHA256,timeStamp:1611037132', '2021-01-19 14:19:04', '2');

-- ----------------------------
-- Table structure for t_fbc_pay_record
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_pay_record`;
CREATE TABLE `t_fbc_pay_record` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部ID',
  `pay_id` int(11) DEFAULT NULL COMMENT '缴费ID',
  `child_id` int(11) DEFAULT NULL COMMENT '学员ID',
  `status` int(11) DEFAULT NULL COMMENT '状态（1未支付，2已支付 3支付失败）',
  `pay_order_id` varchar(200) DEFAULT NULL COMMENT '支付订单ID',
  `pay_time` datetime DEFAULT NULL COMMENT '缴费时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='在线缴费记录';

-- ----------------------------
-- Records of t_fbc_pay_record
-- ----------------------------
INSERT INTO `t_fbc_pay_record` VALUES ('33', '1', '4', '26', '2', '2020121114303600000', '2020-12-11 14:30:44');
INSERT INTO `t_fbc_pay_record` VALUES ('34', '1', '4', '29', '1', null, null);
INSERT INTO `t_fbc_pay_record` VALUES ('35', '1', '4', '28', '1', null, null);
INSERT INTO `t_fbc_pay_record` VALUES ('36', '1', '4', '25', '1', null, null);
INSERT INTO `t_fbc_pay_record` VALUES ('38', '1', '5', '25', '1', null, null);
INSERT INTO `t_fbc_pay_record` VALUES ('40', '1', '7', '26', '1', null, null);
INSERT INTO `t_fbc_pay_record` VALUES ('41', '1', '7', '28', '1', null, null);
INSERT INTO `t_fbc_pay_record` VALUES ('42', '1', '7', '29', '1', null, null);
INSERT INTO `t_fbc_pay_record` VALUES ('43', '1', '7', '39', '1', null, null);

-- ----------------------------
-- Table structure for t_fbc_role
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_role`;
CREATE TABLE `t_fbc_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `type` int(2) DEFAULT '1' COMMENT '角色类型，1普通角色，2俱乐部系统角色，3后台系统角色',
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '角色说明',
  `route_id` varchar(255) DEFAULT NULL COMMENT '路由id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户角色表';

-- ----------------------------
-- Records of t_fbc_role
-- ----------------------------
INSERT INTO `t_fbc_role` VALUES ('1', '1', '2', '管理员', '后台权限', '1101,2200,2201,2202,2203,3302,4400,4401,4402,5500,5501,5502,5503,5504,5505,5506,6600,6602,6603,6604,6605,6606');
INSERT INTO `t_fbc_role` VALUES ('2', '1', '2', '财务', '财务', '3300,3301,3302,4400,4401,4402');
INSERT INTO `t_fbc_role` VALUES ('3', '1', '2', '教练员', '教练员', '2201,4400,4401,4402');
INSERT INTO `t_fbc_role` VALUES ('4', '0', '3', '系统管理员', '最高权限', '1100,1101,1102,1103,2200,2201,2202,2203,3300,3301,3302,4400,4401,4402,5500,5501,5502,5503,5504,5505,5506,6600,6601,6602,6603,6604,6605,6606');
INSERT INTO `t_fbc_role` VALUES ('33', '4', '2', '管理员', '俱乐部管理员', '1100,1101,1102,1103,2200,2201,2202,2203,3300,3301,3302,4400,4401,4402,5500,5501,5502,5503,5504,5505,5506');
INSERT INTO `t_fbc_role` VALUES ('34', '1', null, '教练员2', null, '2201,2203,4400,4401,4402');
INSERT INTO `t_fbc_role` VALUES ('35', '1', null, '财务2', null, '3300,3301,3302,4400,4401,4402');

-- ----------------------------
-- Table structure for t_fbc_school
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_school`;
CREATE TABLE `t_fbc_school` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `name` varchar(50) DEFAULT NULL COMMENT '学校名称',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='班级';

-- ----------------------------
-- Records of t_fbc_school
-- ----------------------------
INSERT INTO `t_fbc_school` VALUES ('1', '1', '一小', '2020-08-13 19:04:42', '2020-08-26 16:37:10');
INSERT INTO `t_fbc_school` VALUES ('2', '1', '62中', '2020-08-13 20:04:42', '2020-08-26 16:34:09');
INSERT INTO `t_fbc_school` VALUES ('3', '1', '梦园', '2020-08-13 20:04:42', '2020-08-13 20:50:38');
INSERT INTO `t_fbc_school` VALUES ('4', '1', '翡翠', '2020-08-13 20:04:42', '2020-08-13 20:50:48');
INSERT INTO `t_fbc_school` VALUES ('5', '1', '桂花园', '2020-08-13 20:04:42', '2020-08-26 16:35:25');
INSERT INTO `t_fbc_school` VALUES ('6', '1', '兴园学校', '2020-08-26 09:45:03', '2020-08-26 09:45:03');
INSERT INTO `t_fbc_school` VALUES ('9', '1', '梦小文曲路校区', '2020-08-26 09:52:35', '2020-08-26 09:52:35');
INSERT INTO `t_fbc_school` VALUES ('10', '1', '梦园学校', '2020-08-26 10:02:25', '2020-08-26 10:02:25');
INSERT INTO `t_fbc_school` VALUES ('11', '1', '金湖小学', '2020-08-26 10:02:25', '2020-08-26 10:02:25');
INSERT INTO `t_fbc_school` VALUES ('12', '1', '海顿学校', '2020-08-26 10:02:25', '2020-08-26 10:02:25');
INSERT INTO `t_fbc_school` VALUES ('13', '1', '合肥市六十二中学', '2020-08-26 10:05:13', '2020-08-26 10:05:13');
INSERT INTO `t_fbc_school` VALUES ('14', '1', '合肥市第六十二中', '2020-08-26 10:05:13', '2020-08-26 10:05:13');
INSERT INTO `t_fbc_school` VALUES ('15', '1', '桂花园学校', '2020-08-26 10:26:15', '2020-08-26 10:26:15');
INSERT INTO `t_fbc_school` VALUES ('16', '1', '文曲路小学', '2020-08-26 10:51:01', '2020-08-26 10:51:01');
INSERT INTO `t_fbc_school` VALUES ('17', '1', '梦小文曲路', '2020-08-26 10:54:03', '2020-08-26 10:54:03');
INSERT INTO `t_fbc_school` VALUES ('18', '1', '梦园中学', '2020-08-26 11:15:35', '2020-08-26 11:15:35');

-- ----------------------------
-- Table structure for t_fbc_score
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_score`;
CREATE TABLE `t_fbc_score` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_type` varchar(10) DEFAULT NULL COMMENT '足球场上的事件类型：进球，红牌，黄牌',
  `against_id` int(10) unsigned DEFAULT NULL COMMENT '对阵信息关联',
  `team_id` int(10) unsigned DEFAULT NULL COMMENT '球员队伍ID',
  `player_id` int(10) unsigned DEFAULT NULL COMMENT '队员ID值',
  `time` datetime DEFAULT NULL COMMENT '事件发生事件',
  `team_type` varchar(20) DEFAULT NULL COMMENT '左右球队区分：left、right',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_fbc_score
-- ----------------------------
INSERT INTO `t_fbc_score` VALUES ('78', '进球', '179', '136', '725', '2020-12-11 10:52:00', 'left');
INSERT INTO `t_fbc_score` VALUES ('79', '进球', '179', '137', '726', '2020-12-11 10:52:00', 'right');
INSERT INTO `t_fbc_score` VALUES ('80', '黄牌', '179', '137', '726', '2020-12-11 10:52:00', 'right');
INSERT INTO `t_fbc_score` VALUES ('82', '红牌', '179', '136', '725', '2020-12-11 10:52:00', 'left');
INSERT INTO `t_fbc_score` VALUES ('83', '进球', '179', '136', '725', '2020-12-11 10:53:00', 'left');
INSERT INTO `t_fbc_score` VALUES ('84', '进球', '262', '136', '725', '2020-12-15 15:15:00', 'left');
INSERT INTO `t_fbc_score` VALUES ('85', '红牌', '262', '136', '725', '2020-12-15 16:47:00', 'left');
INSERT INTO `t_fbc_score` VALUES ('86', '进球', '264', '177', '1109', '2020-12-16 11:17:00', 'left');
INSERT INTO `t_fbc_score` VALUES ('89', '黄牌', '264', '177', '1109', '2020-12-16 11:17:00', 'left');
INSERT INTO `t_fbc_score` VALUES ('91', '红牌', '265', '180', '1114', '2021-01-21 09:48:00', 'left');
INSERT INTO `t_fbc_score` VALUES ('92', '进球', '265', '185', '1122', '2021-01-21 09:48:00', 'right');

-- ----------------------------
-- Table structure for t_fbc_sign_record
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_sign_record`;
CREATE TABLE `t_fbc_sign_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `user_id` int(11) DEFAULT NULL COMMENT '签到用户id(学员id,或教练员id)',
  `name` varchar(11) CHARACTER SET utf8 DEFAULT NULL COMMENT '姓名',
  `type` int(11) DEFAULT '0' COMMENT '0 学员，1教练',
  `sign_time` datetime DEFAULT NULL COMMENT '签到时间',
  `location` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '打卡位置',
  `longitude` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '经度',
  `latitude` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='教练员和学生打卡记录';

-- ----------------------------
-- Records of t_fbc_sign_record
-- ----------------------------
INSERT INTO `t_fbc_sign_record` VALUES ('125', '1', '51', '刘小帅', '1', '2020-12-11 13:56:51', '安徽省合肥市蜀山区天元路', '117.20477403428819', '31.852412651909724');
INSERT INTO `t_fbc_sign_record` VALUES ('126', '1', '25', '刘帅', '0', '2020-12-15 14:48:57', '安徽省合肥市蜀山区天元路', '117.20491889105902', '31.850929633246526');
INSERT INTO `t_fbc_sign_record` VALUES ('127', '1', '52', '宋子昕', '1', '2020-12-15 14:49:14', '安徽省合肥市蜀山区天元路9号', '117.20488739013672', '31.85232162475586');
INSERT INTO `t_fbc_sign_record` VALUES ('128', '1', '26', '汤子晨', '0', '2020-12-15 14:49:16', '安徽省合肥市蜀山区天元路9号', '117.20487213134766', '31.852415084838867');
INSERT INTO `t_fbc_sign_record` VALUES ('129', '1', '28', '高清游', '0', '2020-12-15 14:49:17', '安徽省合肥市蜀山区天元路9号', '117.20487213134766', '31.8524227142334');
INSERT INTO `t_fbc_sign_record` VALUES ('130', '1', '29', '王思佳', '0', '2020-12-15 14:49:18', '安徽省合肥市蜀山区天元路9号', '117.20487976074219', '31.852388381958008');
INSERT INTO `t_fbc_sign_record` VALUES ('131', '1', '51', '刘小帅', '1', '2020-12-15 14:52:07', '安徽省合肥市蜀山区天元路', '117.20491889105902', '31.850929633246526');
INSERT INTO `t_fbc_sign_record` VALUES ('134', '1', '51', '刘小帅', '1', '2020-12-17 14:43:10', '安徽省合肥市蜀山区天元路', '117.20491889105902', '31.850929633246526');
INSERT INTO `t_fbc_sign_record` VALUES ('135', '1', '52', '宋子昕', '1', '2021-01-07 11:11:45', '安徽省合肥市蜀山区天元路9号', '117.20494842529297', '31.852394104003906');
INSERT INTO `t_fbc_sign_record` VALUES ('136', '1', '26', '汤子晨', '0', '2021-01-07 11:11:47', '安徽省合肥市蜀山区天元路9号', '117.20500946044922', '31.852285385131836');
INSERT INTO `t_fbc_sign_record` VALUES ('137', '1', '28', '高清游', '0', '2021-01-07 11:11:48', '安徽省合肥市蜀山区天元路9号', '117.20501708984375', '31.85218620300293');
INSERT INTO `t_fbc_sign_record` VALUES ('138', '1', '29', '王思佳', '0', '2021-01-07 11:11:49', '安徽省合肥市蜀山区天元路9号', '117.20500946044922', '31.852081298828125');
INSERT INTO `t_fbc_sign_record` VALUES ('139', '1', '53', '唐七', '1', '2021-01-21 16:46:03', '安徽省合肥市蜀山区天元路9号', '117.20487976074219', '31.85236930847168');
INSERT INTO `t_fbc_sign_record` VALUES ('140', '1', '26', '汤子晨', '0', '2021-01-21 16:46:05', '安徽省合肥市蜀山区天元路9号', '117.20489501953125', '31.852298736572266');
INSERT INTO `t_fbc_sign_record` VALUES ('141', '1', '28', '高清游', '0', '2021-01-21 16:46:07', '安徽省合肥市蜀山区天元路9号', '117.20488739013672', '31.852373123168945');
INSERT INTO `t_fbc_sign_record` VALUES ('142', '1', '29', '王思佳', '0', '2021-01-21 16:46:08', '安徽省合肥市蜀山区天元路9号', '117.20490264892578', '31.85234260559082');
INSERT INTO `t_fbc_sign_record` VALUES ('157', '1', '53', '唐七', '1', '2021-01-25 15:41:51', '安徽省合肥市蜀山区天元路9号', '117.20492553710938', '31.85224151611328');
INSERT INTO `t_fbc_sign_record` VALUES ('158', '1', '26', '汤子晨', '0', '2021-01-25 15:41:53', '安徽省合肥市蜀山区天元路9号', '117.2049331665039', '31.852264404296875');
INSERT INTO `t_fbc_sign_record` VALUES ('159', '1', '28', '高清游', '0', '2021-01-25 15:41:54', '安徽省合肥市蜀山区天元路9号', '117.20492553710938', '31.852270126342773');
INSERT INTO `t_fbc_sign_record` VALUES ('160', '1', '29', '王思佳', '0', '2021-01-25 15:41:55', '安徽省合肥市蜀山区天元路9号', '117.20491790771484', '31.852272033691406');
INSERT INTO `t_fbc_sign_record` VALUES ('161', '1', '54', '唐教练', '1', '2021-02-03 13:53:52', '安徽省合肥市蜀山区天元路9号', '117.20488739013672', '31.852256774902344');
INSERT INTO `t_fbc_sign_record` VALUES ('162', '1', '26', '汤子晨', '0', '2021-02-03 13:53:53', '安徽省合肥市蜀山区天元路9号', '117.20486450195312', '31.852420806884766');
INSERT INTO `t_fbc_sign_record` VALUES ('163', '1', '26', '汤子晨', '0', '2021-02-03 13:53:53', '安徽省合肥市蜀山区天元路9号', '117.20484924316406', '31.852418899536133');
INSERT INTO `t_fbc_sign_record` VALUES ('164', '1', '28', '高清游', '0', '2021-02-03 13:53:55', '安徽省合肥市蜀山区天元路9号', '117.20484924316406', '31.852405548095703');
INSERT INTO `t_fbc_sign_record` VALUES ('165', '1', '28', '高清游', '0', '2021-02-03 13:53:57', '安徽省合肥市蜀山区天元路9号', '117.204833984375', '31.85245132446289');
INSERT INTO `t_fbc_sign_record` VALUES ('166', '1', '28', '高清游', '0', '2021-02-03 13:54:55', '安徽省合肥市蜀山区天元路9号', '117.20484161376953', '31.85240936279297');
INSERT INTO `t_fbc_sign_record` VALUES ('167', '1', '51', '刘小帅', '1', '2021-02-03 13:56:53', '安徽省合肥市蜀山区天元路', '117.20501844618056', '31.85093967013889');
INSERT INTO `t_fbc_sign_record` VALUES ('168', '1', '54', '唐教练', '1', '2021-02-07 17:58:31', '安徽省合肥市蜀山区科学大道', '117.20050048828125', '31.819883346557617');
INSERT INTO `t_fbc_sign_record` VALUES ('169', '1', '26', '汤子晨', '0', '2021-02-07 17:58:33', '安徽省合肥市蜀山区科学大道', '117.20043182373047', '31.819915771484375');
INSERT INTO `t_fbc_sign_record` VALUES ('170', '1', '28', '高清游', '0', '2021-02-07 17:58:34', '安徽省合肥市蜀山区科学大道', '117.20054626464844', '31.819835662841797');
INSERT INTO `t_fbc_sign_record` VALUES ('171', '1', '29', '王思佳', '0', '2021-02-07 17:58:35', '安徽省合肥市蜀山区科学大道', '117.20053100585938', '31.819778442382812');
INSERT INTO `t_fbc_sign_record` VALUES ('172', '1', '39', '唐晏', '0', '2021-02-07 17:58:36', '安徽省合肥市蜀山区科学大道', '117.20053100585938', '31.819778442382812');

-- ----------------------------
-- Table structure for t_fbc_sms_code
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_sms_code`;
CREATE TABLE `t_fbc_sms_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID,自增',
  `sms_type` int(11) DEFAULT NULL COMMENT '验证码类型 1芙水源 2名流',
  `code` varchar(20) DEFAULT NULL COMMENT '验证码',
  `timestamp` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '时间戳',
  `phone` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '手机号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='短信验证码表-\r\n用完删除';

-- ----------------------------
-- Records of t_fbc_sms_code
-- ----------------------------

-- ----------------------------
-- Table structure for t_fbc_sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_sys_dict`;
CREATE TABLE `t_fbc_sys_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID,自增',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `pid` int(11) DEFAULT NULL COMMENT '上级ID',
  `type` varchar(50) DEFAULT NULL COMMENT '字典类型',
  `type_str` varchar(100) DEFAULT NULL COMMENT '字典类型名称',
  `key` varchar(100) DEFAULT NULL COMMENT '编码',
  `value` varchar(100) DEFAULT NULL COMMENT '字典值/名称',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `status` int(11) DEFAULT NULL COMMENT '状态，1正常， 2删除',
  `desc` varchar(255) DEFAULT NULL COMMENT '字典描述',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  `author` varchar(50) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='数据字典表(存储格式: key:XXX_XXX value:名称)';

-- ----------------------------
-- Records of t_fbc_sys_dict
-- ----------------------------
INSERT INTO `t_fbc_sys_dict` VALUES ('1', '1', '0', 'SCHOOL', '学校', 'SC', '学校', null, '1', null, '2020-09-21 15:34:43', '2020-09-21 15:34:46', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('2', '1', '1', 'SCHOOL', '学校', 'SC0001', '一小', '21', '1', null, '2020-08-13 19:04:42', '2020-12-15 14:13:12', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('3', '1', '1', 'SCHOOL', '学校', 'SC0002', '62中', '30', '1', null, '2020-08-13 20:04:42', '2020-08-26 16:34:09', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('4', '1', '1', 'SCHOOL', '学校', 'SC0003', '梦园', '29', '1', null, '2020-08-13 20:04:42', '2020-08-13 20:50:38', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('5', '1', '1', 'SCHOOL', '学校', 'SC0004', '翡翠', '28', '1', null, '2020-08-13 20:04:42', '2020-08-13 20:50:48', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('6', '1', '1', 'SCHOOL', '学校', 'SC0005', '桂花园', '27', '1', null, '2020-08-13 20:04:42', '2020-08-26 16:35:25', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('7', '1', '1', 'SCHOOL', '学校', 'SC0006', '兴园学校', '32', '1', null, '2020-08-26 09:45:03', '2020-08-26 09:45:03', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('8', '1', '1', 'SCHOOL', '学校', 'SC0007', '梦小文曲路校区', '31', '1', null, '2020-08-26 09:52:35', '2020-08-26 09:52:35', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('9', '1', '1', 'SCHOOL', '学校', 'SC0008', '梦园学校', '33', '1', null, '2020-08-26 10:02:25', '2020-08-26 10:02:25', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('10', '1', '1', 'SCHOOL', '学校', 'SC0009', '金湖小学', '26', '1', null, '2020-08-26 10:02:25', '2020-08-26 10:02:25', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('11', '1', '1', 'SCHOOL', '学校', 'SC0010', '海顿学校', '22', '1', null, '2020-08-26 10:02:25', '2020-08-26 10:02:25', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('12', '1', '1', 'SCHOOL', '学校', 'SC0011', '合肥市六十二中学', '37', '1', null, '2020-08-26 10:05:13', '2020-08-26 10:05:13', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('13', '1', '1', 'SCHOOL', '学校', 'SC0012', '合肥市第六十二中', '38', '1', null, '2020-08-26 10:05:13', '2020-08-26 10:05:13', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('14', '1', '1', 'SCHOOL', '学校', 'SC0013', '桂花园学校', '39', '1', null, '2020-08-26 10:26:15', '2020-08-26 10:26:15', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('15', '1', '1', 'SCHOOL', '学校', 'SC0014', '文曲路小学', '40', '1', null, '2020-08-26 10:51:01', '2020-08-26 10:51:01', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('16', '1', '1', 'SCHOOL', '学校', 'SC0015', '梦小文曲路', '41', '1', null, '2020-08-26 10:54:03', '2020-08-26 10:54:03', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('17', '1', '1', 'SCHOOL', '学校', 'SC0016', '梦园中学', '36', '1', null, '2020-08-26 11:15:35', '2020-08-26 11:15:35', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('18', '1', '0', 'GRADE', '年级', 'GR', '年级', null, '1', null, '2020-09-22 16:56:48', '2020-09-22 16:56:50', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('19', '1', '18', 'GRADE', '年级', 'GR0001', '幼儿园小班', '16', '1', null, '2020-09-22 16:57:40', '2020-09-22 16:57:42', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('20', '1', '18', 'GRADE', '年级', 'GR0002', '幼儿园中班', '8', '1', null, '2020-09-23 08:50:02', '2020-09-23 08:50:05', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('21', '1', '18', 'GRADE', '年级', 'GR0003', '幼儿园大班', '15', '1', null, '2020-09-23 08:50:02', '2020-09-23 08:50:05', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('22', '1', '18', 'GRADE', '年级', 'GR0004', '一年级', '11', '1', null, '2020-09-23 08:50:02', '2020-09-23 09:45:55', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('23', '1', '18', 'GRADE', '年级', 'GR0005', '二年级', '7', '1', null, '2020-09-23 08:50:02', '2020-09-23 09:45:55', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('24', '1', '18', 'GRADE', '年级', 'GR0006', '三年级', '17', '1', null, '2020-09-23 08:50:02', '2020-09-23 09:45:55', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('25', '1', '18', 'GRADE', '年级', 'GR0007', '四年级', '6', '1', null, '2020-09-23 08:50:02', '2020-09-23 08:50:05', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('26', '1', '18', 'GRADE', '年级', 'GR0008', '五年级', '9', '1', null, '2020-09-23 08:50:02', '2020-09-23 08:50:05', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('27', '1', '18', 'GRADE', '年级', 'GR0009', '六年级', '23', '1', null, '2020-09-23 08:50:02', '2020-09-23 08:50:05', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('28', '1', '18', 'GRADE', '年级', 'GR0010', '七年级', '24', '1', null, '2020-09-23 08:50:02', '2020-09-23 08:50:05', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('29', '1', '18', 'GRADE', '年级', 'GR0011', '八年级', '25', '1', null, '2020-09-23 08:50:02', '2020-09-23 08:50:05', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('30', '1', '18', 'GRADE', '年级', 'GR0012', '九年级', '26', '1', null, '2020-09-23 08:50:02', '2020-09-23 09:44:24', null);
INSERT INTO `t_fbc_sys_dict` VALUES ('74', '1', '1', 'SCHOOL', '学校', 'SC0017', '慧园校区（兴园中学）', '43', '2', null, '2020-12-30 09:37:58', '2020-12-30 09:38:12', 'zy');
INSERT INTO `t_fbc_sys_dict` VALUES ('75', '1', '1', 'SCHOOL', '学校', 'SC0018', '梦园小学', '44', '2', null, '2021-03-01 11:14:24', '2021-03-01 11:18:47', 'zy');
INSERT INTO `t_fbc_sys_dict` VALUES ('87', '1', '1', 'SCHOOL', '学校', 'SC0019', 'aaaaaaaaaa', '45', '2', null, '2021-04-22 16:55:57', '2021-04-22 16:56:45', 'zy');
INSERT INTO `t_fbc_sys_dict` VALUES ('91', '1', '1', 'SCHOOL', '学校', 'SC0020', 'bbbbbbbb', '46', '2', null, '2021-04-22 16:57:48', '2021-04-22 16:58:43', 'zy');

-- ----------------------------
-- Table structure for t_fbc_user
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_user`;
CREATE TABLE `t_fbc_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `real_name` varchar(255) DEFAULT NULL COMMENT '真实姓名',
  `username` varchar(50) DEFAULT NULL COMMENT '登录名称',
  `password` varchar(100) DEFAULT NULL COMMENT '登录密码',
  `gender` int(2) DEFAULT '1' COMMENT '性别：0未知，1男，2女',
  `phone` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱地址',
  `avatar` text COMMENT '头像照片',
  `type` int(2) DEFAULT '0' COMMENT '类型：1管理员，2教练等',
  `introduction` varchar(255) DEFAULT NULL COMMENT '个人介绍',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部id',
  `bindStatus` int(255) DEFAULT '0' COMMENT '0 未绑定 1已绑定',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='人员表';

-- ----------------------------
-- Records of t_fbc_user
-- ----------------------------
INSERT INTO `t_fbc_user` VALUES ('1', '后台管理', 'admin', '$2a$10$qi1h.fJ3dmfBMV2C7R0cmOgIWPwfk7KH6vuUTU4IwTndw.wVEM3zi', '1', '13688889999', '1345@gmail.com', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/unlogin.jpg', '1', '后台超级管理员', '0', '0');
INSERT INTO `t_fbc_user` VALUES ('2', 'zy', 'zy', '$2a$10$JJ//ktYha3fdpwesBsXrhOF45zPtwqUlBYaaVYq4bVYjLTSuA7cne', '1', '13763345810', 'aa@16a2331.com', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/1606787198_20201030065039_f7e97.jpg', '1', '', '1', '1');
INSERT INTO `t_fbc_user` VALUES ('46', '张三', '张三', '$2a$10$5O1e1UybWQTGUgpCsKKgG.SeyU3RrBuuHM.1TKzWIMd5HtaRwhODG', '1', '13856951396', '', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/unlogin.jpg', '1', '', '1', '1');

-- ----------------------------
-- Table structure for t_fbc_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_user_role`;
CREATE TABLE `t_fbc_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=226 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_fbc_user_role
-- ----------------------------
INSERT INTO `t_fbc_user_role` VALUES ('218', '46', '2');
INSERT INTO `t_fbc_user_role` VALUES ('219', '2', '1');
INSERT INTO `t_fbc_user_role` VALUES ('220', '2', '3');
INSERT INTO `t_fbc_user_role` VALUES ('221', '2', '2');
INSERT INTO `t_fbc_user_role` VALUES ('222', '2', '34');
INSERT INTO `t_fbc_user_role` VALUES ('225', '1', '4');

-- ----------------------------
-- Table structure for t_fbc_wx_user
-- ----------------------------
DROP TABLE IF EXISTS `t_fbc_wx_user`;
CREATE TABLE `t_fbc_wx_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `club_id` int(11) DEFAULT NULL COMMENT '俱乐部ID',
  `openid` varchar(28) CHARACTER SET utf8 DEFAULT NULL COMMENT '小程序用户的openid',
  `nickname` varchar(100) DEFAULT NULL COMMENT '用户头像',
  `avatarurl` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '用户头像',
  `gender` tinyint(1) DEFAULT NULL COMMENT '性别  0-男、1-女',
  `country` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '所在国家',
  `province` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '省份',
  `city` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '城市',
  `language` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '语种',
  `ctime` datetime DEFAULT NULL COMMENT '创建/注册时间',
  `mobile` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '手机号码',
  `user_id` int(11) DEFAULT NULL COMMENT 'user表id',
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1234 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='微信用户id/家长';

-- ----------------------------
-- Records of t_fbc_wx_user
-- ----------------------------
INSERT INTO `t_fbc_wx_user` VALUES ('1111', '1', 'obEcA5BR2IZ9pNY76BgI_D9MxSrI', '魏征', 'https://thirdwx.qlogo.cn/mmopen/vi_32/6Cyqp7uksJ8kicnZcWRmyycTQslv1icrnhKkckQBPqC3Iuz7a42AsAEAg73Y6PNPNbQdShnCtPZ9uXsIXRPv8pag/132', '1', 'China', 'Shaanxi', 'Xianyang', 'zh_CN', '2020-12-11 16:01:03', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1112', '1', 'obEcA5LDXqCPEN9NEapcP3RaxnJQ', 'Hwhwhw', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqfgPxFvLia70ALbVwtg5p3dgRGiaFPg0da5WXgoMvGBDdGpSwNJicnlzxYDcImZJr8xOiajxNqBicMwRw/132', '1', 'China', 'Anhui', 'Hefei', 'zh_CN', '2020-12-11 17:01:20', '18155106060', '158', null);
INSERT INTO `t_fbc_wx_user` VALUES ('1113', '1', 'obEcA5OH_XfdGN8VEdPaMIFUwEks', '李威珮', 'https://thirdwx.qlogo.cn/mmhead/jMDZdZiciby47EibbV9ic8ic03LBkCQQ1Z7o1Nu24VkSbtVo/132', '0', '', '', '', 'zh_CN', '2020-12-12 00:42:42', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1114', '1', 'obEcA5KMwMbTI4-KTYkpTCW3BAZM', '赵静怡', 'https://thirdwx.qlogo.cn/mmhead/LFr7EJOdSzeA7j8Umz0gtf8n41d9aRkciars7EXnjvxo/132', '0', '', '', '', 'zh_CN', '2020-12-12 01:01:30', null, '46', null);
INSERT INTO `t_fbc_wx_user` VALUES ('1115', '1', 'obEcA5El4IDmgCQ0UKCuZXoj8vIo', '林上盈', 'https://thirdwx.qlogo.cn/mmhead/RILlOftibdwBduOLzWibjmfrT2flQM9g9pibMqrqoeic4s4/132', '0', '', '', '', 'zh_CN', '2020-12-13 02:59:33', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1116', '1', 'obEcA5Hhk3j0SKpoiAeqLO_PG5Gs', '林乐妹', 'https://thirdwx.qlogo.cn/mmhead/T5tUJibWEGlH9yoeUpbVoUs3hibIZQ0eiancq4keZkpza0/132', '0', '', '', '', 'zh_CN', '2020-12-14 15:48:34', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1117', '1', 'obEcA5Hhk3j0SKpoiAeqLO_PG5Gs', '林乐妹', 'https://thirdwx.qlogo.cn/mmhead/T5tUJibWEGlH9yoeUpbVoUs3hibIZQ0eiancq4keZkpza0/132', '0', '', '', '', 'zh_CN', '2020-12-14 15:48:34', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1118', '1', 'obEcA5Ps5iyotuPFlHGhZVAiV9u8', 'Shansf', 'https://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEJvwptf6S36ZD2FAHfwCt9XLbzjVdjAhuzcaHbLB9b5QPzahY4ve9B6F791GNrPKXAYvFILhdVwsw/132', '1', 'China', 'Anhui', 'Hefei', 'zh_CN', '2020-12-15 10:57:41', '18656131177', '147', '517');
INSERT INTO `t_fbc_wx_user` VALUES ('1119', '1', 'obEcA5AxSuS8Lip9qq5YkD80m8AE', '10.11', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJzmBzIeVHkjghGibQlAlcxziaevAGAP3KWxnBY1ibia5RY41g9yL2QSfWp5RJ7e9UuibpRbmfJlW6YlKg/132', '2', 'China', 'Hebei', 'Shijiazhuang', 'zh_CN', '2020-12-15 11:07:05', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1120', '1', 'obEcA5PaNMfAHOfT3S8uvk73UpqE', '福星高照', 'https://thirdwx.qlogo.cn/mmopen/vi_32/INc61dATFS9suRfnfb5OYY6wGsJwyl4G9JhrNoWlI36QbESY7feMl9nZQD6rOD6QuFtK2pUeticjcKLuvsDv7Lw/132', '2', 'China', 'Henan', 'Zhengzhou', 'zh_CN', '2020-12-15 11:07:05', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1121', '1', 'obEcA5FGPa4ULQSWOylJp3rJpLdc', '林静怡', 'https://thirdwx.qlogo.cn/mmhead/TxKKvLIgreysmNww4GZI5akAVODOoum82W9jaJwchOM/132', '0', '', '', '', 'zh_CN', '2020-12-15 11:58:34', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1122', '1', 'obEcA5HA2vRqdoz8Ox9w2xnf67Pg', '蓝色妖姬', 'https://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEL1hiaOspDC6XyYXvicqNT9jNwflASZt9QQfGmWumc1OKpgPSHQiap24A1VKIRJoVUJK8NFcj2fM6Szw/132', '1', 'China', 'Guangdong', 'Shenzhen', 'zh_CN', '2020-12-15 12:30:18', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1123', '1', 'obEcA5HgCjoDdghieKq-i-20QBVY', 'Shinawatra', 'https://thirdwx.qlogo.cn/mmopen/vi_32/KNjuziax9KHdbMvc2H0FxfWpA4hPjle7B0brNgvFSzIibXgsicCZxDDVmuiahBtzMxXQIs2vAdOYykoYEubq21fwWQ/132', '2', 'China', 'Shanxi', 'Changzhi', 'zh_CN', '2020-12-15 12:30:18', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1124', '1', 'obEcA5EM34CbjPTwWYvsd79mwuBA', '张诗刚', 'https://thirdwx.qlogo.cn/mmhead/hU7SibkqNnGr0AW67DnM5NHCFkvIRcznzdEdYRVAYz28/132', '0', '', '', '', 'zh_CN', '2020-12-15 15:16:59', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1125', '1', 'obEcA5BcQ0Ji2hJ4eTniIcV9axkA', '何伶元', 'https://thirdwx.qlogo.cn/mmhead/1g0iax6wubUmYibpETSa5W8YqxaV0CnNKEic9aib2dJOCJo/132', '0', '', '', '', 'zh_CN', '2020-12-15 15:17:00', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1126', '1', 'obEcA5PEOHhtkaebCnbpAUjoKR2o', '王秀玲', 'https://thirdwx.qlogo.cn/mmhead/ykn76iaG5WXBgmChLicT9u9hl4IuaGtfb5zS6iaOiaCTA2U/132', '0', '', '', '', 'zh_CN', '2020-12-15 15:17:18', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1127', '1', 'obEcA5DBUhyQGP0jZl84d1u0sgaA', '林竹水', 'https://thirdwx.qlogo.cn/mmhead/FfsoVwmNPnz2UMKfSRrkOdfnYbuict5ibg03mAYaHaOB8/132', '0', '', '', '', 'zh_CN', '2020-12-15 15:17:19', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1128', '1', 'obEcA5HrRdeWy6CqoVrxUnhlpV2I', '林慧颖', 'https://thirdwx.qlogo.cn/mmhead/kFqwnRZxAZwvuqYVpCE5c22qhJvredjFkEpD59k7HvE/132', '0', '', '', '', 'zh_CN', '2020-12-15 15:21:00', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1129', '1', 'obEcA5P93a7BgNWl8Aidvx0m_r7c', '徐紫富', 'https://thirdwx.qlogo.cn/mmhead/YLTIDXqxdHuByeFRFjxrvrJHKX1icMvxNAaKiaIQuDWAs/132', '0', '', '', '', 'zh_CN', '2020-12-15 15:30:19', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1130', '1', 'obEcA5PK52813gRGSqUIRp96Tpbw', '张鸿信', 'https://thirdwx.qlogo.cn/mmhead/tE6WDbtJOP1wLFclaMHicoicgKeRSKrLbX2zVVZPQAb24/132', '0', '', '', '', 'zh_CN', '2020-12-15 15:43:57', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1131', '1', 'obEcA5Oc5ICxHN73vOVAS7SlYAYs', '潘右博', 'https://thirdwx.qlogo.cn/mmhead/2Z91EBjKNmOA1msMMgdBTbcctjgs2SpDQA4aXfednvo/132', '0', '', '', '', 'zh_CN', '2020-12-15 15:47:07', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1132', '1', 'obEcA5NrJcF-crOoNJZ8qNS5gux0', '郑凯婷', 'https://thirdwx.qlogo.cn/mmhead/uBhFRA9Yc9VzyaSodsbwjDoN2C4ABy9afbFrz1QsiaAI/132', '0', '', '', '', 'zh_CN', '2020-12-15 15:50:18', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1133', '1', 'obEcA5Ap4I4FfSI4WdtMeMcEfHSw', '梁哲宇', 'https://thirdwx.qlogo.cn/mmhead/0APmnLuDpvbQr6t5ld6yfWsyIHEh4trbockibE4VYMAU/132', '0', '', '', '', 'zh_CN', '2020-12-15 16:34:07', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1134', '1', 'obEcA5PUDohFBQaNmpdoYG6ZRc3w', '何美玲', 'https://thirdwx.qlogo.cn/mmhead/14ujgYpNm5BgET4stlQ1rpe6dUupgB4TUKAFX30Xhuc/132', '0', '', '', '', 'zh_CN', '2020-12-15 16:34:57', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1135', '1', 'obEcA5O4MozxyAzLftaA0mF0M3bI', '韩健毓', 'https://thirdwx.qlogo.cn/mmhead/xUpey8hxSIRBSJEbvx4YCa5BBicEQc6SAdxzNptfuuKc/132', '0', '', '', '', 'zh_CN', '2020-12-15 16:35:02', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1136', '1', 'obEcA5F5HhM-pQEUOrp7yizu4Tqo', '谢佳雯', 'https://thirdwx.qlogo.cn/mmhead/Ichib5hYyuKHqIJhbleK5N66aNUSFdgKxicyJgwVW1pz0/132', '0', '', '', '', 'zh_CN', '2020-12-15 16:35:26', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1137', '1', 'obEcA5NUeiBfgPlEyPN-KHSFon3I', '黄秋萍', 'https://thirdwx.qlogo.cn/mmhead/g2Jkvqm3qNLicJvmGnplLVyP4xmfJOZYPSXQnyrrMN4E/132', '0', '', '', '', 'zh_CN', '2020-12-15 16:38:23', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1138', '1', 'obEcA5HJ6U2A7XRMg8KzsZBMjIbg', '王淑芳', 'https://thirdwx.qlogo.cn/mmhead/ndiboYMXoszJ762tXW8ac8rsEM6KVM2r3AqmtWvGmjiaY/132', '0', '', '', '', 'zh_CN', '2020-12-15 16:40:13', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1139', '1', 'obEcA5FxEg2XvE5rOgMuVPygaPTI', '陈伟伦', 'https://thirdwx.qlogo.cn/mmhead/nF9pJiaFKZibboAyNghy3Rh9WianN6fhdQJd2Q38M7K0Ic/132', '0', '', '', '', 'zh_CN', '2020-12-15 16:40:30', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1140', '1', 'obEcA5JLojPB0Ivz7EMuCePNb73Y', '郑雅任', 'https://thirdwx.qlogo.cn/mmhead/uWK749FiasibtbIia0Zlfq27xHjTqzmptxTFic9pF4k8Gnk/132', '0', '', '', '', 'zh_CN', '2020-12-15 16:43:50', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1141', '1', 'obEcA5AHT5wa1YYXc7HmqkYO5NiA', '黄国妹', 'https://thirdwx.qlogo.cn/mmhead/ichMFiaiarazpoibvwRzp4T9G7CibtALH0Bk3pIqZ7nRia3BE/132', '0', '', '', '', 'zh_CN', '2020-12-15 16:45:32', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1142', '1', 'obEcA5MDRZbkfEpbgYlldtXwdYiA', '许爱礼', 'https://thirdwx.qlogo.cn/mmhead/5tCU4Bs3tY11Hn7G6YlfGnj0yOuxtNjpqungWFJSabA/132', '0', '', '', '', 'zh_CN', '2020-12-16 03:59:52', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1143', '1', 'obEcA5Lj7_Zo-x4C5EnOQc2k0pXQ', '张莉雯', 'https://thirdwx.qlogo.cn/mmhead/6esUwuDZcnic0MfZcGnic6swLgkSJNyqj4YH5r0SWS79c/132', '0', '', '', '', 'zh_CN', '2020-12-16 04:52:31', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1144', '1', 'obEcA5KFtRmFtXBLZ_MWrxifCzVU', '王登康', 'https://thirdwx.qlogo.cn/mmhead/K0olExic1ZUXqicdicMegBic0kkWw8tp7WXib0B5L68dKyh0/132', '0', '', '', '', 'zh_CN', '2020-12-16 08:43:30', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1145', '1', 'obEcA5NqT7-HXqWt3TrWUGu5n5aU', '陈绍翰', 'https://thirdwx.qlogo.cn/mmhead/IUmaWg0Efj54EEew0xicQbVHzVWHZnusIAWShpXfcCYI/132', '0', '', '', '', 'zh_CN', '2020-12-20 03:12:27', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1146', '1', 'obEcA5OkIMnBr_Zk4oQpejK9ElD4', '林孟富', 'https://thirdwx.qlogo.cn/mmhead/cYNYds6gbO1AJFXSYXCEsa1hYqHGA9n4Qvzs3WavsCU/132', '0', '', '', '', 'zh_CN', '2020-12-20 06:36:56', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1147', '1', 'obEcA5IyS3YWj6qrVtJsL2gapq8Q', '张政霖', 'https://thirdwx.qlogo.cn/mmhead/gS5xVyTCH9EDOF5zXk8nzlk1rtjt716iaEiaUT1ibcib3Fs/132', '0', '', '', '', 'zh_CN', '2020-12-20 06:50:02', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1148', '1', 'obEcA5Keowx3GNsm94YMEhAhJa50', '神隐青枫', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKympFiaDsiaBriaKUS3AX0hD6tCLsCKibqM0cJpa51RANmsXDUuQLnibZrSDM6olgnub6FiaeDH9AMrxhw/132', '1', 'China', 'Anhui', 'Hefei', 'zh_CN', '2020-12-20 21:47:14', '18815597841', '148', null);
INSERT INTO `t_fbc_wx_user` VALUES ('1149', '1', 'obEcA5D88Y-VJKth0GdoVkPWxW1s', '低调', 'https://thirdwx.qlogo.cn/mmopen/vi_32/KNjuziax9KHdbMvc2H0FxfbNYI3lic2XOruRbVPuLgLG3V2IqavWdKzsb8nqLpFCbrDBvUUhHbFvb7YZsg7GfR9A/132', '1', 'China', 'Gansu', 'Lanzhou', 'zh_CN', '2020-12-22 09:05:00', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1150', '1', 'obEcA5K4W-CUxl_Sd34rYogCBVlM', '姚哲维', 'https://thirdwx.qlogo.cn/mmhead/FFvZugUeJFlhTrcYdOtIUCJv2yHMds9WIOhQ0jZzZUU/132', '0', '', '', '', 'zh_CN', '2020-12-24 09:59:03', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1151', '1', 'obEcA5OEVg19uqytRzitRwCyd-JM', '袁富毓', 'https://thirdwx.qlogo.cn/mmhead/1ZU9qTESZetEBbKnmJNTdhrnNufnUMzYHD5D11yGFrs/132', '0', '', '', '', 'zh_CN', '2020-12-24 12:56:33', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1152', '1', 'obEcA5IAZId1WdK-_LDMpMcWVXtk', '倪建辉', 'https://thirdwx.qlogo.cn/mmhead/a4gKxuDCUEwOXbTDAZyjCUSAB0AW3dB9Odh7NfOib3AQ/132', '0', '', '', '', 'zh_CN', '2020-12-27 05:47:38', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1153', '1', 'obEcA5J7NbQz0M8m3GJU-08axg-U', '林政哲', 'https://thirdwx.qlogo.cn/mmhead/PMeapZVlulDNaD2KSZlvtOt72AoleWJXjQ6oetIE86Y/132', '0', '', '', '', 'zh_CN', '2020-12-27 08:50:26', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1154', '1', 'obEcA5OyDIJmQhAyiLmpuaOA8x6g', '母笑阳', 'https://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEKoyjrRGcIsficoLsx0WdOnibT0nBsU33ofBgTEJKpMHicLuE2GfEOmmq91SFc2QKibiaFadfb0ZGsDjhA/132', '2', 'China', 'Hebei', 'Baoding', 'zh_CN', '2020-12-28 09:52:23', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1155', '1', 'obEcA5GcBpICin7sWeZQc9Y7tBZw', '詹梦顺', 'https://thirdwx.qlogo.cn/mmhead/esPILb1AoFoV3h1vq6TMTibcjNvCVTR3gicOrWw6KlyLU/132', '0', '', '', '', 'zh_CN', '2020-12-29 10:57:41', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1156', '1', 'obEcA5Bb-fSw9LLrot3532Nv5SAs', '冉佳琪', 'https://thirdwx.qlogo.cn/mmhead/qoKWXB1Vh7zonvAdlw3Nd5P6aKlaekq6WibS5Ev2klzQ/132', '0', '', '', '', 'zh_CN', '2021-01-03 00:18:42', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1157', '1', 'obEcA5KI2yZ63jPau00ZIz7H1Xxg', '吴淑芳', 'https://thirdwx.qlogo.cn/mmhead/ajuS1gBpJgkwmMkmxdOkTmTTjc94aIYibj0k1EABkyqI/132', '0', '', '', '', 'zh_CN', '2021-01-10 11:59:36', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1161', '1', 'obEcA5Pz-_JWyJ99uXuGUvmtijrw', '陈明义', 'https://thirdwx.qlogo.cn/mmhead/WCeCRj0RNr0ZSHoqpx7B035Z6PabWRu7b7cwzXdpiaKA/132', '0', '', '', '', 'zh_CN', '2021-01-17 11:57:41', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1169', '1', 'obEcA5Hle4ragXFgTc4VfgiEmCUA', 'Z', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKuaZauv0hcyBeFc63bTpWGYWCxb6XqiceppibmMsibfjkHH7KGKNesymT6I7lAKHhrF8ZSgiamvJSSvw/132', '2', 'China', 'Anhui', 'Hefei', 'zh_CN', '2021-01-19 16:39:56', '18269755531', '155', null);
INSERT INTO `t_fbc_wx_user` VALUES ('1183', '1', 'obEcA5LVpu-Gf0yHJfiWhI9kDYWs', '唐不二', 'https://thirdwx.qlogo.cn/mmopen/vi_32/dIFvSwakpR4W0GSkyibvgA6n4t6u8JI3VEdHEdZ7sIKPM9duTVHNTokgiaj0btJviamkBUvvxEribzAI5IAktPSDdg/132', '2', 'China', 'Anhui', 'Hefei', 'zh_CN', '2021-02-05 10:13:12', '13856951396', '154', null);
INSERT INTO `t_fbc_wx_user` VALUES ('1184', '1', 'obEcA5DycKcQ8ll__Y4e_FUNhcSQ', '许尚达', 'https://thirdwx.qlogo.cn/mmhead/mY4uVQzICrsQ6h6icY53cOHq2s1Bic34H9y4BhnUUU48M/132', '0', '', '', '', 'zh_CN', '2021-02-07 01:19:09', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1185', '1', 'obEcA5LgfuOuWEGvEdbh6XCRuDAk', '陈友汉', 'https://thirdwx.qlogo.cn/mmhead/Y30dqCCU9YOQXzU10M1YGdoqsdCuticibIonlMkseNPzg/132', '0', '', '', '', 'zh_CN', '2021-02-14 01:21:02', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1186', '1', 'obEcA5J0b5oZw0w7yhFnfcQ3WvcU', '蔡仪谕', 'https://thirdwx.qlogo.cn/mmhead/upHE5sXNwTHRvzicwFwdXRWNhJsfWKiaoniap4vCLRn2bM/132', '0', '', '', '', 'zh_CN', '2021-02-21 00:51:31', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1187', '1', 'obEcA5NSWN6OIT4krIRZVkaBJf8M', '刘淑卿', 'https://thirdwx.qlogo.cn/mmhead/269ISSGSEvicic2giaY3uPfIoy8pJobe8rNLn64E3ztNSc/132', '0', '', '', '', 'zh_CN', '2021-02-28 05:51:47', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1188', '1', 'obEcA5EBQEOzjYI3w4h0QJnROY2I', '陈育福', 'https://thirdwx.qlogo.cn/mmhead/DR06xqCCGvrkbj4Ria6OC3Wb9TOMB3qCFEvf97hT0FrQ/132', '0', '', '', '', 'zh_CN', '2021-02-28 13:09:40', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1189', '1', 'obEcA5PJc0Nh8vgFtgY07lPEDT2M', '月下独酌', 'https://thirdwx.qlogo.cn/mmopen/vi_32/WgL2B7mia5OeiaqkSslyibDhoIrG2Qvj8Mqia1icWy8rMcaqgl0L9Ufgw4Tbr50tohYvXQOBiadCreUibUQrFq1OknXJA/132', '0', '', '', '', 'zh_CN', '2021-03-01 14:36:31', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1190', '1', 'obEcA5LyorKm5GnweiGZNCvk2GrI', '丫丫宝贝', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erRZgRxSG6Ryx0Q0pEicdf3EyCsS2uj3PGWAwS0icxtibGqSyS8tS1YmzsrIKc2b78kSSm6oQmhztOdw/132', '2', 'China', 'Anhui', 'Hefei', 'zh_CN', '2021-03-01 15:15:34', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1191', '1', 'obEcA5AxVMwd5P1_1MwyIWATeGN8', '☆﹎影月*', 'https://thirdwx.qlogo.cn/mmopen/vi_32/SlcYrYPa2uBt3ictCb05TPTf4WcxhX3bCwQRCTD9rb8RBg92RBnHGvhvdrg647t429ibYwrzIEibrC9F5WKv3Jk0Q/132', '1', 'Ireland', 'Donegal', '', 'zh_CN', '2021-03-01 16:28:59', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1192', '1', 'obEcA5E7naDmhACsJzVVLacZTKdk', 'huanyu', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJK90DzCQ4oianfCaliapAIrLlfyQjcibLjCLQrgWMgAxLMTCWVHb7NwChI4tPwvzmoWPqQb76TIN3KQ/132', '2', 'China', 'Anhui', 'Hefei', 'zh_CN', '2021-03-02 15:38:45', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1193', '1', 'obEcA5C2CfvyE9tGlSAmMw5ei8VM', '谢仁琦', 'https://thirdwx.qlogo.cn/mmhead/qicOucBOcVuqOichRuhHoD77xdXGTZpmWZjflEUYeese8/132', '0', '', '', '', 'zh_CN', '2021-03-07 04:13:00', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1194', '1', 'obEcA5F2OjfSZBn6ijDWH7kwM2Sc', '普通人。。', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI3tWGQkicicpiadoxy4LWKibricHA0BlR2Y4z1UKLgiaO6ztcPtbicL9l2c1FFI8x71OI5QvgIlbt0rcia7g/132', '1', 'China', 'Anhui', 'Hefei', 'zh_CN', '2021-03-11 22:52:20', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1195', '1', 'obEcA5JGW37yzoSf6yjIp2BkmXkA', '赖雅惠', 'https://thirdwx.qlogo.cn/mmhead/Ms690xiaayXGY9ARPqKUcEibia4K19jqia3veQxCCCibJhia0/132', '0', '', '', '', 'zh_CN', '2021-03-14 02:00:58', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1196', '1', 'obEcA5D1LR9J0SrH-hi-BBHU71co', '崔蜀黍ing', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epFRbF5IkEBkNRHK64ktWOZFyn1Lc0kCtNEb1lcXW6ibh394fpm1bAMn2qCibnAibIEicgxaISAO7riaGQ/132', '1', 'China', 'Anhui', 'Hefei', 'zh_CN', '2021-03-19 18:03:35', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1197', '1', 'obEcA5B3d1baP51OHd4IAWIYYUwI', '吴威德', 'https://thirdwx.qlogo.cn/mmhead/X2lm8S5STdLwIlcRLoOMFnmHdgSFUjFyViaJJhFBPGoY/132', '0', '', '', '', 'zh_CN', '2021-03-21 04:46:57', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1198', '1', 'obEcA5Gmg8HhXltx0dVKOqogtd-8', '微信用户', 'https://thirdwx.qlogo.cn/mmopen/vi_32/POgEwh4mIHO4nibH0KlMECNjjGxQUq24ZEaGT4poC6icRiccVGKSyXwibcPq4BWmiaIGuG1icwxaQX6grC9VemZoJ8rg/132', '0', '', '', '', '', '2021-04-04 04:31:30', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1199', '1', 'obEcA5FReQJfQczyJ_kA8UC60sZk', '微信用户', 'https://thirdwx.qlogo.cn/mmopen/vi_32/POgEwh4mIHO4nibH0KlMECNjjGxQUq24ZEaGT4poC6icRiccVGKSyXwibcPq4BWmiaIGuG1icwxaQX6grC9VemZoJ8rg/132', '0', '', '', '', '', '2021-04-10 23:39:42', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1200', '1', 'obEcA5GSJ_xSTifqzyw1nAtEb_nE', '栉（zhì）风', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epkfkicvju9ITewB6Apekjian8c2kbPUibKwcgdoApeXL8KhCcsP2jpHzRnXicXibeKUE8MWhOcfaxlLlw/132', '1', 'China', 'Jiangsu', 'Suzhou', 'zh_CN', '2021-04-17 15:07:27', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1201', '1', 'obEcA5NCSN70S-HcSihbkxbPYXHw', '郑俊成', 'https://thirdwx.qlogo.cn/mmhead/1XNXLKWJ763L38lrfkFib1q4TVoaEN6qaHVudGeWmDpA/132', '0', '', '', '', 'zh_CN', '2021-04-18 08:34:28', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1202', '1', 'obEcA5NPeWPsaGBLZ5bV58lfY8zI', '李佳蓉', 'https://thirdwx.qlogo.cn/mmhead/DX1nQyXf4GTjCHopwWkMMuAfUwnW0fIPj15IxcHxPA8/132', '0', '', '', '', 'zh_CN', '2021-04-21 07:29:33', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1203', '1', 'obEcA5EKF5kzONIQtLsjvOfRk6xI', '陈世隆', 'https://thirdwx.qlogo.cn/mmhead/pqC7DJznNIEehU8IC5iaSiahIdiaQ9Y2KOOWaK0GGwnvMQ/132', '0', '', '', '', 'zh_CN', '2021-04-25 13:04:16', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1204', '1', 'obEcA5IqXbPmhPDgz0fRDfMgsyxk', '赖雨帆', 'https://thirdwx.qlogo.cn/mmhead/7zNhzibE5ylWpa4cz15yLKhLVFd1lfZ8vLYicKNL3CxFM/132', '0', '', '', '', 'zh_CN', '2021-05-01 23:53:26', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1205', '1', 'obEcA5EZ9PdU05L8XLw4QmH-DKqs', '李佳颖', 'https://thirdwx.qlogo.cn/mmhead/K7NzoK4o2QSQq1pDAvws5Uv6fwiaEahyz5clgtjjT2fc/132', '0', '', '', '', 'zh_CN', '2021-05-09 13:54:22', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1206', '1', 'obEcA5PLfpZCfIUxl0v7bMvaeEpc', '放学丶别走', 'https://thirdwx.qlogo.cn/mmopen/vi_32/9iaqWHYPcMHMFgpqZicvTANt6Ra3CIfapB460vnwowSUP64mFb0ibUlx2blibdEfdu89X2o9iaLhfSBh2n4bOcEpsibg/132', '1', 'China', 'Henan', 'Luohe', 'zh_CN', '2021-05-12 19:44:13', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1207', '1', 'obEcA5NKVeHnToUnSBxoNk2B9UCw', '李秀英', 'https://thirdwx.qlogo.cn/mmhead/gOo88bpbDGHCX3vQTm5FrkqVl1daxVGvdVJhx6zbSo4/132', '0', '', '', '', 'zh_CN', '2021-05-16 02:34:44', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1208', '1', 'obEcA5AB0LHudm62e6vl4s2HNzJ0', '夏昭来', 'https://thirdwx.qlogo.cn/mmhead/edsOKzKA7IsHicWEMWibdudrqevbQ9ujA5s3yd690iayPw/132', '0', '', '', '', 'zh_CN', '2021-05-23 06:31:29', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1209', '1', 'obEcA5HgZJYoO_DbVVi6RU4wlHps', '赖汉白', 'https://thirdwx.qlogo.cn/mmhead/o6EbVtUticvrtZSXUKCcM7eCTw8z4ql1FD76akz0stN8/132', '0', '', '', '', 'zh_CN', '2021-05-30 08:54:28', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1210', '1', 'obEcA5IoErXyTg4kUOSCOhaerEL8', '王培伦', 'https://thirdwx.qlogo.cn/mmhead/iaSb77d3BEHtTKbXricib5ibtwLdpKKiaT6eulEw3uIJicTN4/132', '0', '', '', '', 'zh_CN', '2021-06-06 15:51:17', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1211', '1', 'obEcA5B6XSw9n7fUGeK2hDt2sW7U', '谢欢岳', 'https://thirdwx.qlogo.cn/mmhead/13UFmBH4xXygZBfeftTRf7zxTknk2E9tFclSl190G9o/132', '0', '', '', '', 'zh_CN', '2021-06-13 16:15:00', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1212', '1', 'obEcA5LL_EH40ZHp7GHDOoJDnEQY', '啥也不是.', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DU6kIsJJMbmibRMdjhp20pEFtEvSticUT3ACpr7rORslehVKO3EbfLFGqNUWNuDfuG9vZ4w0ubBdyqswcFx1eHRQ/132', '1', 'China', 'Guangxi', 'Baise', 'zh_CN', '2021-06-18 00:05:25', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1213', '1', 'obEcA5K5m7QdfN2TT3DH0-UnpgzQ', '卢淑芬', 'https://thirdwx.qlogo.cn/mmhead/Y30dqCCU9YOQXzU10M1YGdoqsdCuticibIonlMkseNPzg/132', '0', '', '', '', 'zh_CN', '2021-06-20 03:50:04', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1214', '1', 'obEcA5Po77jMvZs_WgiJJqQrDeZ8', '黄志法', 'https://thirdwx.qlogo.cn/mmhead/j2ic7PR2I0qx0OwtAvY4fxKib5LKNU1HNsxY8mkjGZ7uc/132', '0', '', '', '', 'zh_CN', '2021-06-27 12:18:45', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1215', '1', 'obEcA5OuhWOJ2bsOOt7mxbqM2-KM', '罗静蓁', 'https://thirdwx.qlogo.cn/mmhead/IvqF0OPGgYHWjNSOIhU2GnvcK59145UXhVzicZ47tKCI/132', '0', '', '', '', 'zh_CN', '2021-07-05 16:27:17', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1216', '1', 'obEcA5NOKgaulqLyIHZf4SgTDEkI', 'Light.-光', 'https://thirdwx.qlogo.cn/mmopen/vi_32/M1SGEgNicoHBrzfyb8eEjXNUsQcTRnW4fiaYcoOoY0g08bz37HxOf7GsX9ddBqPDiaEuw2ofpIQGMW7egiaP4ZJZqw/132', '2', 'Hong Kong', 'Kowloon City', '', 'zh_CN', '2021-07-05 20:32:26', '18933787697', '156', null);
INSERT INTO `t_fbc_wx_user` VALUES ('1217', '1', 'obEcA5Eo7WxcHFJjkYrdFz7qtat0', '蒙孟涵', 'https://thirdwx.qlogo.cn/mmhead/wsLbTgRR2iaqLDuVQynRa8txV7A4B7nzNFDK0vkEu5pQ/132', '0', '', '', '', 'zh_CN', '2021-07-17 05:46:01', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1218', '1', 'obEcA5HTpuZnLkmysjaJgZhN1e68', '在下尚方宝贱.', 'https://thirdwx.qlogo.cn/mmopen/vi_32/wXAc0GzM0W2aJqY3rnQMcia49thowBtKEBB8wMcqqZL3dP3cbb2PicwqA7xibZRz2tfGMibiaHHo1hHd1BpicQrbQibzA/132', '1', 'China', 'Liaoning', 'Shenyang', 'zh_CN', '2021-07-19 21:25:34', '18640433798', '157', null);
INSERT INTO `t_fbc_wx_user` VALUES ('1219', '1', 'obEcA5EU04TMIf6ojo1tcm-L5P5s', '黄俊坤', 'https://thirdwx.qlogo.cn/mmhead/gO1TDMLfREuvU1VlUfOxEMjaJom6wKhf1AkibmvZfZPw/132', '0', '', '', '', 'zh_CN', '2021-07-24 11:08:23', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1220', '1', 'obEcA5EVK95HLxvRn2nMH_LFotXU', '蒋世昌', 'https://thirdwx.qlogo.cn/mmhead/rneMlzCiabb4AnIG0ibqGC9PwRrf0PSdSpFQehXNwHq0I/132', '0', '', '', '', 'zh_CN', '2021-07-31 16:28:32', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1221', '1', 'obEcA5PaLAZApXXTMgF711LHXKKI', '张振宇', 'https://thirdwx.qlogo.cn/mmhead/PMeapZVlulDNaD2KSZlvtOt72AoleWJXjQ6oetIE86Y/132', '0', '', '', '', 'zh_CN', '2021-08-07 08:55:13', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1222', '1', 'obEcA5Cqn6EbVvaKZZuNZSy4OFQE', '朱左峰', 'https://thirdwx.qlogo.cn/mmhead/e7ZsDmcUN4mF89Jm2UAuhmiaGXOvYXbrrzOtuK92NbBM/132', '0', '', '', '', 'zh_CN', '2021-08-14 04:11:29', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1223', '1', 'obEcA5ItcsYDxh-FwdAf0G31ImU8', '林承枝', 'https://thirdwx.qlogo.cn/mmhead/Onz7ibmX6nD3XsVcHkjvL2SEiajpibiaINSIW5XftF8iazRc/132', '0', '', '', '', 'zh_CN', '2021-08-21 10:23:15', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1224', '1', 'obEcA5HfD2PZxqUoJPEjybwg288k', '郭建宇', 'https://thirdwx.qlogo.cn/mmhead/tcL8qvsBJrKiaCz6619mvgEXBlZhlw2ibT0UkUI9CDecA/132', '0', '', '', '', 'zh_CN', '2021-08-28 11:04:59', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1225', '1', 'obEcA5LX8aWSmNrWjBtJBFmCkB8M', '丁昆来', 'https://thirdwx.qlogo.cn/mmhead/olDBR3OqOdzMApuwjNDdZicdEz9EcRwEASA0vVz9OYNk/132', '0', '', '', '', 'zh_CN', '2021-09-04 14:47:43', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1226', '1', 'obEcA5BLbKwjFzh-Vr7Vnv8Hp_bM', '戴火劭', 'https://thirdwx.qlogo.cn/mmhead/5Nc6EVibjiakNhk377fDugX17meibFgWU0L4XGKX7kGaz4/132', '0', '', '', '', 'zh_CN', '2021-09-11 18:18:30', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1227', '1', 'obEcA5Pjg7DdUBou8D7-iw953w-Q', '且行且珍惜', 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqZN3icyPfGK7XiaLddd34E99B9cwntvRxoylpdNbKxDLlSpK9SMecWYlwpKibdAZMDjlVPicMiakvvexg/132', '1', 'China', 'Jiangsu', 'Nanjing', 'zh_CN', '2021-09-11 21:56:18', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1228', '1', 'obEcA5LbrpWPtrBRm-TQc9r8Y9mc', 'Fairy?', 'https://thirdwx.qlogo.cn/mmopen/vi_32/hYWNvXtUOUplsfdPzbDqySKRdfmBbLbT72WicZ6gkWrW6BI4uUiar0icn1935b77bqOUBKYHaQjkc2jyCY8GcOMcg/132', '0', '', '', '', 'zh_CN', '2021-09-15 08:55:02', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1229', '1', 'obEcA5IfaV6BZarZaP9Hmk-YbOWo', '许平纬', 'https://thirdwx.qlogo.cn/mmhead/9wicafibjB87NaRHCGiaxAesdjnNdBAgMYCicicgagNBxrZE/132', '0', '', '', '', 'zh_CN', '2021-09-18 05:22:05', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1230', '1', 'obEcA5N3qQt-2iPUc2QmnHdbPBdI', '刘海亮', 'https://thirdwx.qlogo.cn/mmopen/vi_32/nicubVnkJefTwXWHVjYnJHh35lbNKhJszjQxvcTvGKe7bfILRradIgH9ibRcKcEqp3pk5yslFCTwyYnvDKDp2D1g/132', '1', 'China', 'Guangdong', 'Zhaoqing', 'zh_CN', '2021-09-22 21:01:41', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1231', '1', 'obEcA5A-EJmBBcNdeaNA7RxeZ5Ts', '林欣喜', 'https://thirdwx.qlogo.cn/mmhead/gCCqib5ZaCdba8B4aReRpuuRpvPuh1dU9rpOdic5IeIxk/132', '0', '', '', '', 'zh_CN', '2021-09-25 03:42:30', null, null, null);
INSERT INTO `t_fbc_wx_user` VALUES ('1232', '1', 'obEcA5CpwZdRVOSm8SHvL4TRyPZs', 'Mi Manchi', 'https://thirdwx.qlogo.cn/mmopen/vi_32/PR0My9bc2BDJGtsJhnjpxLXBNvrUKmicehRvKYR5dX4zibuamhHtYQWATGxBbGpgRiaHooAgic1mO3cIF6P0NRwLKA/132', '1', 'United Arab Emirates', 'Dubai', '', 'zh_CN', '2021-09-26 22:04:01', '13360557156', '159', null);
INSERT INTO `t_fbc_wx_user` VALUES ('1233', '1', 'obEcA5LE8Ff6C3kiPJwmHIebm8yA', '。', 'https://thirdwx.qlogo.cn/mmopen/vi_32/to2r1q3l77iaicw845ZE4ILGgZKsjF2xicRuQ9Xu4GibwDqEIYcjHrPoHzcicfK0jxg3iaR9syHhtyuT2CyRSFibjukqg/132', '1', 'United Arab Emirates', 'Dubai', '', 'zh_CN', '2021-09-29 01:04:56', null, null, null);
