/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50730
Source Host           : localhost:3306
Source Database       : super

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2021-10-04 16:51:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for jc_role
-- ----------------------------
DROP TABLE IF EXISTS `jc_role`;
CREATE TABLE `jc_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `type` int(2) DEFAULT '1' COMMENT '角色类型，1普通角色，2后台系统角色',
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '角色说明',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户角色表';

-- ----------------------------
-- Records of jc_role
-- ----------------------------
INSERT INTO `jc_role` VALUES ('1', '2', '管理员', '后台权限',now(),now());

-- ----------------------------
-- Table structure for jc_menu
-- ----------------------------
DROP TABLE IF EXISTS `jc_menu`;
CREATE TABLE `jc_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(100) DEFAULT NULL COMMENT '菜单名称',
  `path` varchar(50) DEFAULT NULL COMMENT '路径',
  `component` varchar(100) DEFAULT NULL COMMENT '组件',
  `redirect` varchar(200) DEFAULT '' COMMENT '重定向',
  `always_show` tinyint(1) DEFAULT '0' COMMENT '仅有一个子节点时是否显示根目录（1:是0：否）',
  `pid` int(11) DEFAULT '0' COMMENT '父ID',
  `meta_title` varchar(50) DEFAULT '' COMMENT 'meta标题',
  `meta_icon` varchar(50) DEFAULT '' COMMENT 'meta icon',
  `meta_nocache` tinyint(1) DEFAULT '0' COMMENT '是否缓存（1:是 0:否）',
  `meta_affix` tinyint(1) DEFAULT '0' COMMENT '是否加固（1:是0：否）',
  `status` tinyint(1) DEFAULT '0' COMMENT '是否隐藏（1:是0：否）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2011 DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of jc_menu
-- ----------------------------
INSERT INTO `jc_menu` VALUES ('1000', 'SystemManage', '/permission', 'Layout', '/permission/user', '0', '0', '系统管理', 'setting', '0', '0', '0');
INSERT INTO `jc_menu` VALUES ('1005', 'Permission', 'role', '/system/role', '', '0', '1000', '角色管理', '', '0', '0', '0');
INSERT INTO `jc_menu` VALUES ('1010', 'WxUser', 'wx', '/system/wxmanage', '', '0', '1000', '微信管理', '', '0', '0', '0');
INSERT INTO `jc_menu` VALUES ('1015', 'UserSetting', 'user', '/system/user', '', '0', '1000', '用户管理', '', '0', '0', '0');
INSERT INTO `jc_menu` VALUES ('1020', 'ClubUser', 'clubuser', '/system/clubuser', '', '0', '1000', '已绑定用户管理', '', '0', '0', '1');
INSERT INTO `jc_menu` VALUES ('1025', 'Advertising', 'advertising', '/system/advertising', '', '0', '1000', '首页广告位管理', '', '0', '0', '0');
INSERT INTO `jc_menu` VALUES ('2000', 'ChatRoom', '/chat', 'Layout', '/permission/chat', '0', '0', '聊天室', 'peoples', '0', '0', '0');
INSERT INTO `jc_menu` VALUES ('2005', 'PrivateRoom', 'privateroom', '/chat/privateroom', '', '0', '2000', '私聊', '', '0', '0', '0');
INSERT INTO `jc_menu` VALUES ('2010', 'GroupRoom', 'grouproom', '/chat/grouproom', '', '0', '2000', '群聊', '', '0', '0', '0');

-- ----------------------------
-- Table structure for jc_menu_role
-- ----------------------------
DROP TABLE IF EXISTS `jc_menu_role`;
CREATE TABLE `jc_menu_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menu_id` int(11) DEFAULT NULL COMMENT '菜单id',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jc_menu_role
-- ----------------------------
INSERT INTO `jc_menu_role` VALUES ('1', '2000', '1');
INSERT INTO `jc_menu_role` VALUES ('2', '2005', '1');
INSERT INTO `jc_menu_role` VALUES ('3', '2010', '1');
INSERT INTO `jc_menu_role` VALUES ('6', '1000', '1');
INSERT INTO `jc_menu_role` VALUES ('7', '1005', '1');
INSERT INTO `jc_menu_role` VALUES ('8', '1010', '1');
INSERT INTO `jc_menu_role` VALUES ('9', '1015', '1');
INSERT INTO `jc_menu_role` VALUES ('10', '1025', '1');
INSERT INTO `jc_menu_role` VALUES ('51', '1010', '2');
INSERT INTO `jc_menu_role` VALUES ('52', '1025', '2');
INSERT INTO `jc_menu_role` VALUES ('53', '2000', '2');
INSERT INTO `jc_menu_role` VALUES ('54', '2005', '2');
INSERT INTO `jc_menu_role` VALUES ('55', '2010', '2');


-- ----------------------------
-- Table structure for jc_user
-- ----------------------------
DROP TABLE IF EXISTS `jc_user`;
CREATE TABLE `jc_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `real_name` varchar(255) DEFAULT NULL COMMENT '真实姓名',
  `user_name` varchar(50) DEFAULT NULL COMMENT '登录名称',
  `pass_word` varchar(100) DEFAULT NULL COMMENT '登录密码',
  `gender` int(2) DEFAULT '1' COMMENT '性别：0未知，1男，2女',
  `phone` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱地址',
  `avatar` text COMMENT '头像照片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of jc_user
-- ----------------------------
INSERT INTO `jc_user` (`id`, `real_name`, `user_name`, `pass_word`, `gender`, `phone`, `email`, `avatar`) VALUES ('1', '后台管理', 'admin', '$2a$10$qi1h.fJ3dmfBMV2C7R0cmOgIWPwfk7KH6vuUTU4IwTndw.wVEM3zi', '1', '13688889999', '1345@gmail.com', 'https://ahzy-oss.oss-cn-hangzhou.aliyuncs.com/fbc/unlogin.jpg');

-- ----------------------------
-- Table structure for jc_user_role
-- ----------------------------
DROP TABLE IF EXISTS `jc_user_role`;
CREATE TABLE `jc_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色关联表';

INSERT INTO `jc_user_role` (`id`, `user_id`, `role_id`) VALUES ('1', '1', '1');

-- ----------------------------
-- Table structure for jc_archives
-- ----------------------------
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for jc_archives
-- ----------------------------
DROP TABLE IF EXISTS `jc_archives`;
CREATE TABLE `jc_archives` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) DEFAULT '' COMMENT '姓名',
  `sex` varchar(8) DEFAULT '' COMMENT '性别（男，女）',
  `picture` varchar(255) DEFAULT '' COMMENT '照片（存照片位置路径）',
  `birth_date` date DEFAULT NULL COMMENT '出生日期',
  `location` varchar(255) DEFAULT '' COMMENT '所在地',
  `work` varchar(128) DEFAULT NULL COMMENT '工作',
  `introduction` varchar(255) DEFAULT NULL COMMENT '个人简介',
  `requirement` varchar(255) DEFAULT NULL COMMENT '择偶要求',
  `wx_id` int(11) DEFAULT NULL COMMENT '微信用户表id',
  `contact` varchar(32) DEFAULT NULL COMMENT '联系方式',
  `status` varchar(255) DEFAULT NULL COMMENT '隐藏标识（0不隐藏 1隐藏）',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `salary` varchar(20) DEFAULT NULL COMMENT '年薪',
  `education` varchar(20) DEFAULT NULL COMMENT '学历',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for jc_wx_user
-- ----------------------------
DROP TABLE IF EXISTS `jc_wx_user`;
CREATE TABLE `jc_wx_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `openid` varchar(28) DEFAULT NULL COMMENT '小程序用户的openid',
  `nickName` varchar(100) DEFAULT NULL COMMENT '用户头像',
  `avatarUrl` varchar(200) DEFAULT NULL COMMENT '用户头像',
  `sex` varchar(1) DEFAULT NULL COMMENT '性别  0-男、1-女',
  `country` varchar(100) DEFAULT NULL COMMENT '所在国家',
  `province` varchar(100) DEFAULT NULL COMMENT '省份',
  `city` varchar(100) DEFAULT NULL COMMENT '城市',
  `ctime` datetime DEFAULT NULL COMMENT '创建/注册时间',
  `customer_service` int(2) DEFAULT '0' COMMENT '小程序客服 0非客服 1客服',
  `archId` int(11) DEFAULT NULL COMMENT '档案ID',
  PRIMARY KEY (
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
`id`) USING BTREE,
  UNIQUE KEY `openIdInx` (`openid`) USING BTREE

-- ----------------------------
-- Table structure for jc_favorites
-- ----------------------------
DROP TABLE IF EXISTS `jc_favorites`;
CREATE TABLE `jc_favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wxId` int(11) DEFAULT NULL,
  `archId` int(11) DEFAULT NULL COMMENT '档案表id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for jc_advertising
-- ----------------------------
DROP TABLE IF EXISTS `jc_advertising`;
CREATE TABLE `jc_advertising` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `picture` varchar(255) DEFAULT NULL COMMENT '图片url',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `status` varchar(2) DEFAULT '0' COMMENT '状态 0显示 1隐藏',
  `order_no` int(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for jc_msg_record
-- ----------------------------
DROP TABLE IF EXISTS `jc_msg_record`;
CREATE TABLE `jc_msg_record` (
  `from_id` int(11) NOT NULL COMMENT '发送人id',
  `to_id` int(11) NOT NULL COMMENT '接收人id',
  `group_id` int(255) NOT NULL DEFAULT '0' COMMENT '群组id',
  `type` int(255) NOT NULL DEFAULT '0' COMMENT '系统标识（0：微信 1：后台）',
  `count` int(11) DEFAULT '1' COMMENT '未读消息条数',
  `total` int(255) DEFAULT NULL COMMENT '总消息条数',
  `status` int(255) DEFAULT '0' COMMENT '是否已读 （1：是，0：否）',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`from_id`,`to_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
