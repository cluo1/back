#!/bin/sh
count=$(ps -fe|grep jc-manager |grep -v grep |wc -l)
if [ "$count" -eq "0" ]; then
 nohup java -Xms1g -Xmx1g -XX:PermSize=128M -XX:MaxPermSize=128M -Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=18082,server=y,suspend=n -jar jc-manager-0.0.1-SNAPSHOT.jar > manager.log 2>&1 &
 PID=$!
# echo $PID >  manager.pid
else
 echo ": manager has runing"
fi
